//
//  RoutingTypes.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <string>

namespace citymaps
{
    enum RoutingUnitSystem
    {
        UnitSystemImperial,
        UnitSystemMetric
    };

    enum RoutingMode
    {
        RoutingModeDriving = (1 << 1),
        RoutingModeWalking = (1 << 2),
        RoutingModePublicTransport = (1 << 3),
        RoutingModeBicycle = (1 << 4),
        RoutingModeShortest = (1 << 5),
        RoutingModeFastest = (1 << 6),
        RoutingModeTrafficEnabled = (1 << 7),
        RoutingModeTrafficDisabled = (1 << 8)
    };

    static const int kDefaultRoutingMode = RoutingModeDriving | RoutingModeFastest | RoutingModeTrafficEnabled;

    class Route;

    typedef std::function<void(const std::vector<Route*>&)> TRouteSuccess;
    typedef std::function<void(std::string)> TRouteError;
}