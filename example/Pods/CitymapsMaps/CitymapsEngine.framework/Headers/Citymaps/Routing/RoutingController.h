//
//  RoutingController.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/4/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Core/MapState.h>
#include <CitymapsEngine/Citymaps/Routing/RoutingRequest.h>
#include <CitymapsEngine/Citymaps/Routing/RouteInstruction.h>
#include <CitymapsEngine/Citymaps/Routing/RoutingControllerListener.h>
#include <CitymapsEngine/Citymaps/Routing/KalmanGPSPredictor.h>

namespace citymaps
{
    class Route;
    class VectorLayer;
    class Map;
    class CanvasShapeGroup;
    class CanvasLine;
    class HTTPConnection;
    
    struct RouteNotificationState
    {
        int Instruction = -1;
        double LastNotificationTime = 0;
        double LastNotificationDistance = INFINITY;
    };
    
    class RoutingController
    {
    public:
        
        RoutingController(VectorLayer *layer);
        ~RoutingController();
        
        void SetMap(Map *map);
        void SetListener(IRoutingControllerListener *listener);
        void Update(const MapState& mapState);
        
        void StartRoutingRequest(const RoutingRequest& request, TRouteSuccess success, TRouteError error);
        void StartRoute(Route *route);
        void CancelRouting();
        
        bool IsNavigating() const { return mCurrentRoute != NULL;}
        
    private:
        
        RoutingRequest mCurrentRequest;
        IRoutingControllerListener *mListener;
        Map *mMap;
        Route* mCurrentRoute;
        
        double mRoutingStartTime;
        double mLastStateUpdateTime;
        KalmanGPSPredictor mGPSPredictor;
        RouteState mCurrentRouteState;
        RouteNotificationState mNotificationState;
        
        HTTPConnection *mRerouteRequestconnection;
        
        // Builds a notification for the current state.
        std::string BuildNotificationString(int instruction, double distance);
        RouteState CalculateRouteState(const MapState& mapState);
        double MinimumDistance(const RouteInstruction& instruction, const Point& lonLat);
    };
}