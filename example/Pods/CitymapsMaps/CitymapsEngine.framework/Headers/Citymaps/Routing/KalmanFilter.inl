//
//  KalmanFilter.cpp
//  CityMaps
//
//  Created by Adam Eskreis on 7/30/14.
//  Copyright (c) 2014 CityMaps. All rights reserved.
//

#include "KalmanFilter.h"

namespace citymaps
{
    template <typename Vector, typename Matrix>
    void KalmanFilter<Vector, Matrix>::Step(const Vector &control, const Vector &measurement)
    {
        //Logger::Log("Position: %f, %f, Velocity: %f, %f", x[0], x[1], control[0], control[1]);
        
        Vector xk = (x * A) + (control * B);
        Matrix Pk = ((A * P) * glm::transpose(A)) + Q;
        
        Vector y = measurement - (xk * H);
        Matrix S = ((H * Pk) * glm::transpose(H)) + R;
        
        Matrix K = Pk * glm::transpose(H) * glm::inverse(S);
        x = xk + (y * K);
        P = (Matrix() - (K * H)) * Pk;
    }
}