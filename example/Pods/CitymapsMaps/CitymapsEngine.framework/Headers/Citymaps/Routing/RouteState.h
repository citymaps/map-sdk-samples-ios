//
//  RouteState.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/8/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

namespace citymaps
{
    struct RouteState
    {
        int Instruction = -1;
        double DistanceUntilNextInstruction = 0;
        double DistanceUntilDestination = 0;
        double TimeUntilDestination = 0;
        double TimeUntilNextInstruction = 0;
        double RouteHeading = 0;
        double Velocity = 0;
    };
}