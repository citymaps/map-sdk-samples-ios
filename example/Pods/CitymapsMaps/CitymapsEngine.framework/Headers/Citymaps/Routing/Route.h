//
//  Route.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/4/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

#include <CitymapsEngine/Citymaps/Routing/RoutingTypes.h>
#include <rapidjson/document.h>

namespace citymaps
{
    class RouteInstruction;
    
    struct Route
    {
    public:
        Route();
        Route(rapidjson::Value &jsonObject, uint32_t mode);
        ~Route();
        
        void Set(rapidjson::Value &jsonObject);
        
        const std::vector<Point>& GetPoints() const { return mPoints; }
        
        const std::vector<RouteInstruction>& GetInstructions() const { return mInstructions;}
        
        double GetDistance() const { return mDistance; }
        
        int GetDuration() const { return mDuration; }
        
        const Point& GetStart() const { return mStart; }
    
        const Point& GetEnd() const { return mEnd; }
        
        uint32_t GetMode() { return mMode; }
        
    private:
        std::vector<Point> mPoints;
        std::vector<RouteInstruction> mInstructions;
        
        double mDistance;
        int mDuration;
        Point mStart;
        Point mEnd;
        uint32_t mMode;
    };
};