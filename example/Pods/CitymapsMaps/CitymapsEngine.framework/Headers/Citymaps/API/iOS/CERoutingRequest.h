//
//  CERoutingRequest.h
//  CityMaps
//
//  Created by Adam Eskreis on 5/1/14.
//  Copyright (c) 2014 CityMaps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CitymapsEngine/Citymaps.h>

@interface CERoutingRequest : NSObject

/** The starting position for routing. Should be the user's location in order to work with turn by turn navigation. */
@property (assign, nonatomic) CELonLat start;

/** The ending position for routing. */
@property (assign, nonatomic) CELonLat end;

/** The number of alternative routes to return in the response. */
@property (assign, nonatomic) NSInteger alternatives;

/** The unit system to report notifications in. */
@property (assign, nonatomic) CERoutingUnitSystem units;

/** The modes of transportation to support in the routes. */
@property (assign, nonatomic) NSInteger mode;

@end