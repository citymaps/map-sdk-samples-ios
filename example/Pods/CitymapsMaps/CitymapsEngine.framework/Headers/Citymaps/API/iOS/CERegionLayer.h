#import <CitymapsEngine/Map/API/iOS/Layer/CETileLayer.h>

/** This layer is used to render Citymaps regions at higher zoom levels.  Citymaps regions identify cities and towns across the USA with a unique marker.
 
 See CECitymapsTileLayer for a list of available options.
 
 */

@interface CERegionLayerOptions : CETileLayerOptions

@property (strong, nonatomic) NSString *imageHostname;

@end

@interface CERegionLayer : CETileLayer

/**
 @name Initializers
 */

/**
 Initialize with an API Key
 
 @param apiKey Your Citymaps API Key
 */

- (id)initWithAPIKey:(NSString *)apiKey;

@end