#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>

/** A description of a Citymaps business.
 
 Used by CEBusinessFilter to describe a business within a filter.  Also used by CEBusinessDelegate to return information when a business receives an event.
 */

@interface CEBusinessData : NSObject

/** 
 * @name Properties
 *  
 */

/** The Citymaps business ID. */
@property (nonatomic, strong) NSString *businessID;

/** The display name of the business.  */
@property (nonatomic, strong) NSString *name;

/** Street address of the business */
@property (nonatomic, strong) NSString *address;

/** City of the business. */
@property (nonatomic, strong) NSString *city;

/** State or province of the business. */
@property (nonatomic, strong) NSString *state;

/** Country of the business. */
@property (nonatomic, strong) NSString *country;

/** Postal Code of the business. */
@property (nonatomic, strong) NSString *zip;

/** Phone number of the business */
@property (nonatomic, strong) NSString *phone;

/** The Citymaps logo image identifier */
@property (nonatomic, assign) NSUInteger logoImageID;

/** The full Citymaps category ID */
@property (nonatomic, assign) NSUInteger category;

/** The Citymaps category icon identifier. */
@property (nonatomic, assign) NSUInteger categoryIconID;

/** The location of the business. */
@property (nonatomic, assign) CELonLat location;

/** The analytics partners for this business. */
@property (nonatomic, assign) NSUInteger analyticsPartner;

/** The 5 star rating for this business. A value of 0 indicates unknown. */
@property (nonatomic, assign) CGFloat starRating;

/** The name of the category. */
@property (nonatomic, strong) NSString *categoryName;

/** All businesses that are at the same location as this business */
@property (nonatomic, strong) NSArray *adjacentBusinesses;

@end
