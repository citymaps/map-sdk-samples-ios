#import <CitymapsEngine/Citymaps/API/iOS/CECitymapsMapView.h>

#include <CitymapsEngine/Citymaps/Layer/BusinessLayer.h>
#include <CitymapsEngine/Citymaps/Layer/Util/BusinessData.h>


namespace citymaps
{
    class BusinessListenerIOS : public IBusinessListener
    {
    public:
        
        void SetDelegate(id <CEBusinessDelegate> delegate)
        {
            mDelegate = delegate;
        }
        
        void SetBusinessLayer(CEBusinessLayer *layer)
        {
            mBusinessLayer = layer;
        }
        
        void OnBusinessTapped(const BusinessData& data);
        void OnBusinessDoubleTapped(const BusinessData& data);
        void OnBusinessLongPressed(const BusinessData& data);
        
        void OnBusinessEnterZone(const BusinessData& data, int index);
        void OnBusinessExitZone(const BusinessData& data, int index);
        
        void OnBusinessAdded(const BusinessData& data);
        void OnBusinessRemoved(const BusinessData& data);
        
    private:
        __weak CEBusinessLayer *mBusinessLayer;
        __weak id <CEBusinessDelegate> mDelegate;
        
    };
}