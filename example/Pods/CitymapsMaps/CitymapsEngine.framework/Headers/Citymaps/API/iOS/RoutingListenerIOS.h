//
//  RoutingListenerIOS.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/8/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//


#include <CitymapsEngine/Citymaps/API/iOS/CECitymapsMapView.h>
#include <CitymapsEngine/Citymaps/Routing/RoutingControllerListener.h>

namespace citymaps
{
    class RoutingListenerIOS : public IRoutingControllerListener
    {
    public:
        
        void SetDelegate(id<CERoutingDelegate> delegate)
        {
            mDelegate = delegate;
        }

        //void OnReroute(Route *route);
        //void OnRouteFinished(bool reachedDestination);
        void OnRouteNotification(const std::string& notification);
        void OnRouteInstructionBegan(int index);
        void OnRouteStateUpdate(const RouteState& state);
        
    private:
        __weak CEVectorLayer *mVectorLayer;
        __weak id<CERoutingDelegate> mDelegate;
        
    };
}