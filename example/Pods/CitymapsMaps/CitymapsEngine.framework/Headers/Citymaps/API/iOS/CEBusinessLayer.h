#import <CitymapsEngine/Map/API/iOS/Layer/CETileLayer.h>

#import <CitymapsEngine/Citymaps/API/iOS/CEBusinessFilter.h>
#import <CitymapsEngine/Citymaps/API/iOS/CEBusinessZone.h>
#import <CitymapsEngine/Map/API/iOS/CEMarkerAttachmentView.h>
#import <CitymapsEngine/Map/API/iOS/CEMarkerGroup.h>

#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>

/** This delegate will indicate to the user when actions are taken on business markers in particular. */

@protocol CEBusinessDelegate <NSObject>

/** Notifies the delegate when a business is tapped.  Returns all businesses at this location.
 
 @param data - The business data of the tapped marker.
 */
- (void)businessTapped:(CEBusinessData *)data;

/** Notifies the delegate when a business is double tapped.
 
 @param data - The business data of the tapped marker.
 */
- (void)businessDoubleTapped:(CEBusinessData *)data;

/** Notifies the delegate when a business is long pressed.
 
 @param data - The business data of the pressed marker.
 */
- (void)businessLongPressed:(CEBusinessData *)data;

/** Notifies the delegate when a business enters a zone.
  * @param zone - The zone entered.
 @param data - The business data of the entering marker.
 */
- (void)business:(CEBusinessData *)data didEnterZone:(CEBusinessZone *)zone;

/** Notifies the delegate when a business exits a zone.
 
 @param data - The business data of the exiting marker.
 @param zone - The zone left.
 */
- (void)business:(CEBusinessData *)data didExitZone:(CEBusinessZone *)zone;

/** Notifies the delegate when a business is added to the map.
 
 @param data - The business data of the added business.
*/
- (void)businessAdded:(CEBusinessData *)data;

/** Notifies the delegate when a business is added to the map.
 
 @param data - The business data of the removed business.
 */
- (void)businessRemoved:(CEBusinessData *)data;

@end

typedef enum {
    CEBusinessSortDistance,
    CEBusinessSortVisibility,
    CEBusinessSortAlphabetical
} CEBusinessSortMethod;

@interface CEBusinessQuery : NSObject

@property (assign, nonatomic) CELonLatBounds bounds;
@property (assign, nonatomic) int limit;
@property (assign, nonatomic) int offset;
@property (assign, nonatomic) CEBusinessSortMethod sortMethod;

@end

@interface CEBusinessLayerOptions : CETileLayerOptions

@property (strong, nonatomic) NSString *imageHostname;
@property (strong, nonatomic) NSString *logoURL;
@property (strong, nonatomic) NSString *categoryURL;
@property (strong, nonatomic) NSMutableDictionary *businessAttachments;

@end

/** A Citymaps-specific layer.  Used to load and show businesses from the Citymaps database. Some control can be exercised over which businesses show up and how they display. 
 
 See CECitymapsTileLayer for a list of available options.
 
 */

@interface CEBusinessLayer : CETileLayer

/** 
 @name Initializers
*/

/**
Initialize with an API Key

@param apiKey Your Citymaps API Key
*/

- (id)initWithAPIKey:(NSString *)apiKey;

/** 
 * @name Business Management
 *  
 */

/** Apply a business filter to the map
 
 @param filter The filter to apply
 */
- (void)applyBusinessFilter:(CEBusinessFilter *)filter;

/** Remove any active business filter */
- (void)removeBusinessFilter;

/** Add an additional business to an active filter
 *
 * The business state may be one of the following values:
 *
 * - kCEBusinessStateNormal - The default state of businesses.  Set this to remove a business state.
 * - kCEBusinessState2X - Doubles the size of the business.
 * - kCEBusinessStateSelected - Puts a blue halo around the business.
 * - kCEBusinessStateHidden - Hides the business from view.
 *
 * @param data The business data for this business
 * @param state The state for this business
 */
- (void)addBusinessToActiveFilter:(CEBusinessData *)data state:(CEBusinessMarkerState)state;

/** Remove a business from an active filter
 *
 * @param bid The business ID to remove
 */
- (void)removeBusinessFromActiveFilter:(NSString *)bid;

/** Add a business ID to the blacklist.  Once a business is added to the blacklist, it will not show up for this session.
 *
 * @param bid The business ID to blacklist
 */
- (void)addBusinessToBlacklist:(NSString *)bid;

/** Remove a business ID from the blacklist.
 *
 * @param bid The business ID to remove from the blacklist.
 */
- (void)removeBusinessFromBlacklist:(NSString *)bid;

/** Set the state of a particular business.
 *
 * The business state may be one of the following values:
 *
 * - kCEBusinessStateNormal - The default state of businesses.  Set this to remove a business state.
 * - kCEBusinessState2X - Doubles the size of the business.
 * - kCEBusinessStateSelected - Puts a blue halo around the business.
 * - kCEBusinessStateHidden - Hides the business from view.
 *
 * @param bid The business ID
 * @param state The new state for this business
 */
- (void)setBusinessState:(NSString *)bid state:(CEBusinessMarkerState)state;

/** Reset the state of all businesses to kCEBusinessStateNormal */
- (void)resetBusinessStates;

/** Sets a marker group as an additional constraint to the business marker placement.
 @param group The marker group to add as a constraint.
 */
- (void)addMarkerGroupConstraint:(CEMarkerGroup *)group;

/** Removes a marker group constraint.
 @param group The marker group to remove as a constraint.
 */
- (void)removeMarkerGroupConstraint:(CEMarkerGroup *)group;

/** Removes all marker group constraints.
 */
- (void)removeAllMarkerGroupConstraints;
/** Attach a UIView to a business marker.
 
 @param attachment The view to attach to the business.
 @param bid The business id.
 */
- (void)setAttachment:(CEMarkerAttachmentView *)attachment forBusiness:(NSString *)bid;

/** Get the attachment for the given business ID if it exists.
 
 @param bid The business ID to search for.
 @return The attachment view if it exists, and nil if it doesnt.
 */
- (CEMarkerAttachmentView *)attachmentForBusiness:(NSString *)bid;

/** Remove any attachments from a business marker.
 
 @param bid The business id.
 
 */
- (void)removeAttachment:(NSString *)bid;

/** Removes all attachments from all businesses.
 */
- (void)removeAttachments;


/** 
 @name Marker Attachments
 */

/** Adds a marker as a child of a business marker. If a marker is added via this method, it must be removed by removeMarker:fromBusiness:. Do not call CEMarker's removeFromParent.
 * It is recommended that you do not add or keep children attached to businesses that are not on screen.
 @param marker The child to add.
 @param bid The business id to add the marker to.
 */
- (void)addMarker:(CEMarker*)marker toBusiness:(NSString*)bid;

/** Removes a child from a business marker.
 @param marker The child to remove.
 @param bid The business id to remove the child from. 
 */
- (void)removeMarker:(CEMarker*)marker fromBusiness:(NSString*)bid;

/** Removes all children from a specific business.
 @param bid The business to remove children from.
 */
- (void)removeAllMarkersFromBusiness:(NSString*)bid;

/** Remove all children from all businesses.
 */
- (void)removeAllMarkersFromAllBusinesses;

/** Returns a list of children for the specified business.
 @param bid The business to retrieve children for.
 */
- (NSArray*)markersForBusiness:(NSString*)bid;

/** Add a screen-space zone which will notify the delegate when a business enters or exits this region.
 
 @param zone The new zone
 */
- (void)addBusinessZone:(CEBusinessZone *)zone;

/** Remove a business zone, causing events to longer be generated for this zone.
 
 @param zone The zone to be removed
 */
- (void)removeBusinessZone:(CEBusinessZone *)zone;

/** Removes all business zone, causing no more events to be generated until a new one is added.
 */
- (void)removeAllBusinessZones;

/** Sets the visibility rating for a business. This rating should be a number between -100 and 100. The rating will be applied to the base visibility rating for this business. Higher rated businesses will appear first on the map.
 */
- (void)setVisibilityRating:(NSInteger)rating forBusiness:(NSString *)bid;

/** Gets the visibility rating for a business.
 */
- (NSInteger)getVisibilityRatingForBusiness:(NSString *)bid;

/** Clears all visibility ratings back to 0.
 */
- (void)clearVisibilityRatings;

/** Gets a list of all possible businesses that could show on the current screen. */
- (NSMutableArray *)allBusinessesInView;

- (BOOL)addBusiness:(CEBusinessData *)data withVisibility:(short)visibility;

/** Query the business layer based on a set of query criteria
 @param query The query parameters
 @return An array of CEBusinessData objects
 */
- (NSArray *)resultsForBusinessQuery:(CEBusinessQuery *)query;

/** 
 * @name Properties
 *  
 */

/** The delegate for the business layer */
@property (weak, nonatomic) id<CEBusinessDelegate> delegate;

/** Current active business Filter */
@property (strong, nonatomic) CEBusinessFilter *filter;

/** Current active busienss zones */
@property (strong, nonatomic) NSMutableArray *businessZones;

@property (assign, nonatomic) BOOL miniPinsEnabled;

@end
