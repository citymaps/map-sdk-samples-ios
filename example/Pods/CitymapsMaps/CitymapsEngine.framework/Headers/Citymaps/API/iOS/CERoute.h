//
//  CERoute.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <CitymapsEngine/Citymaps.h>

@interface CERoute : NSObject

@property (nonatomic, readonly, assign) CELonLat start;
@property (nonatomic, readonly, assign) CELonLat end;
@property (nonatomic, readonly, assign) CELonLat *points;
@property (nonatomic, readonly, assign) NSUInteger numPoints;
@property (nonatomic, readonly, strong) NSArray *instructions;
@property (nonatomic, readonly, assign) CGFloat distance;
@property (nonatomic, readonly, assign) NSInteger distanceSeconds;
@property (nonatomic, readonly, assign) NSUInteger mode;

@end
