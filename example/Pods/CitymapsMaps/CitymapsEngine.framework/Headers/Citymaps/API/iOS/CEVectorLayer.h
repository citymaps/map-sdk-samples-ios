#import <CitymapsEngine/Map/API/iOS/Layer/CETileLayer.h>
#import <CitymapsEngine/Citymaps/API/iOS/CERoutingTypes.h>

@class CERoutingRequest;
@class CERoute;
@class CERouteState;
@protocol CERoutingDelegate <NSObject>

@required

//- (void)navigationDidReroute:(CERoute *)route;
//- (void)navigationDidFinish:(BOOL)reachedDestination;

/** Called when the user should be notified of an instruction, either via voice or text. */
- (void)navigationDidSendNotification:(NSString *)notification;

/** Called when routing has started a new instruction. */
- (void)navigationDidBeginInstruction:(NSInteger)instruction;

/** Called periodically to update the overall state of routing. */
- (void)navigationDidUpdate:(CERouteState *)state;
@end

/** This is the base layer used by the Citymaps map view to draw the vector-based data received from the Citymaps tile server.
 
 See CECitymapsTileLayer for a list of available options.
 
 */

@interface CEVectorLayer : CETileLayer

/**
 @name Initializers
 */

/**
 Initialize with an API Key
 
 @param apiKey Your Citymaps API Key
 */

- (id)initWithAPIKey:(NSString *)apiKey;

/** Updates the map style from a file on the filesystem.
 * @param configFile The file to update the style from.
 */
- (void)updateStyleFromFile:(NSString*)configFile;

/** Updates the map style from an application resource.
 * @param configFile The resource to update the style from.
 */
- (void)updateStyleFromResource:(NSString*)configFile;

/** Updates the map style from an xml string.
 * @param configFile The xml string to update the style from.
 */
- (void)updateStyleFromString:(NSString *)xml;

- (void)startRoutingRequest:(CERoutingRequest *)request success:(CERoutingSuccessBlock)success failure:(CERoutingErrorBlock)failure;

- (void)startNavigationForRoute:(CERoute *)route;

@property (nonatomic, weak) id<CERoutingDelegate> routingDelegate;

@end
