//
//  RegionMarker.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Citymaps/Layer/Feature/RegionFeature.h>
#include <CitymapsEngine/Citymaps/Marker/CitymapsFeatureMarker.h>
#include <CitymapsEngine/Map/Marker/LabelMarker.h>

namespace citymaps
{
    static const Bounds kRegionEmbedRect(0, 0, 53, 59);
    static const Size kRegionPinSize(53, 59);
    static const Point kRegionPinAnchorPoint(0.5, 1.0);
    
    class Sprite;
    class RegionMarker : public CitymapsFeatureMarker
    {
    public:
        RegionMarker(const MARKER_DESC& desc = MARKER_DESC(), const LABEL_MARKER_DESC& labelDesc = LABEL_MARKER_DESC());
        ~RegionMarker();
        
        void RemoveAdditionalChildren();
        
        void SetRegionFeature(RegionFeature *feature);
        
        static void LoadGlobalImages();
        static void LoadGlobalSprites(IGraphicsDevice *device);
        
        void SetImage(std::shared_ptr<Image> image);
        
        const std::string& GetDownloadURL() { return mDownloadURL; }
        
        float GetPriority() const { return mPriority; }
        const Point& GetRegionPoint() const {return mRegionPoint;}
        
        LabelMarker& Label() { return mLabel;}
        
        static Image *GetRegionMask() {return sRegionMask;}
        static Image *GetRegionPin() {return sRegionPin;}
        
    protected:
        
        void OnRender(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        Size CalculateSize();
        
    private:
        uint64_t mGid;
        float mPriority;
        Point mRegionPoint;
        std::string mDownloadURL;
        Sprite *mRegionSprite;
        Sprite *mLoadingSprite;
        
        LabelMarker mLabel;
        
        static Image *sRegionPin;
        static Image *sRegionMask;
        static bool sGlobalImagesLoaded;
    };
};
