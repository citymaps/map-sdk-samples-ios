//
//  BusinessInfoMarker.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 11/7/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Marker/Marker.h>
#include <CitymapsEngine/Core/Graphics/Shape/Label.h>
#include <CitymapsEngine/Citymaps/Layer/Util/BusinessData.h>

namespace citymaps
{
    
    static const float kBusinessInfoMarkerMaxWidth = 55;
    class MeshShape;
    class BusinessInfoMarker : public Marker
    {
    public:
        BusinessInfoMarker();
        ~BusinessInfoMarker();
        
        Size CalculateSize();
        
        void SetBusinessData(const BusinessData &data);
        
        float GetContentWidth() const
        {
            return mWidth;
        }
        
    protected:
        
        void OnRender(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        void RenderDebug(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        
        std::u16string mBusinessName;
        std::u16string mCategoryName;
        float mStarRating;
        float mContentHeight;
        float mWidth;
        size_t mNumAdjacentBusinesses;
        
        Label *mTitleLabel;
        Label *mSubtitleLabel;
        Sprite *mStarRatingSprite;
        
        MeshShape *mDebugShape;
        
        float CalculateContentHeight();
        
        static void LoadGlobalSprites(IGraphicsDevice *device);
        
    };
}