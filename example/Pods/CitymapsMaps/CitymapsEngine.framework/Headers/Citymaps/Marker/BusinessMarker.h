//
//  BusinessMarker.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Citymaps/Layer/Feature/BusinessFeature.h>
#include <CitymapsEngine/Citymaps/Marker/CitymapsFeatureMarker.h>
#include <CitymapsEngine/Citymaps/Layer/Util/BusinessData.h>
#include <CitymapsEngine/Core/Graphics/Shape/Label.h>
#include <CitymapsEngine/Citymaps/Marker/BusinessInfoMarker.h>
#include <CitymapsEngine/Map/Marker/LabelMarker.h>
#include <CitymapsEngine/Map/Marker/ImageMarker.h>

namespace citymaps
{
    //static const Bounds kBusinessEmbedRectNormal(2, 2, 42, 42);
    static const Bounds kBusinessEmbedRectNormal(5, 7, 35, 38);
    static const float kBusinessCornerRadiusRegular = 10;
    
    static const Size kMiniPinSize(10, 12);
    static const Size kBusinessPinSize(40, 48);
    static const Point kBusinessPinAnchorPoint(0.5, 1.0);
    
    static const Size kCategoryLabelSize(38, 20);
    static const Point kCategoryLabelPosition(0, -20);
    
    static const float kRegularFactor = 1;
    static const float kHotFactor = 1;
    
    enum BusinessMarkerState
    {
        BusinessStateNormal,
        BusinessStateSelected,
    };
    
    class Sprite;
    class BusinessMarker : public CitymapsFeatureMarker
    {
    public:
        BusinessMarker(const MARKER_DESC& desc = MARKER_DESC());
        ~BusinessMarker();
        
        void RemoveAdditionalChildren();
        
        static Size GetMarkerSize(bool mini, bool hot, float scale)
        {
            if (mini)
                return kMiniPinSize;
            if (hot)
                return kBusinessPinSize * kHotFactor * scale;
            return kBusinessPinSize * kRegularFactor * scale;
        }
        
        virtual void SetLoadingState(MarkerLoadingState state);
        
        void SetBusinessFeature(BusinessFeature *feature);
        
        void ClearClusteredBidHashes()
        {
            mClusteredBidHashes.clear();
        }
        
        bool ContainsClusteredBidHash(uint64_t hash)
        {
            return mClusteredBidHashes.find(hash) != mClusteredBidHashes.end();
        }
        
        void AddClusteredBidHash(uint64_t hash)
        {
            mClusteredBidHashes.insert(hash);
        }
    
        bool RemoveClusteredBidHash(uint64_t hash)
        {
            bool erased = mClusteredBidHashes.erase(hash) == 1;
            return erased;
        }
        
        const std::set<uint64_t>& GetClusteredBidHashes() const
        {
            return mClusteredBidHashes;
        }
        
        void SetInfoMarkerVisible(bool visible, bool immediate = false);
        
        bool IsInfoMarkerVisible() const { return mInfoMarker->IsVisible();}
        
        const Bounds& GetInfoMarkerBounds() const
        {
            return mInfoMarker->GetScreenBounds();
        }
        
        const float GetInfoMarkerWidth() const
        {
            return mInfoMarker->GetContentWidth();
        }
        
        uint64_t GetBidHash() const
        {
            return mBidHash;
        }
        
        std::string GetDownloadURL();
        
        void SetImage(std::shared_ptr<Image> image);
        
        void SetMini(bool mini);
        bool IsMini() { return mMini; }
        
        void SetBusinessState(BusinessMarkerState state);
        
        BusinessMarkerState GetBusinessState() const { return mBusinessState; }
        
        void SetVisibilityRating(int rating) { mCustomVisibilityRating = rating;}
        int GetVisibilityRating() const { return (mCustomVisibilityRating) + mFeatureVisibilityRating; }
        bool IsHot() const { return mHot; }
        bool IsCategory() const { return mBusinessData.logoImageId == 0; }
        int GetLogoImageId() const { return mBusinessData.logoImageId; }
        
        const BusinessData& GetBusinessData();
        
        void ResetState();
        
        void SetDownloadURLs(const std::string& businessLogoURL, const std::string& businessCategoryURL)
        {
            mLogoURL = businessLogoURL;
            mCategoryURL = businessCategoryURL;
        }
        
        Size CalculateSize();
        
        static void LoadGlobalSprites(IGraphicsDevice *device);
        static void DestroyGlobalSprite();
        static void LoadGlobalImages();

        std::vector<BusinessData>& GetAllBusinesses() { return mAllBusinesses; }
     
    protected:
        
        void OnRender(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        
        uint64_t mBidHash;
        struct BusinessData mBusinessData;
        std::set<uint64_t> mClusteredBidHashes;
        std::vector<BusinessData> mAllBusinesses;
        int mFeatureVisibilityRating;
        int mCustomVisibilityRating;
        bool mHot;
        
        Sprite *mProxySprite;
        Sprite *mCategorySprite;
        Sprite *mPinSprite;
        Sprite* mLogoSprite;
        
        BusinessInfoMarker *mInfoMarker;
        class Label* mCategoryLabel;
        bool mUpdateLabel;

        size_t mClusterCount;
        
        // Temp solution for 3.7
        bool mHasDeal;
        //
        bool mMini;
        bool mUpdatePin;
        Vector4f mTintColor;
        
        BusinessMarkerState mBusinessState;
        
        std::string mLogoURL;
        std::string mCategoryURL;
        
        std::shared_ptr<Image> mPendingImage;
        
        void UpdateSprites(IGraphicsDevice *device);
        
        void UpdateImageSize(); 
        Bounds DetermineEmbedBounds();
        Size DetermineMarkerSize();
        void UpdateBusinessMarkerBounds(const Point& screenPos);
        void SetImageInternal(std::shared_ptr<Image> image);
        
        static std::shared_ptr<Image> sPinLogoRegularImage;
        static std::shared_ptr<Image> sPinLogoBuildingRegularImage;
        static std::shared_ptr<Image> sPinLogoSelectedImage;
        
        static bool sGlobalImagesLoaded;
    };
}


