//
//  StyleLayer.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Citymaps/Layer/Style/StyleData.h>
#include <rapidxml/rapidxml.hpp>

namespace citymaps
{
    typedef std::vector<StyleData> TStyleDataList;
    
    class StyleLayer
    {
        
    public:
        StyleLayer();
        StyleLayer(rapidxml::xml_node<> *xmlNode);
        ~StyleLayer() {}
        
        const StyleData* GetStyleDataForZoom(int zoom) const
        {
            return &mStyles[zoom];
        }
        
        /* Getters */
        const std::string& GetFont() const { return mFont; }
        const int GetFontSize() const { return mFontSize; }
        const Vector4& GetTextColor() const { return mTextColor; }
        const int GetLabelPriority() const { return mLabelPriority; }
        const bool IsTextAllCaps() const { return mTextAllCaps; }
        const int GetLayerId() const { return mLayerId; }
        const int GetMinZoom() const { return mMinZoom; }
        const int GetMaxZoom() const { return mMaxZoom; }
        const std::string& GetName() const { return mLayerName; }
        
        bool IsWithinZoomLevel(int zoom) const
        {
            return mMinZoom <= zoom && mMaxZoom >= zoom;
        }
        
        bool IsTextHiddenAtZoom(int zoom, MapFeatureBadgeType badgeType = MapFeatureBadgeNone) const
        {
            const StyleData* data = this->GetStyleDataForZoom(zoom);
            
            return (data->mTextHidden || data->mFontSize == 0 ||
                    (badgeType == MapFeatureBadgeNone && zoom < mLabelMinZoom) ||
                    (badgeType != MapFeatureBadgeNone && zoom < mBadgeMinZoom) ||
                    !this->IsWithinZoomLevel(zoom));
        }
        
        std::string mLayerName;
        int mMinZoom;
        int mMaxZoom;
        int mLabelMinZoom;
        int mBadgeMinZoom;
        short mLayerId;
        short mDelta;
        int mLabelPriority;
        std::string mFont;
        int mFontSize;
        Vector4 mTextColor;
        bool mTextShadow;
        bool mTextAllCaps;
        
        TStyleDataList mStyles;
        
    };
};
