//
//  MapConfiguration.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Citymaps/Layer/Style/StyleLayer.h>
#include <CitymapsEngine/Citymaps/Layer/Style/LabelStyle.h>

namespace citymaps
{
    class StyleLayer;
    
    typedef std::vector<StyleLayer> TStyleLayerList;
    typedef std::vector<LabelStyle> TLabelStyleList;
    
    class MapConfiguration
    {
        
    public:
        MapConfiguration(const char *configData, size_t length);
        MapConfiguration(const std::string& config) :
            MapConfiguration(config.c_str(), config.length())
        {
        }
        
        ~MapConfiguration() {}
        
        const bool IsReady() const { return mReady; }
        
        int GetNumLayers() const { return (int)mLayers.size(); }
        int GetIndexForLayerId (short layerId) const;
        const StyleLayer* GetStyleLayerForId (short layerId) const;
        const StyleLayer* GetStyleLayerByName (const std::string& name) const;
        
        const LabelStyle* GetLabelStyleForZoom(unsigned char zoom) const;
        
        TStyleLayerList mLayers;
        TLabelStyleList mLabelStyles;
        bool mReady;
    };
};
