//
//  StyleData.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <rapidxml/rapidxml.hpp>
#include <CitymapsEngine/Core/Font/FontManager.h>

namespace citymaps
{
    class StyleData
    {
        
    public:
        StyleData() {}
        StyleData(rapidxml::xml_node<> *xmlNode);
        ~StyleData() {}
        
        static Vector4f ParseColor(int encodedColor)
        {
            Vector4f colorVec;
            
            colorVec.r = ((0x00FF0000 & encodedColor) >> 16) / 255.0f;
            colorVec.g = ((0x0000FF00 & encodedColor) >> 8) / 255.0f;
            colorVec.b = (0x000000FF & encodedColor) / 255.0f;
            colorVec.a = 1.0f;
            
            return colorVec;
        }
        
        /* Getters */
        const int GetMinScale() const { return mMinScale; }
        const int GetMaxScale() const { return mMaxScale; }
        const Vector4f& GetStrokeColor() const { return mStrokeColor; }
        const float GetStrokeWidth() const { return mStrokeWidth; }
        const Vector4f& GetFillColor() const { return mFillColor; }
        const float GetFillWidth() const { return mFillWidth; }
        Font* GetFont() const
        {
            if (!mFont)
            {
                mFont = FontManager::GetFont(mFontFamily, mFontSize);
            }
            
            return mFont;
        }
        
        int mMinScale;
        int mMaxScale;
        int mZoom;
        Vector4f mStrokeColor;
        float mStrokeWidth;
        Vector4f mFillColor;
        float mFillWidth;
        float mAlpha;
        std::string mFontFamily;
        int mFontSize;
        Vector4f mTextColor;
        Vector4f mTextOutlineColor;
        int mTextOutlineWidth;
        bool mTextAllCaps;
        bool mTextHidden;
        
    private:
        mutable Font *mFont;
        
    };
}
