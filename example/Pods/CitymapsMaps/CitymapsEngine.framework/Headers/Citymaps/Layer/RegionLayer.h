//
//  RegionLayer.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/TileLayer.h>
#include <CitymapsEngine/Citymaps/Marker/RegionMarker.h>

namespace citymaps
{
    typedef std::unordered_map<uint64_t, std::shared_ptr<RegionMarker> > TRegionMarkerMap;
    
    class MarkerGroup;
    class CollisionGroup;
    
    template<typename T>
    class ObjectPool;
    
    template<typename T>
    class MemoryCache;
    
    static const std::string kCEDefaultRegionImageHostname = "r.citymaps.com";
    
    struct CITYMAPS_REGION_LAYER_DESC : TILE_LAYER_DESC
    {
        CITYMAPS_REGION_LAYER_DESC() :
        TILE_LAYER_DESC(),
        RegionImageHostname(kCEDefaultRegionImageHostname)
        {}
        
        std::string RegionImageHostname;
    };
    
    class LogoRepository;
    class RegionLayer : public TileLayer, public IMarkerListener
    {
    public:
        RegionLayer(CITYMAPS_REGION_LAYER_DESC desc);
        ~RegionLayer();
        
        void SetMap(Map *map);
        
        virtual void OnDisable();
        
        virtual void ResetTiles();
        
        bool RequiresUpdate(const MapState& state);
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        void SetVisible(bool visible);
        
        bool OnMarkerEvent(MarkerEvent event, Marker* marker);
        
        
    private:
        ObjectPool<RegionMarker> *mMarkerPool;
        LogoRepository *mLogoRepository;
        
        MarkerGroup *mMarkerGroup;
        CollisionGroup *mMarkerCollisionGroup;
        
        TRegionMarkerMap mActiveMarkers;
        TRegionMarkerMap mDisappearingMarkers;

        real mLastScale;
        std::string mImageHostname;
        
        void UpdateRegions(const MapState& state);
        
        RegionMarker* ShowRegion(RegionFeature *feature);
        void HideRegion(RegionFeature *feature);
        void HideRegion(uint64_t key);
        void ResetRegions();
        
    };
}
