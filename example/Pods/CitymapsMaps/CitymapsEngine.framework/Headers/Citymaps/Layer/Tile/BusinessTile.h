//
//  BusinessTile.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/Tile.h>
#include <CitymapsEngine/Citymaps/Layer/Feature/BusinessFeature.h>

namespace citymaps
{
    class BusinessLayer;
    
    class BusinessTile : public Tile
    {
    public:
        BusinessTile(GridPoint gridPoint, TileLayer *layer);
        ~BusinessTile();
        
        BusinessFeature* GetFeature(int index)
        {
            if(index < mFeatures.size()) {
                return &mFeatures[index];
            }
            
            return NULL;
        }
        
        int GetNumFeatures()
        {
            return mNumFeatures;
        }
        
        bool IsOnScreen(Map *map, const Bounds &logoZoneBounds);
        
    private:
        int mNumFeatures;
        std::vector<BusinessFeature> mFeatures;
        
        void SetData(const TByteArray &data);
        
    };
};
