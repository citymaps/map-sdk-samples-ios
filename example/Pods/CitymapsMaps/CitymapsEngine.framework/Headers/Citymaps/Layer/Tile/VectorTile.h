//
//  VectorTile.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/Tile.h>
#include <CitymapsEngine/Citymaps/Layer/Feature/MapFeature.h>
#include <CitymapsEngine/Core/Graphics/Shape/MeshShape.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasLine.h>

namespace citymaps
{
    struct VectorLineVertex
    {
        float position[2];
        float perp[2];
        float layerId;
        float u;
    };

    struct VectorPolygonVertex
    {
        float position[2];
        float texCoord[2];
        float layerId;
    };
    
    struct TileLabelFeature
    {
        int featureId;
        MapFeature* feature;
        int segmentId;
        Point location;
        float minZoom;
        float maxZoom;
    };
    
    struct TileArrowFeature
    {
        int featureId;
        int segmentId;
        Point segment[2];
        Point position;
        float minZoom;
        int index;
        uint64_t tile;
        int direction;
    };
    
    class VectorLayer;
    class Sprite;
    
    class VectorTile : public Tile
    {
    public:
        
        VectorTile(GridPoint gridPoint, TileLayer *layer, std::shared_ptr<MapConfiguration> configuration);
        ~VectorTile();
        
        size_t GetNumFeatures() { return mFeatures.size(); }
        size_t GetNumLabelFeatures() const { return mTileLabelFeatures.size(); }
        size_t GetNumArrowFeatures() const { return mTileArrowFeatures.size(); }
        
        MapFeature* GetFeature(int index)
        {
            if(index < mFeatures.size())
            {
                return &mFeatures[index];
            }
            
            return NULL;
        }
        
        MapFeature* GetSortedFeature(int index)
        {
            if(index < mSortedFeatures.size())
            {
                return mSortedFeatures[index];
            }
            
            return NULL;
        }

        TileLabelFeature* GetTileLabelFeature(unsigned int index)
        {
            if (index < mTileLabelFeatures.size())
            {
                return &mTileLabelFeatures[index];
            }
            
            return NULL;
        }
        
        TileArrowFeature* GetTileArrowFeature(unsigned int index)
        {
            if (index < mTileArrowFeatures.size())
            {
                return &mTileArrowFeatures[index];
            }
            
            return NULL;
        }
        
        bool IsOneWaySegment(int featureId)
        {
            return mOneWaySegments.find(featureId) != mOneWaySegments.end();
        }
        
        bool IsCacheable() { return mCacheable;}
        
        void RenderPolygons(IGraphicsDevice *device, RenderState &state);
        void RenderLines(IGraphicsDevice *device, RenderState &state);
        
        void SetGridPoint(const GridPoint& point);
        
        bool HasRendered() { return mHasRendered; }
        void SetRendered(bool rendered) { mHasRendered = rendered; }
        
        void GetFeaturesWithinRadius(const Point &position, double radius, std::vector<MapFeature *> &outFeatures);
        
    private:

        std::vector<MapFeature> mFeatures;
        std::vector<MapFeature*> mSortedFeatures;
        std::vector<TileLabelFeature> mTileLabelFeatures;
        std::vector<TileArrowFeature> mTileArrowFeatures;
        std::set<int> mOneWaySegments;
        
        MeshShape *mLineMesh;
        std::vector<MeshShape *> mPolygonMesh;
        Point mTileOrigin;
        std::shared_ptr<MapConfiguration> mConfiguration;
        VectorLayer *mVectorLayer;
        bool mCacheable;
        
        std::vector<VectorLineVertex> mLineVerts;
        std::vector<std::vector<VectorPolygonVertex> > mPolyVerts;
        std::vector<std::vector<uint16_t> > mPolyIndices;
        
        bool mHasRendered;
        
        void SetData(const TByteArray &data);
    };
};
