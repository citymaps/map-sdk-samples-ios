//
//  ArrowFeature.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 2/10/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Shape/Sprite.h>
#include <CitymapsEngine/Map/Map.h>
#include <CitymapsEngine/Core/Physics/Shapes/AABBShape.h>
#include <CitymapsEngine/Citymaps/Layer/Tile/VectorTile.h>

namespace citymaps
{
    static const Size kArrowSize(80, 50);
    

    class ArrowFeature
    {
    public:
        ArrowFeature();
        ~ArrowFeature();
        
        void SetFeature(TileArrowFeature *feature);
        
        bool IsVisible()
        {
            return mVisible;
        }
        
        void SetVisible(bool visible)
        {
            mVisible = visible;
        }
        
        bool IsRenderable()
        {
            return mAlpha > 0.0f;
        }
        
        bool IsOnScreen()
        {
            return mOnScreen;
        }
        
        double MinDistanceFromGroup(const std::vector<std::shared_ptr<ArrowFeature>> &features);
        double MinDistance2FromGroup(const std::vector<std::shared_ptr<ArrowFeature>>& features);
        
        const Point& GetPosition() { return mData.position; }
        
        float GetMinZoom() { return mData.minZoom; }
        
        AABBShape* GetBounds() { return &mBounds; }
        
        void Update(Map *map, const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        static AABBShape CalculateBounds(const Point &point)
        {
            real halfWidth = kArrowSize.width * 0.5;
            real halfHeight = kArrowSize.height * 0.5;
            
            AABBShape shape(Vector3(), Vector3(halfWidth, halfHeight, 0));
            shape.SetPosition(Vector3(point.x, point.y, 0));
            
            return shape;
        }
        
        bool operator==(TileArrowFeature *feature)
        {
            return feature->tile == mData.tile &&
            feature->index == mData.index;
        }
        
        bool IsVisibleAtZoom(int zoom)
        {
            return mData.minZoom <= zoom;
        }
        
        float GetAlpha() { return mAlpha; }
        
    private:
        AABBShape mBounds;
        Point mProjPoint;
        real mAngle;
        bool mVisible;
        int mDirection;
        float mAlpha;
        bool mOnScreen;
        TileArrowFeature mData;
        
        static Sprite *msArrowSprite;
        
        void UpdateAlpha(const MapState& state);
    };
};
