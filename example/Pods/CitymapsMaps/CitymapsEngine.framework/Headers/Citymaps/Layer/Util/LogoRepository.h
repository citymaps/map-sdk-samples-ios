//
//  LogoRepository.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/24/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Network/HTTPConnection.h>
#include <CitymapsEngine/Core/Util/Image.h>
#include <CitymapsEngine/Core/Util/MemoryCache.h>
#include <CitymapsEngine/Core/Util/ObjectPool.h>
#include <CitymapsEngine/Core/Util/SafeObject.h>
#include <CitymapsEngine/Map/Map.h>

namespace citymaps
{
    typedef SafeObject<std::map<uint64_t, std::shared_ptr<HTTPConnection>>>  TLogoDownloadOpMap;
    typedef SafeObject<std::map<uint64_t, std::shared_ptr<TByteArray>>>  TLogoDataMap;
    class LogoRepository
    {
    public:
        LogoRepository(Map* map);
        ~LogoRepository();
        
        std::shared_ptr<Image> GetImage(uint64_t key);
        void StartDownload(const std::string &url, uint64_t key);
        bool IsDownloading(uint64_t key);
        void CancelDownload(uint64_t key);
        void CancelDownloads();
        
    private:
        MemoryCache<std::shared_ptr<Image>> mImageCache;
        //SafeObject<std::map<uint64_t, Image *>> mImageCache;
        TLogoDownloadOpMap mDownloadOps;
        TLogoDataMap mLogoData;
        
        // Who to invalidate when a download finishes (complete or failed)
        Map* mMap;
        
        void DownloadComplete(std::shared_ptr<TByteArray> data, uint64_t key);
        void DownloadFailed(NetworkResponseStatus status, uint64_t key, const std::string &uri);
    };
};