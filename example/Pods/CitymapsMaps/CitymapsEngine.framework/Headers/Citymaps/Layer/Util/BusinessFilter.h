//
//  BusinessFilter.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Citymaps/Layer/Feature/BusinessFeature.h>
#include <CitymapsEngine/Citymaps/Marker/BusinessMarker.h>

namespace citymaps
{
    struct BusinessFilterObject
    {
        BusinessFeature feature;
        BusinessMarkerState state = BusinessStateNormal;
    };
    
    typedef std::vector<BusinessFilterObject> TBusinessFilterObjectList;
    
    class BusinessFilter
    {
    public:
        BusinessFilter();
        BusinessFilter(const BusinessFilter &other);
        ~BusinessFilter();
        
        void AddBusiness(const BusinessData &data, BusinessMarkerState state);
        void AddBusiness(const BusinessFilterObject &obj);
        void RemoveBusiness(const std::string &bid);
        
        bool operator==(const BusinessFilter &other);
        
        TBusinessFilterObjectList& GetBusinesses() { return mBusinesses; }
        
        bool IsApplied() { return mApplied; }
        void SetApplied(bool status) { mApplied = status; }
        
        TBusinessFilterObjectList::iterator begin() { return mBusinesses.begin(); }
        TBusinessFilterObjectList::iterator end() { return mBusinesses.end(); }
        TBusinessFilterObjectList::const_iterator cbegin() { return mBusinesses.cbegin(); }
        TBusinessFilterObjectList::const_iterator cend() { return mBusinesses.cend(); }
        
    private:
        TBusinessFilterObjectList mBusinesses;
        bool mApplied;
    };
};
