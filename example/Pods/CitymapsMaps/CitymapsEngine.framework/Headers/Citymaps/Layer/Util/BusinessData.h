//
//  BusinessData.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <string>
#include <CitymapsEngine/Core/EngineTypes.h>

namespace citymaps
{
    struct BusinessData
    {
        std::string businessId = "";
        std::u16string name = u"";
        std::u16string address = u"";
        std::u16string city = u"";
        std::u16string state = u"";
        std::u16string country = u"";
        std::string zip = "";
        std::string phone = "";
        std::string categoryName = "";
        int logoImageId = 0;
        int categoryIconId = 0;
        int analyticsPartner = 0;
        int category = 0;
        float starRating = 0;
        
        // Location in lon / lat
        Point location = Point(0,0);
        
        std::vector<BusinessData> adjacentBusinesses;
    };
}