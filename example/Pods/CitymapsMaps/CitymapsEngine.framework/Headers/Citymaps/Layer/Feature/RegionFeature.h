//
//  RegionFeature.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <rapidjson/document.h>

namespace citymaps
{
    class Map;
    class RegionFeature
    {
        friend class RegionTile;
        friend class RegionMarker;
        friend class RegionLayer;
        
    public:
        RegionFeature() :
            mGid(0),
            mPriority(0)
        {
        }
        
        ~RegionFeature()
        {
        }
        
        void FromJSON(rapidjson::Value &object, Map *map);
        uint64_t GetGid() { return mGid; }
        const Point& GetPoint() { return mPoint; }
        float GetPriority() { return mPriority; }
        
    private:
        std::u16string mName;
        uint64_t mGid;
        float mPriority;
        int mRegionType;
        Point mPoint;
        Point mLonLat;
        std::string mImageURL;
    };
};
