//
//  CitymapsDataSource.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/WebDataSource.h>
#include <Poco/Crypto/RSAKey.h>

namespace citymaps
{
    struct CITYMAPS_DATA_SOURCE_DESC : public WEB_DATA_SOURCE_DESC
    {
        int CacheVersion;
        std::string APIKey;
        
        bool SignedURLs = true;
        std::string SignedURLPattern;
    };
    
    enum APIValidationStatus
    {
        APIValidationStatusNone,
        APIValidationStatusApproving,
        APIValidationStatusApproved,
        APIValidationStatusDenied
    };
    
    class CLOUDFRONT_SIGNED_URL_DESC;
    class Archive;
    
    class CitymapsDataSource : public WebDataSource
    {
        
    public:
        CitymapsDataSource(const CITYMAPS_DATA_SOURCE_DESC& desc);
        ~CitymapsDataSource()
        {
        }
        
        void SetUserId(const std::string &userId)
        {
            if (userId.length() > 0) {
                mUserId = "/" + userId;
            } else {
                mUserId = "";
            }
        }
        
        void SetAPIKey(const std::string &apiKey)
        {
            mAPIKey = apiKey;
        }
        
        bool IsReady();
        
    private:
        CITYMAPS_DATA_SOURCE_DESC mDesc;
        std::string mUserId;
        int mCacheVersion;
        std::string mAPIKey;
        APIValidationStatus mStatus;
        bool mCloudfrontKeysLoaded;
        double mNextValidationRetryTime;
        int mValidationRequestCount;
        
        std::unique_ptr<HTTPConnection> mValidationConn;
        
        static std::unique_ptr<HTTPConnection> msKeypairConn;
        static Archive *msKeypairArchive;
        static std::mutex msCloudfrontMutex;

        void DownloadKeypair();
        void PrepareForRetry();
    };
}