//
//  CitymapsDataSource.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/DataSource.h>
#include <Poco/Crypto/RSAKey.h>

namespace citymaps
{
    struct CITYMAPS_TILE_DESC
    {
        int CacheVersion = 30;
    };
    
    class CitymapsPathTransformer : public IPathTransformer
    {
    public:
        
        CitymapsPathTransformer(const CITYMAPS_TILE_DESC& desc);
        ~CitymapsPathTransformer()
        {
        }
        
        std::string GeneratePath(const GridPoint& cell, const std::string uri);
        std::string GenerateCacheFilename(const std::string &url)
        {
            return url;
        }
        
        void SetUserId(const std::string &userId)
        {
            if (userId.length() > 0) {
                mUserId = "/" + userId;
            } else {
                mUserId = "";
            }
        }
        
    private:
        std::string mUserId;
        int mCacheVersion;
        Poco::Crypto::RSAKey *mKey;
        std::string mSignature;
        std::time_t mCloudfrontExpiration;
        std::mutex mRefreshLock;
        std::string mCloudfrontParams;
        std::string mBaseURL;
    
        void RefreshCloudfrontSignature();
        
        std::string ToCloudfrontSafeString(std::string &str);
    };
}