//
//  VectorTileFactory.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/TileFactory.h>
#include <CitymapsEngine/Citymaps/Layer/Tile/VectorTile.h>
#include <CitymapsEngine/Citymaps/Layer/Style/MapConfiguration.h>

namespace citymaps
{
    class VectorTileFactory : public BaseTileFactory
    {
    public:
        
        VectorTileFactory(const std::string& apiKey = "", const std::string& style = "");

        void LoadMapConfig();
        
        std::shared_ptr<MapConfiguration> GetConfiguration();
        
        void UpdateMapConfigFromFile(const std::string& configPath);
        
        void UpdateMapConfigFromResource(const std::string& configPath);
        
        void UpdateMapConfigFromString(const std::string& xmlString);
        
    protected:
        
        std::shared_ptr<Tile> AllocateTile(const GridPoint& gp, TileLayer* layer)
        {
            auto config = this->GetConfiguration();
            return std::shared_ptr<VectorTile>(new VectorTile(gp, layer, config));
        }
        
        void CreateMapConfig(const TByteArray& data)
        {
            this->CreateMapConfig(std::string(data.begin(), data.end()));
        }
        
        void CreateMapConfig(const std::string& data)
        {
            auto config = std::make_shared<MapConfiguration>(data);
            mConfig = config;
        }
	std::string GeneratePath();
        
    private:

        std::string mApiKey;
        std::string mStyle;
        std::shared_ptr<MapConfiguration> mConfig;
        std::mutex mConfigLoaderMutex;
        
        std::shared_ptr<HTTPConnection> mStyleConnection;
        int mLoadingAttempts;
        int mLocalLoadingAttempts;
        
        static const std::string sStyleURL;
        static const std::string sDefaultStyleURL;
        
        void LoadLocalConfig();
    };
}
