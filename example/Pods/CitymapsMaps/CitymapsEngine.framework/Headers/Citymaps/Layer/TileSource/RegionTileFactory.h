//
//  RegionTileFactory.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/TileFactory.h>
#include <CitymapsEngine/Citymaps/Layer/Tile/RegionTile.h>

namespace citymaps
{
    class RegionTileFactory : public BaseTileFactory
    {
        
    protected:
        
        std::shared_ptr<Tile> AllocateTile(const GridPoint& gp, TileLayer* layer)
        {
            return std::make_shared<RegionTile>(gp, layer);
        }
    };
}