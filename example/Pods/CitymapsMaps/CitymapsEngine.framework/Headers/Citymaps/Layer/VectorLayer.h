//
//  VectorLayer.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/TileLayer.h>
#include <CitymapsEngine/Citymaps/Layer/Style/MapConfiguration.h>
#include <CitymapsEngine/Citymaps/Layer/VectorLabelFeatureLayer.h>
#include <CitymapsEngine/Citymaps/Routing/RoutingController.h>

namespace citymaps
{
    class VectorLabelFeatureLayer;
    class MapConfiguration;
    class VectorLayerNode;
    class Map;
    
    class VectorLayer : public TileLayer
    {
        friend class LabelProcessor;
        
    public:
        VectorLayer(TILE_LAYER_DESC &layerDesc);
        ~VectorLayer();
        
        void SetMap(Map *map);
        virtual bool RequiresUpdate(const MapState& state);
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        const double GetResolution() const { return mResolution; }
        const double GetCurrentTileLength() const { return mResolution * this->GetTileSize(); }
        const short GetMissingId() { return mMissingId; }
        
        void ForceRenderDataUpdate()
        {
            this->Invalidate();
            mForceRenderDataUpdate = true;
        }

        virtual void ResetTiles()
        {
            TileLayer::ResetTiles();
            this->ResetLabels();
        }
        
        void ResetLabels()
        {
            DispatchOnMainThread([this]()
             {
                 mLabelLayer->ResetLabels();
             });
        }
        
        void GetFeaturesWithinRadius(const Point &position, double radius, std::vector<MapFeature *> &outFeatures);
        
        void SetRoutingListener(IRoutingControllerListener *listener)
        {
            mRoutingController.SetListener(listener);
        }
        
        void StartRoutingRequest(const RoutingRequest& request, TRouteSuccess success, TRouteError error)
        {
            mRoutingController.StartRoutingRequest(request, success, error);
        }
        
        void StartRouting(Route *route)
        {
            mRoutingController.StartRoute(route);
        }
        
        void CancelRouting()
        {
            mRoutingController.CancelRouting();
        }
        
    protected:
        
        virtual void AddTile(std::shared_ptr<Tile> tile, uint64_t key)
        {
            TileLayer::AddTile(tile, key);
            DispatchOnMainThread([this]()
             {
                mLabelLayer->ForceLabelReset();
             });
        }
        
        virtual void RemoveTile(uint64_t key)
        {
            TileLayer::RemoveTile(key);
            DispatchOnMainThread([this]()
             {
                 mLabelLayer->ForceLabelReset();
             });
        }
        
    private:
    
        int mVectorZoom;
        double mResolution;
        short mMissingId;
        
        int mColorIndex;
        int mWidthIndex;
        
        bool mInitIndices;
        int mMillisecondsSinceLabelUpdate;
        double mLastUsedResolution;
        bool mForceRenderDataUpdate;
        
        Vector4f mFillColorData[75];
        Vector4f mOutlineColorData[75];
        float mFillWidthData[75];
        float mOutlineWidthData[75];
        
        void UpdateRenderData(const MapState& state);
        std::shared_ptr<MapConfiguration> GetMapConfiguration();
        std::shared_ptr<VectorLabelFeatureLayer> mLabelLayer;
        
        RoutingController mRoutingController;

    };
};
