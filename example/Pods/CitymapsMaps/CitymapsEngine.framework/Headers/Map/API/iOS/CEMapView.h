//
//  CEMapView.h
//  vectormap2
//
//  Created by Lion User on 04/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CitymapsEngine/Core/Platform/iOS/CEWindowIOS.h>
#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>
#import <CitymapsEngine/Map/API/iOS/CEMarker.h>
#import <CitymapsEngine/Map/API/iOS/CEMarkerGroup.h>
#import <CitymapsEngine/Map/API/iOS/CERegion.h>
#import <CitymapsEngine/Map/API/iOS/Layer/CanvasFeatures/CEFeatureGroup.h>

typedef void (^CEAnimCompletionBlock)(BOOL completed);
static const CEAnimCompletionBlock kEmptyAnimCompletionBlock = ^(BOOL x){};

@class CEMapView;
@class CELayer;

/** 
 *Delegate for events which occur on the map.
 *
 *All of these events will be triggered on the main thread.
 *
 * These events are only triggered by user interactions with the map. Animations performed on the map by the developer will not cause these delegate methods to trigger.
 */
@protocol CEMapViewDelegate <NSObject>
@optional

/** Notifies the delegate when the map began moving.  Responds only to user interaction.
 *@param map - The map view calling the delegate.
 */
- (void)mapDidBeginMoving:(CEMapView *)map;

/** Notifies the delegate when the map has stopped moving.  Responds only to user interaction.
 *@param map - The map view calling the delegate.
 */
- (void)mapWillEndMoving:(CEMapView *)map;

/** Notifies the delegate when the map began zooming.  Responds only to user interaction.
 *@param map - The map view calling the delegate.
 */
- (void)mapDidBeginZooming:(CEMapView *)map;

/** Notifies the delegate when the map has stopped zooming.  Responds only to user interaction.
 *@param map - The map view calling the delegate.
 */
- (void)mapWillEndZooming:(CEMapView *)map;

/** Notifies the delegate of a touch up event on the map.
 *@param map - The map view calling the delegate.
 *@param point - The location of the touch.
 */
- (void)mapTouchUp:(CEMapView *)map atPoint:(CELonLat)point;

/** Notifies the delegate of a touch down event on the map.
 *@param map - The map view calling the delegate.
 *@param point - The location of the touch.
 */
- (void)mapTouchDown:(CEMapView *)map atPoint:(CELonLat)point;

/** Notifies the delegate of a tap on the map.
 *@param map - The map view calling the delegate.
 *@param point - The location of the tap.
 */
- (void)mapTapped:(CEMapView *)map atPoint:(CELonLat)point;

/** Notifies the delegate of a double tap on the map.
 *@param map - The map view calling the delegate.
 *@param point - The location of the tap.
 */
- (void)mapDoubleTapped:(CEMapView *)map atPoint:(CELonLat)point;

/** Notifies the delegate of a long press on the map.
 *@param map - The map view calling the delegate.
 *@param point - The location of the touch.
 */
- (void)mapLongPressed:(CEMapView *)map atPoint:(CELonLat)point;

/** Notifies the delegate when the map begins rotating
 *@param map - The map view calling the delegate
 *@param orientation - The map's current orientation
 */
- (void)mapWillBeginRotating:(float)orientation map:(CEMapView *)map;

/** Notifies the delegate when the map's orientation changes.
 *@param map - The map view calling the delegate.
 *@param orientation - The new map orientation.
 */
- (void)mapOrientationDidChange:(float)orientation map:(CEMapView *)map;

/** Notifies the delegate when the map stops rotating
 *@param map - The map view calling the delegate
 *@param orientation - The map's current orientation
 */
- (void)mapDidEndRotating:(float)orientation map:(CEMapView *)map;

/** Notifies the delegate when the map begins tilting
 *@param map - The map view calling the delegate
 *@param tilt - The map's current tilt
 */
- (void)mapWillBeginTilting:(float)tilt map:(CEMapView *)map;

/** Notifies the delegate when the map's tilt changes.
 *@param map - The map view calling the delegate.
 *@param tilt - The new map tilt.
 */
- (void)mapTiltDidChange:(float)tilt map:(CEMapView *)map;

/** Notifies the delegate when the map stops tilting
 *@param map - The map view calling the delegate
 *@param tilt - The map's current tilt
 */
- (void)mapDidEndTilting:(float)tilt map:(CEMapView *)map;

/** Notifies the delegate when the user's location changes.
 *@param newLocation - The new GPS information for this user.
 *@param map - The map view calling the delegate.
 *@param error - Contains any possible errors that occurred while fetching the GPS location.
 */
- (void)locationDidUpdate:(CEUserLocation)newLocation map:(CEMapView *)map error:(NSError *)error;

/** Notifies the delegate when the user's location authorization status changes.
 
 @param status - The new location authorization status.
 */
- (void)locationAuthorizationStatusDidChange:(CLAuthorizationStatus)status;

/** Notifies the delegate when the user enters a region.
 
 @param region - The region the user has entered.
 @param map - The map view calling the delegate.
 */
- (void)didEnterRegion:(CERegion *)region map:(CEMapView *)map;

/** Notifies the delegate when the user exits a region.
 
 @param region - The region the user has exited.
 @param map - The map view calling the delegate.
 */
- (void)didExitRegion:(CERegion *)region map:(CEMapView *)map;

@end

typedef enum {
    kCETrackingModeNone,
    kCETrackingModePosition,
    kCETrackingModePositionOrientation
} CETrackingMode;

@interface CEMapViewOptions : NSObject

@property (nonatomic, assign) CEMapPosition position;
@property (nonatomic, assign) float minZoom;
@property (nonatomic, assign) float maxZoom;
@property (nonatomic, assign) CGSize size;

@end

/**
 This is the main class for representing a map within the view heirarchy.
 */

@interface CEMapView : CEWindowIOS {
    NSMutableArray *_regions;
}

/** 
 * @name Initializers
 *  
 */

/** Initialize a new map
 
 @param frame Frame of the map view.
 @param options An immutable dictionary of options for the map. Accepted options are:
 
 - center: The default center of the map.
 - zoom: The default zoom of the map.
 - size: The pixel size of the map.
 - minZoom: The lower bound (most zoomed out) the map can zoom to.
 - maxZoom: The upper bound (most zoomed in) the map can zoom to.
 
 @return New map instance.
 */

- (id)initWithFrame:(CGRect)frame options:(CEMapViewOptions *)options;

/** 
 * @name Layers
 *  
 */

/** Set the base layer for the map. This layer is always rendered first.
 *
 * @param layer Layer to become the base layer. It should not already be added to the map.
 */
- (void)setBaseLayer:(CELayer *)layer;

/** Add a layer to the map
 
 @param layer The layer to be added.
 */
- (void)addLayer:(CELayer *)layer;

/** Remove a layer to the map
 
 @param layer The layer to be removed.
 */
- (void)removeLayer:(CELayer *)layer;

/** 
 * @name Map State
 *  
 */

/** Move the map to the given screen position.  The map will be moved to the latitude/longitude location currently occupying the given pixel.
 @param position The screen pixel.
 @param duration Duration of the animation for this movement in seconds.
 */
- (void)panTo:(CGPoint)position withDuration:(double)duration;

/** Move the map to the given screen position.  The map will be moved to the latitude/longitude location currently occupying the given pixel.
 @param position The screen pixel.
 @param duration Duration of the animation for this movement in seconds.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)panTo:(CGPoint)position withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Move the map by a given pixel amount.
 @param pixelDelta The pixel delta.
 @param duration Duration of the animation for this movement in seconds.
 */
- (void)panBy:(CGPoint)pixelDelta withDuration:(double)duration;

/** Move the map by a given pixel amount.
 @param pixelDelta The pixel delta.
 @param duration Duration of the animation for this movement in seconds.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)panBy:(CGPoint)pixelDelta withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Move the map by a given pixel amount.
 @param from The pixel to pan from.
 @param to The pixel to pan to.
 @param duration Duration of the animation for this movement in seconds.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)panPixel:(CGPoint)from toPixel:(CGPoint)to withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Set the current map center, with the option of animation.
 
 @param center The new map center.
 @param duration Duration of the animation for this movement in seconds.
 */
- (void)setMapCenter:(CELonLat)center withDuration:(double)duration;

/** Set the current map center, with the option of animation and completion block.
 
 @param center The new map center.
 @param duration Duration of the animation for this movement in seconds.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)setMapCenter:(CELonLat)center withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Set the current map center and zoom level.
 
 @param center The new map center.
 @param zoom The new map zoom level.
 */
- (void)setMapCenter:(CELonLat)center andZoom:(float)zoom;

/** Set the current map center and zoom level, with the option of animation.
 
 @param center The new map center.
 @param zoom The new map zoom level.
 @param duration Duration of the animation for this movement in seconds.
 */
- (void)setMapCenter:(CELonLat)center andZoom:(float)zoom withDuration:(double)duration;

/** Set the current map center and zoom level, with the option of animation and a completion block.
 
 @param center The new map center.
 @param zoom The new map zoom level.
 @param duration Duration of the animation for this movement in seconds.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)setMapCenter:(CELonLat)center andZoom:(float)zoom withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Set the current map orientation, with the option of animation.
 
 @param orientation The new map orientation.
 @param duration Duration of the animation for this movement in seconds.
 */
- (void)setOrientation:(double)orientation withDuration:(double)duration;

/** Set the current map orientation, with the option of animation and a completion block.
 
 @param orientation The new map orientation.
 @param duration Duration of the animation for this movement in seconds.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)setOrientation:(double)orientation withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

- (void)setMapPosition:(CEMapPosition)position withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Reset the map's orientation back to true north. */
- (void)resetOrientation;
//- (void)panBy:(CGPoint)delta;

/** Zoom the map in one zoom level.  This will always animate unless map animations are disabled. */
- (void)zoomIn;

/** Zoom the map out one zoom level.  This will always animate unless map animations are disabled. */
- (void)zoomOut;

/** Set the current map zoom level, with the option of animation.
 
 @param zoom - The new zoom level.
 @param duration Duration of the animation for this movement in seconds.
 */
- (void)setZoom:(float)zoom withDuration:(double)duration;

/** Set the current map zoom level, with the option of animation and a completion block.
 
 @param zoom The new zoom level.
 @param duration Duration of the animation for this movement in seconds.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)setZoom:(float)zoom withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Zoom the map so that the screen fully contains the bounds specified.
 
 @param bounds The new bounds of the map.
 */
- (void)zoomToBounds:(CELonLatBounds)bounds;

/** Zoom the map so that the screen fully contains the bounds specified, with the option of animation.
 
 @param bounds The new bounds of the map.
 @param duration Duration of the animation for this movement in seconds.
 */
- (void)zoomToBounds:(CELonLatBounds)bounds withDuration:(double)duration;

/** Zoom the map so that the screen fully contains the bounds specified, with the option of animation and a completion block.
 
 @param bounds The new bounds of the map.
 @param duration Duration of the animation for this movement in seconds.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)zoomToBounds:(CELonLatBounds)bounds withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Zoom the map towards the given lon lat one zoom level.
 @param lonLat The position to move towards.
 */
- (void)zoomTowards:(CELonLat)lonLat;

/** Zoom the map towards the given lon lat one zoom level.
 @param lonLat The position to move towards.
 @param duration The duration of the animation.
 */
- (void)zoomTowards:(CELonLat)lonLat withDuration:(double)duration;

/** Zoom the map towards the given lon lat one zoom level.
 @param lonLat The position to move towards.
 @param duration The duration of the animation.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)zoomTowards:(CELonLat)lonLat withDuration:(double)duration completion:(CEAnimCompletionBlock)completed;

/** Zoom the map towards the given lon lat one zoom level.
 @param lonLat The position to move towards.
 @param duration The duration of the animation.
 @param zoomDelta The amount of zoom to change.
 @param completed A completion block which will be called when the animation finishes.
 */
- (void)zoomTowards:(CELonLat)lonLat withDuration:(double)duration zoomDelta:(double)zoomDelta completion:(CEAnimCompletionBlock)completed;

/** Set the map's most zoomed out level.  If the map is zoomed out beyond this point, it will be zoomed in to this new min zoom level.
 
 @param minZoom The new most zoomed out level for this map.
 */
- (void)setMinZoom:(float)minZoom;

/** Set the map's most zoomed in level.  If the map is zoomed in beyond this point, it will be zoomed out to this new max zoom level.
 
 @param maxZoom The new most zoomed in level for this map.
 */
- (void)setMaxZoom:(float)maxZoom;

/** Gets the scale (meters per inch) at the given zoom level.
 * @param zoom The zoom to request scale for.
 */
- (double)scaleForZoom:(float)zoom;

/**
 * @name Canvas
 *
 */

/** Add a feature to the default canvas layer
 *
 *@param feature - The feature to add
 */
- (void)addFeature:(CEFeature *)feature;

/** Remove a feature from the default canvas layer
 *
 *@param feature - The feature to remove
 */
- (void)removeFeature:(CEFeature *)feature;

/** Remove all features from the default canvas layer */
- (void)removeAllFeatures;

/** Get a feature group with the given name from the default canvas layer.  If it does not exist, it will be created.
 *
 *@param name - The name of the feature group
 */
- (CEFeatureGroup*)featureGroupWithName:(NSString*)name;

/** 
 * @name Markers
 *  
 */

/** Get the marker group with the specified name.  If it does not exist, it will be created.
 
 @param name The name of the marker group.
 @return The requested marker group.
 */
- (CEMarkerGroup*)markerGroupWithName:(NSString*)name;

/** Add a marker to the default marker group.
 
 @param marker The marker to be added.
 */
- (void)addMarker:(CEMarker *)marker;

/** Remove a marker to the default marker group.
 
 @param marker The marker to be removed.
 */
- (void)removeMarker:(CEMarker *)marker;

/** 
 * @name Geofencing
 *  
 */

/** Add a new tracking region.  When the user's GPS location enters or existsone of these regions, the map's delegate will be notified.
 
 @param region The region to add for tracking.
 */
- (void)addRegion:(CERegion *)region;

/** Remove an existing tracking region. 
 
 @param region The region to add for tracking.
 */
- (void)removeRegion:(CERegion *)region;

/** Remove all tracking regions. */
- (void)removeAllRegions;

/** 
 * @name Miscellaneous
 *  
 */

/** Sets whether the map should hide the user's location. The accuracy ring will not appear if the user's location is hidden. */
- (void)setUserLocationMarkerHidden:(BOOL)hidden;

/** Sets whether the map should hide the user location's accuracy */
- (void)setUserLocationAccuracyHidden:(BOOL)hidden;

/** Returns the max zoom level that could show given bounds.
 
 @param bounds The bounds to test.
 @return The zoom level that could contain the given bounds. */
- (float)zoomContainingBounds:(CELonLatBounds)bounds;

/** Get the closest pixel to the given longitude/latitude position.
 
 @param lonLat The longitude/latitude location.
 @return The pixel location of this longitude/latitude position.
 */
- (CGPoint)project:(CELonLat)lonLat;

/** Get the closest longitude/latitude location to the given pixel.
 
 @param screenPos The pixel location.
 @return The longitude/latitude location closest to this pixel.
 */
- (CELonLat)unproject:(CGPoint)screenPos;

/** Get the closest pixel region to the given longitude/latitude region.
 
 @param lonLatBounds The longitude/latitude region.
 @return The pixel region of this longitude/latitude region.
 */
- (CGRect)projectBounds:(CELonLatBounds)lonLatBounds;

/** Get the closest longitude/latitude region to the given pixel.
 
 @param screenRect The pixel region.
 @return The longitude/latitude region closest to this pixel.
 */
- (CELonLatBounds)unprojectBounds:(CGRect)screenRect;

/** Set a map option.
 
 The MapOptions for this method are as follows:
 
 - kCEMapOptionKineticPan: Controls the kinetic movement of the map on a pan gesture. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
 - kCEMapOptionKineticZoom: Controls the kinetic movment of the map on a zoom gesture. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
 - kCEMapOptionKineticRotate: Controls the kinetic movment of the map on a rotate gesture. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
 - kCEMapOptionAnimation: Controls the animated movements of the map, invoked by the application. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
 - kCEMapOptionUpdate: Controls the amount of dynamic processing done by the map. Should be set to kCEMapOptionLow, kCEMapOptionMedium, or kCEMapOptionHigh.
 - kCEMapOptionRotation: Controls the ability to orient the map. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
 - kCEMapOptionPanGesture: Controls the user's ability to pan the map through gestures. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
  - kCEMapOptionZoomGesture: Controls the user's ability to zoom the map through gestures. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
  - kCEMapOptionRotateGesture: Controls the user's ability to rotate the map through gestures. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
  - kCEMapOptionTiltGesture: Controls the user's ability to tilt the map through gestures. Should be set to either kCEMapOptionOff or kCEMapOptionOn.
 - kCEMapOptionDynamicGestureTypes: Controls the map's ability to switch between tilt and zoom/rotate gestures. Should be set to either kCEMapOptionOff or kCEMapOptionOn.

 @param value The option value.
 @param key The option key.
 */
- (void)setMapOption:(CEMapOptionValue)value forKey:(CEMapOption)key;

/* Returns the resolution (meters per pixel) of the map for the given zoom level.
 @param zoom The zoom level to request the resolution for.
 */
- (double)resolutionAtZoom:(CGFloat)zoom;

/**
 * @name Properties
 *  
 */

/** Map's delegate */
@property (nonatomic, weak) id <CEMapViewDelegate> delegate;

/** Current map center */
@property (nonatomic, assign) CELonLat mapCenter;

/** Current map extents */
@property (nonatomic, assign) CELonLatBounds extents;

/** Current map zoom level */
@property (nonatomic, assign) float zoom;

/** Current map scale.  Units are meters per inch */
@property (nonatomic, assign, readonly) double scale;

/** Current map resolution.  Units are meters per pixel */
@property (nonatomic, assign, readonly) double resolution;

/** Current map orientation */
@property (nonatomic, assign) double orientation;

/** Current map tilt */
@property (nonatomic, assign) double tilt;

/** User's current GPS information */
@property (nonatomic, assign, readonly) CEUserLocation userLocation;

/** Options dictionary passed in at construction */
@property (nonatomic, strong, readonly) CEMapViewOptions *options;

/** Whether location services have been enabled by the user */
@property (nonatomic, assign, readonly) BOOL locationServicesEnabled;

/** Current geofencing regions */
@property (nonatomic, strong) NSArray *regions;

/** The map's padding.  Padding changes the viewable area of the map */
@property (nonatomic, assign) CGRect padding;

/** Whether the map is currently performing an animation. */
@property (nonatomic, readonly, assign) BOOL animating;

/** Set or read the map's tracking mode.
 
 The tracking mode may be one of the following options:
 
 - kCETrackingModeNone - Disables tracking.
 - kCETrackingModePosition - The map will track the user's current GPS position, and automatically move each time the user's position changes.  Any user interaction will break this tracking mode, and automatically set it back to kCETrackingModeNone.
 - kCETrackingModePositionOrientation - The map will track the user's current GPS position and compass orientation, and automatically move each time the user's position changes.  Any user interaction will break this tracking mode, and automatically set it back to kCETrackingModeNone.
 */
@property (nonatomic, assign) CETrackingMode trackingMode;

@end
