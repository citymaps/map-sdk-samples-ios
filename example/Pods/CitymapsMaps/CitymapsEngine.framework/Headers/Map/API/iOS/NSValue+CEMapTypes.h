//
//  NSValue+CEMapTypes.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/20/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>

@interface NSValue (CEMapTypes)

- (CELonLat)CELonLatValue;
- (CELonLatBounds)CELonLatBoundsValue;

+ (NSValue *)valueWithCELonLat:(CELonLat)lonlat;
+ (NSValue *)valueWithCELonLatBounds:(CELonLatBounds)bounds;

@end
