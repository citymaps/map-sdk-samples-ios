//
//  CEMarkerGroup.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/1/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CitymapsEngine/Map/API/iOS/CEMarker.h>

/** A marker group is a container which can be used to add markers to the map. 
 
 Marker groups allow you to organize your markers so you can perform batch operations on multiple markers at once.  
 Marker groups should NOT be explicitely constructed.  Instead, you should use [CEMapView markerGroupWithName:] to obtain a marker group correctly.
 
 */

@interface CEMarkerGroup : NSObject

/** 
 * @name Initializers
 *  
 */

/** Initialize the marker group with the given name.
 
 This initializer should NOT be called directly, or this marker group will not be added to the map properly.
 Instead, you should use [CEMapView markerGroupWithName:] to obtain a marker group correctly.
 
 @param name The name of the marker group.
*/
- (id)initWithName:(NSString *)name;

/** 
 * @name Marker Management
 *  
 */

/** Add a marker to the given marker group.
 
 @param marker The marker to add to the group
 */
- (void)addMarker:(CEMarker*)marker;

/** Remove a marker to the given marker group.
 
 @param marker The marker to remove from the group
 */
- (void)removeMarker:(CEMarker*)marker;

/** Remove all markers from the group */
- (void)removeAllMarkers;

/** 
 * @name Properties
 *  
 */

/** The name of the marker group */
@property (strong, nonatomic, readonly) NSString *name;
    
/** The markers in this group. */
@property (strong, nonatomic, readonly) NSMutableArray *markers;
    
/** Whether marker visibility should be automated based on collisions. */
@property (assign, nonatomic) BOOL shouldTestForCollisions;

@end
