//
//  CECircle.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>
#import <CitymapsEngine/Map/API/iOS/Layer/CanvasFeatures/CEFeature.h>

/** This class represents a user defined circle to be placed on a CECanvasLayer. */
@interface CECircle : CEFeature

/**
 @name Initializers
 */

/** Initializes a new circle with the given position and radius.
 @param position The center of the circle.
 @param radius The radius of the circle.
 @return A new circle with the given position and radius.
 */
-(id)initWithPosition:(CELonLat)position andRadius:(float)radius;

/**
 @name Properties
 */

/** The center of the circle. */
@property (nonatomic, assign) CELonLat position;

/** The radius of the circle. */
@property (nonatomic, assign) CGFloat radius;

@end
