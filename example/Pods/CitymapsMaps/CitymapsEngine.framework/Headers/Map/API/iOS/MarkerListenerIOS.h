//
//  MarkerListenerIOS.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/2/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Marker/Marker.h>
#include <CitymapsEngine/Map/ApI/iOS/CEMarker.h>

namespace citymaps
{
    class MarkerListenerIOS : public IMarkerListener
    {
    public:
        void SetDelegate(CEMarker* marker, id<CEMarkerDelegate> delegate)
        {
            mMarker = marker;
            mDelegate = delegate;
        }
        
        bool OnMarkerEvent(MarkerEvent event, Marker* marker);
        
    private:
        
        id <CEMarkerDelegate> mDelegate;
        CEMarker* mMarker;
        
    };
}
