//
//  CELine.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>
#import <CitymapsEngine/Map/API/iOS/Layer/CanvasFeatures/CEFeature.h>

/** This class represents a user defined line to be placed on a CECanvasLayer. */
@interface CELine : CEFeature

/**
 * @name Initializers
 */

/** Initializes a new line. */
- (id)init;

/* Initializes a new line with the given points */
- (id)initWithPoints:(CELonLat*)points count:(int)count;

/**
 * @name Points
 */

/** This method adds a point to the line.
 * @param point The point to add to the line.
 */
- (void)addPoint:(CELonLat)point;

/** This method adds a set of points to the line.
 * @param points Points to add to the line.
 * @param count number of points to add.
 */
- (void)addPoints:(CELonLat*)points count:(int)count;

/** This method returns the point at the given index.
 * @param index The index of the point you want to retrieve.
 * @return The point requested.
 */
- (CELonLat)pointAtIndex:(int)index;

/** This method returns the number of points in the line.
 * @return The number of points in the line.
 */
- (NSUInteger)numPoints;

/**
 * @name Properties
 */

/** The fill width of the line (in pixels). */
@property (nonatomic, assign) float width;

@end
