//
//  CEMapView_Protected.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <CitymapsEngine/Map/API/iOS/CEMapView.h>
#import <CitymapsEngine/Map/Map.h>

@interface CEMapView ()

@property (assign, nonatomic) std::shared_ptr<citymaps::Map> enginePointer;

- (void)initializeMap:(CGRect)frame withOptions:(CEMapViewOptions *)options;
- (void)applyOptions:(CEMapViewOptions *)options toDesc:(citymaps::MAP_DESC *)desc;

@end
