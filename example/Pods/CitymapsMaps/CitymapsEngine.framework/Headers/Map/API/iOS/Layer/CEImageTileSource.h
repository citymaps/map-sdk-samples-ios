#import <Foundation/Foundation.h>

#import <CitymapsEngine/Map/API/iOS/Layer/CETileSource.h>

/** A tile source for an image-based tile source
 
 This type of data source is required for CEImageTileLayer.  
 
 See CETileSource for more information about tile sources in general.
 
 */

@interface CEImageTileSource : CETileSource

- (id)initWithFilepath:(NSString *)filepath;
- (id)initWithURL:(NSString *)url;

@end
