//
//  CEFeature.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CEFeature;

/**
 A protocol for listening to touch events on a map feature
 */

@protocol CEFeatureDelegate <NSObject>

/** 
 Notifies the delegate when this feature is tapped 
 
 @param feature The tapped feature
 @return Whether the event has been consumed.
 */
- (BOOL)featureTapped:(CEFeature *)feature;

/** 
 Notifies the delegate when this feature is double tapped 
 
 @param feature The doubled tapped feature
 @return Whether the event has been consumed.
 */
- (BOOL)featureDoubleTapped:(CEFeature *)feature;

/** 
 Notifies the delegate when this feature is long pressed 
 
 @param feature The long pressed feature
 @return Whether the event has been consumed.
 */
- (BOOL)featureLongPressed:(CEFeature *)feature;

@end

/** 
 This class is the base class for all user defined shapes supported by CECanvasLayer.
 */
@interface CEFeature : NSObject

/**
 @name Initializers
 */

/** 
 Initializes a new feature
 */
- (id)init;

- (void)dealloc;

/**
 @name Images
 */

/**
 Sets the image to be displayed from a file.
 @param file The image file to load.
 */
- (void)loadImageFromFile:(NSString *)file;

/**
 Sets the image to be displayed from a resource.
 @param resource The image resource to load.
 */
- (void)loadImageFromResource:(NSString *)resource;

/**
 @name Properties
 */

/** The color to fill the feature with. Only kCGColorSpaceModelMonochrome and kCGColorSpaceModelRGB are supported. */
@property (nonatomic, strong) UIColor *fillColor;

/** The color to outline the feature with. Only kCGColorSpaceModelMonochrome and kCGColorSpaceModelRGB are supported.å */
@property (nonatomic, strong) UIColor *strokeColor;

/** The width of the outline around this feature (in pixels). */
@property (nonatomic, assign) float strokeWidth;

/** Whether this feature will be rendered. */
@property (nonatomic, assign) BOOL visible;

/** The image being displayed on this feature. */
@property (nonatomic, strong) UIImage *image;

/** The feature's delegate */
@property (nonatomic, strong) id<CEFeatureDelegate> delegate;

/** A tag that can be used to identify a specific object */
@property (nonatomic, assign) NSInteger tag;

@end
