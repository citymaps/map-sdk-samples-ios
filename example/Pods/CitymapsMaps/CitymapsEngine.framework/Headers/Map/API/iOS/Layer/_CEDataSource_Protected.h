//
//  _CEDataSource_Protected.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <CitymapsEngine/Map/API/iOS/Layer/CEDataSource.h>
#import <CitymapsEngine/Map/Layer/TileSource/DataSource.h>

@interface CEDataSource()

@property (assign, nonatomic) citymaps::IDataSource* enginePointer;
- (citymaps::IDataSource *)createDataSource:(CEDataSourceOptions *)options;
- (void)applyOptions:(CEDataSourceOptions *)options toDesc:(citymaps::DATA_SOURCE_DESC *)desc;

@end
