//
//  CEMarkerGroup_Protected.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <CitymapsEngine/Map/API/iOS/CEMarkerGroup.h>
#import <CitymapsEngine/Map/Marker/MarkerGroup.h>

@interface CEMarkerGroup ()

@property (assign, nonatomic) citymaps::MarkerGroup *enginePointer;
@property (weak, nonatomic) CEMapView *map;

- (void)postUpdate;

@end
