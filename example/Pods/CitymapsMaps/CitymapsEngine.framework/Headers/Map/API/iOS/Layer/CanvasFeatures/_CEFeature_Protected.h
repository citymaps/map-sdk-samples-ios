//
//  _CELayer_Protected.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <CitymapsEngine/Map/API/iOS/Layer/CELayer.h>
#import <CitymapsEngine/Map/Layer/Canvas/CanvasShape.h>
#import <CitymapsEngine/Map/API/iOS/Layer/CanvasFeatures/CEFeature.h>

@interface CEFeature ()

- (citymaps::CanvasShape*)createShape;

@property (assign, nonatomic) citymaps::CanvasShape *enginePointer;

@end