#import <CitymapsEngine/Map/API/iOS/CELabel.h>

#import <CitymapsEngine/Map/Marker/LabelMarker.h>
#import <CitymapsEngine/Map/API/iOS/MarkerListenerIOS.h>

@interface CELabel()

@property (assign, nonatomic) citymaps::LabelMarker *enginePointer;

@end
