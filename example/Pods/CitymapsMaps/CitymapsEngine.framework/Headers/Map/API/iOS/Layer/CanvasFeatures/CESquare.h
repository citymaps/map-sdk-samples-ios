#import <Foundation/Foundation.h>

#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>
#import <CitymapsEngine/Map/API/iOS/Layer/CanvasFeatures/CEFeature.h>

/**
 * This class represents a user defined square to be placed on a CECanvasLayer.
 */
@interface CESquare : CEFeature

/**
 * @name Initializers
 */

/** Initializes a new square.
 * @param position The center of the square.
 * @param size the half-width of the square.
 * @return A new square at the given position for the given size.
 */
- (id)initWithPosition:(CELonLat)position andSize:(float)size;

/**
 * @name Properties
 *  
 */

/** The center position of the square. */
@property (nonatomic, assign) CELonLat position;

/** The half-width of the square. */
@property (nonatomic, assign) CGFloat size;

@end
