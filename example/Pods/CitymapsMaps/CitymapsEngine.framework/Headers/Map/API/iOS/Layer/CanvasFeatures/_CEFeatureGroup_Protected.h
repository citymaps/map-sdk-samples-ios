//
//  _CEFeatureGroup_Protected.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/19/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <CitymapsEngine/Map/API/iOS/Layer/CanvasFeatures/CEFeatureGroup.h>
#import <CitymapsEngine/Map/Layer/Canvas/CanvasShapeGroup.h>

@interface CEFeatureGroup ()

@property (assign, nonatomic) citymaps::CanvasShapeGroup *enginePointer;

@end