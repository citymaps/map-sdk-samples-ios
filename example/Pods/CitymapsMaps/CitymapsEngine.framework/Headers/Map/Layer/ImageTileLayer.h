//
//  ImageTileLayer.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/22/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/TileLayer.h>

namespace citymaps
{
    class ImageTileLayer : public TileLayer
    {
    public:
        ImageTileLayer(TILE_LAYER_DESC layerDesc);
        ~ImageTileLayer();

        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        Tile* AllocateTile(GridPoint cell);
        void ReleaseTile(Tile *tile);
    
    };
};
