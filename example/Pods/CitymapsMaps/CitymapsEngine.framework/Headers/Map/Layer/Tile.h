//
//  Tile.h
//  vectormap2
//
//  Created by Lion User on 08/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Map/Layer/TileLayer.h>

namespace citymaps
{
    class IGraphicsDevice;
    
    class Tile
    {
    public:
        Tile(GridPoint gridPoint, TileLayer *layer);
        virtual ~Tile(); 
        
        void LoadTileData(const TByteArray &data);
        bool IsReady() { return mReady; }
        bool IsOnZoom(int zoom) { return mGridPoint.zoom == zoom; }
        bool WithinTileBounds(Bounds &bounds)
        {
            return bounds.ContainsPoint(mGridPoint.x, mGridPoint.y);
        }

        const GridPoint& GetGridPoint() const { return mGridPoint; }
        const int GetZoom() { return mGridPoint.zoom; }
        
        virtual void SetGridPoint(const GridPoint& point) { mGridPoint = point;}
        
        bool ShouldRemoveFromLayer(int zoom, Bounds &bounds, TTileMap &tiles);
        
        virtual void Update(const MapState& state);
        virtual void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        virtual void RemovedFromLayer(TileLayer *layer);
        
        virtual Bounds CalculateBounds();
        Bounds CalculateBounds(const GridPoint& tile, double tileLength);
        
        virtual Point CalculateTileOrigin();
        double GetLength()
        {
            return mTileLayer->TileLength(mGridPoint.zoom);
        }
        
        TileLayer* GetLayer() { return mTileLayer; }
        
    protected:
        void SetReady(bool ready) { mReady = true; }
        
        std::mutex& GetLock()
        {
            return mLoadMutex;
        }
        
        void Lock(){mLoadMutex.lock();}
        void Unlock(){mLoadMutex.unlock();}

    private:

        TileLayer *mTileLayer;
        float mAlpha;
        bool mReady;
        GridPoint mGridPoint;
        std::mutex mLoadMutex;
        
        virtual void SetData(const TByteArray &data) = 0;
        
        
    };
};
