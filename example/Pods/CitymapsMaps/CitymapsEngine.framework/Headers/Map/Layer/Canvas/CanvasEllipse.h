//
//  CanvasEllipse.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/CanvasLayer.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasShape.h>

namespace citymaps
{
    class MeshShape;
    class ITexture2D;
    class CanvasEllipse : public CanvasShape
    {
        public:
        
        CanvasEllipse();
        
        ~CanvasEllipse();

        void SetPosition(const Point &p)
        {
            mPoint = p;
            CanvasLayer* layer = this->GetLayer();
            if (layer)
                layer->Invalidate();
        }
        
        void SetRadii(float x, float y)
        {
            mRadii = Vector2f(x,y);
            CanvasLayer* layer = this->GetLayer();
            if (layer)
                layer->Invalidate();
        }
        
        Point GetPosition()
        {
            return mPoint;
        }
        
        Size GetRadii()
        {
            return Size(mRadii.x, mRadii.y);
        }
        
        // Sets the internal position data to p.
        // Does not change the lon/lat position of this ellipse.
        void SetMercPosition(const Point& p)
        {
            mPosition.x = p.x;
            mPosition.y = p.y;
        }
        
        virtual void Update(const MapState& state);
        virtual void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        
        enum GlobalDataVariables
        {
            FillColor,
            StrokeColor,
            OutlineCutoff,
            Radii,
            Texture,
            NumGlobalData
        };
        
    private:
        
        Vector2f mRadii;
        Vector2f mRadiiData;
        Vector3 mPosition;
        double mWorldWidth;
        float mOutlineCutoff;
        Point mPoint;
        MeshShape* mMeshShape;
        std::shared_ptr<Image> mImage;
        ITexture2D* mTexture;
        double mSecondPassTranslation;
        
        int mGlobalDataIndices[NumGlobalData];
    };
}
