//
//  CanvasSquare.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/Canvas/CanvasRectangle.h>

namespace citymaps
{
    class CanvasSquare : public CanvasRectangle
    {
        
    public:
        CanvasSquare():CanvasRectangle()
        {
        }
        
        void SetSize(float r)
        {
            CanvasRectangle::SetSize(r,r);
        }
        
        float GetSize()
        {
            return CanvasRectangle::GetSize().width;
        }
    };
}
