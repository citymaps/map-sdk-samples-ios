//
//  CanvasShape.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Application.h>
#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/Image.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Map/Core/MapState.h>

namespace citymaps
{
    enum CanvasShapeType
    {
        CanvasShapeLine,
        CanvasShapePolygon,
        CanvasShapeEllipse,
        CanvasShapeCircle,
        CanvasShapeRectangle,
        CanvasShapeSquare,
        CanvasShapeImage
    };
    
    enum CanvasShapeEvent
    {
        CanvasShapeEventTap,
        CanvasShapeEventDoubleTap,
        CanvasShapeEventLongPress,
    };

    class CanvasShape;
    
    class ICanvasShapeListener
    {
    public:
        
        // Should return true if the event was consumed.
        virtual bool OnCanvasShapeEvent(CanvasShapeEvent event, CanvasShape* shape) = 0;
    };
    
    
    class CanvasLayer;
    class CanvasShape
    {
    public:
        CanvasShape(CanvasShapeType type) :
            mCanvasLayer(NULL), mType(type), mFillColor(0, 0, 0, 0), mStrokeColor(0, 0, 0, 0), mStrokeWidth(0), mVisible(true), mFillImage(nullptr){}

        virtual ~CanvasShape() {}
        
        const Vector4f& GetFillColor() { return mFillColor; }
        const Vector4f& GetStrokeColor() { return mStrokeColor; }
        const real GetStrokeWidth() { return mStrokeWidth; }
        std::shared_ptr<Image> GetFillImage() { return mFillImage; }
        CanvasShapeType GetType() { return mType; }
        
        void SetListener(std::shared_ptr<ICanvasShapeListener> listener)
        {
            mListener = listener;
        }
        
        std::shared_ptr<ICanvasShapeListener> GetListener() { return mListener; }
        
        void SetFillColor(const Vector4f &color);        
        void SetStrokeColor(const Vector4f &color);
        void SetStrokeWidth(const real width);
        void SetFillImage(std::shared_ptr<Image> image);

        void SetZIndex(float zIndex);
        float GetZIndex() const { return mZIndex;}
        bool IsVisible() const {return mVisible;}
        void SetVisible(bool visible);
        
        void SetLayer(CanvasLayer* layer) { mCanvasLayer = layer;}
        virtual void Update(const MapState& state) = 0;
        virtual void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState) = 0;
        
        virtual bool ProcessTouchEvent(MapEventType type, const Point &p) { return false; }
        
    protected:
        CanvasLayer* GetLayer() { return mCanvasLayer;}
        
    private:
        Vector4f mFillColor;
        Vector4f mStrokeColor;
        real mStrokeWidth;
        bool mVisible;
        float mZIndex;
        std::shared_ptr<Image> mFillImage;
        CanvasShapeType mType;
        CanvasLayer* mCanvasLayer;
        
        std::shared_ptr<ICanvasShapeListener> mListener;
    };
}
