//
//  CanvasLine.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/GeometryUtil.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasShape.h>
#include <CitymapsEngine/Map/Layer/CanvasLayer.h>
#include <CitymapsEngine/Core/Graphics/Shape/MeshShape.h>
#include <CitymapsEngine/Core/Graphics/Texture/Texture2D.h>

namespace citymaps
{
    class CanvasCircle;
    class Projection;
    class CanvasLine : public CanvasShape
    {
    public:
        
        CanvasLine();
        
        ~CanvasLine();
        
        void AddPoint(const Point &point)
        {
            mPoints.push_back(point);
            CanvasLayer* layer = this->GetLayer();
            if (layer)
                layer->Invalidate();
            
            mPointsChanged = true;
        }
        
        void AddPoints(const std::vector<Point>& points)
        {
             mPoints.reserve(mPoints.size() + points.size());
             mPoints.insert(mPoints.end(), points.begin(), points.end());
             CanvasLayer* layer = this->GetLayer();
             if (layer)
                 layer->Invalidate();
             
             mPointsChanged = true;
        }

        void SetPoints(const std::vector<Point>& points)
        {
             mPoints = points;
             CanvasLayer* layer = this->GetLayer();
             if (layer)
                 layer->Invalidate();
             
             mPointsChanged = true;
        }
        
        void SetFillWidth(float width)
        {
             mFillWidth = std::max(0.0f, width);
             CanvasLayer* layer = this->GetLayer();
             if (layer)
                 layer->Invalidate();
        }
        
        float GetFillWidth()
        {
            return mFillWidth;
        }
        
        size_t GetNumPoints() { return mPoints.size(); }
        
        void SetSimplifyEnabled(bool enabled)
        {
            mSimplifyEnabled = enabled;
            mLastZoom = -1;
        }
        
        void SetWrapDateLineEnabled(bool enabled)
        {
            mWrapDateLine = enabled;
            mLastZoom = -1;
        }
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        bool ProcessTouchEvent(MapEventType type, const Point &p);
        
    private:
        
        enum GlobalDataVariables
        {
            Color,
            Width,
            Texture,
            NumGlobalData
        };
        
    private:
        
        struct CanvasLineVertex
        {
            float position[2];
            float perp[2];
            float uv[2];
        };
        
        Bounds mMapBounds;
        float mFillWidth;
        std::vector<Point> mPoints;
        std::vector<Point> mSimplifiedPoints;
        Vector2 mLastOrigin;
        Point mMapCenter;
        double mWorldWidth;
        
        MeshShape *mShape;
        GeometryUtilVertexVector mDoublePrecisionData;
        std::vector<CanvasLineVertex> mFloatingPrecisionData;
        
        std::shared_ptr<Image> mTextureImage;
        ITexture2D* mTexture;
        int mLastZoom;
        bool mFirstFrame;
        bool mDataChanged;
        bool mImageChanged;
        bool mPointsChanged;
        bool mSimplifyEnabled;
        bool mWrapDateLine;
        float mFillWidthData;
        float mStrokeWidthData;
        float mTotalWidthData;
        float mMercWidth;
        
        int mGlobalDataIndices[NumGlobalData];
        
        void Simplify(const MapState& state, double tolerance);
    };
};