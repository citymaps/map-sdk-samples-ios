
#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasShape.h>

namespace citymaps
{
    class Sprite;
    class Image;
    class CanvasImage : public CanvasShape
    {
    public:
        CanvasImage();
        ~CanvasImage();
        
        void SetAnchorPoint(float x, float y);
        void SetAnchorPoint(const Point& p);
        void SetRotation(float rotation);
        void SetAlpha(float alpha);

        void SetPosition(const Point& lonLat);
        void SetPosition(const Point& lonLat, float width, float height);
        void SetPosition(const Point& lonLat, float width);
        void SetBounds(const Bounds& lonLatBounds);
        
        Point GetAnchorPoint() const { return mAnchorPoint; }
        float GetRotation() const { return mRotation; }
        float GetAlpha() const { return mAlpha; }
        Point GetPosition() const { return mPosition; }
        const Bounds& GetBounds() const { return mBounds; }
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        
        enum PositionState
        {
            PositionStatePosition,
            PositionStatePositionRatio,
            PositionStatePositionSize,
            PositionStateBounds
        };
        
        PositionState mState;
        
        Point mAnchorPoint;
        float mRotation;
        float mAlpha;
        double mWorldWidth;
        std::shared_ptr<Image> mImage;
        bool mImageChanged;
        
        Sprite *mSprite;
        Point mPosition;
        Size mSize;
        Bounds mBounds;
        
        Point mMercPoint;
        Bounds mMercBounds;
        Size mMercSize;
    };
}