//
//  CanvasPolygon.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/Canvas/CanvasShape.h>
#include <CitymapsEngine/Core/Graphics/Shape/MeshShape.h>

namespace citymaps
{
    class MeshShape;
    class CanvasLine;
    class CanvasPolygon : public CanvasShape
    {
    public:
        
        CanvasPolygon();
        
        ~CanvasPolygon();
        
        void AddPoint(const Point& p);
        
        void AddPoints(const std::vector<Point>& p);
        void SetPoints(const std::vector<Point>& p);
        
        void AddHole(CanvasPolygon* hole);
        
        void RemoveHole(CanvasPolygon* hole);
        void RemoveAllHoles();
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        
        void PrepareRender(IGraphicsDevice* device);
        
        void RenderFill(IGraphicsDevice* device, RenderState &perspectiveState, RenderState &orthoState);
        
        void RenderStroke(IGraphicsDevice* device, RenderState &perspectiveState, RenderState &orthoState);
        
        enum GlobalDataVariables
        {
            Color,
            NumGlobalData
        };
        
    private:
        
        struct CanvasPolygonVertex
        {
            Vector2f pos;
        };
        
    private:
        
        bool mPointsChanged;
        bool mDataChanged;
        double mWorldWidth;
        std::vector<Point> mPoints;
        std::vector<CanvasPolygonVertex> mData;
        std::vector<CanvasPolygon*> mHoles;
        Vector2f mOrigin;
        MeshShape* mShape;
        CanvasLine* mStroke;
        int mGlobalDataIndices[NumGlobalData];
    };
}