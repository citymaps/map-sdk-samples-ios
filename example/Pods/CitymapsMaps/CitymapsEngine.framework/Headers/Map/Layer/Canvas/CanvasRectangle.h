//
//  CanvasRectangle.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/Canvas/CanvasShape.h>
#include <CitymapsEngine/Map/Layer/CanvasLayer.h>
#include <CitymapsEngine/Core/Graphics/Shape/MeshShape.h>
#include <CitymapsEngine/Core/Graphics/Texture/Texture2D.h>

namespace citymaps
{
    class MeshShape;
    class ITexture2D;
    class CanvasRectangle : public CanvasShape
    {
    public:
        
        CanvasRectangle();

        ~CanvasRectangle();

        void SetPosition(const Point &p)
        {
            mPoint = p;
            CanvasLayer* layer = this->GetLayer();
            if (layer)
                layer->Invalidate();
        }
        
        void SetSize(float x, float y)
        {
            mSize = Vector2f(x,y);
            CanvasLayer* layer = this->GetLayer();
            if (layer)
                layer->Invalidate();
        }
        
        Point GetPosition()
        {
            return mPoint;
        }
        
        Size GetSize()
        {
            return Size(mSize.x, mSize.y);
        }
        
        virtual void Update(const MapState& state);
        virtual void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        
        enum GlobalDataVariables
        {
            Position,
            Color,
            Widths,
            Texture,
            NumGlobalData
        };
        
    private:
        
        Vector2f mSize;
        Vector2f mPosition;
        Vector2f mStrokePassWidths;
        Vector2f mFillPassWidths;
        Point mPoint;
        MeshShape* mMeshShape;
        std::shared_ptr<Image> mImage;
        ITexture2D* mTexture;
        double mWorldWidth;
        
        int mGlobalDataIndices[NumGlobalData];
    };
}
