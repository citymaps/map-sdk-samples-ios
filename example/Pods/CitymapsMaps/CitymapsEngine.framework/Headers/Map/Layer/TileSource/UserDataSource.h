#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/TileSource/DataSource.h>
#include <CitymapsEngine/Core/Util/OperationQueue.h>

namespace citymaps
{
	class UserDataSource : public BaseDataSource
	{
	public:
		UserDataSource(const DATA_SOURCE_DESC& desc);
		virtual ~UserDataSource();

		bool RetrieveData(const GridPoint& gp);

		IDataSource* Clone() const;

        std::string GenerateCachePath(const GridPoint& gp) { return "";}

	protected:
		virtual std::shared_ptr<TByteArray> GetTileData(const GridPoint& gp) = 0;

	private:
		jobject mObj;
		jmethodID mGetTileData;
		OperationQueue mQueue;
	};
};