//
//  CloudfrontSignedTileTransformer.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 3/17/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/TileSource/DataSource.h>

#include <Poco/Crypto/RSAKey.h>

namespace citymaps
{
    struct CLOUDFRONT_SIGNED_URL_DESC
    {
        std::string AccessKeyID;
        std::string EncryptionAlgorithm = "";
        std::string EncryptionPassphrase = "";
        std::string DistributionURL = "";
        uint64_t Timeout;
    };
    
    class CloudfrontSignedPathTransformer : public IPathTransformer
    {
    public:
        CloudfrontSignedPathTransformer(CLOUDFRONT_SIGNED_URL_DESC &desc, const TByteArray &publicKeyData, const TByteArray &privateKeyData);
        ~CloudfrontSignedPathTransformer();
        
        std::string GeneratePath(const GridPoint& cell, const std::string uri);
        std::string GenerateCacheFilename(const std::string &url);
        
    private:
        CLOUDFRONT_SIGNED_URL_DESC mDesc;
        std::string mSignature;
        std::time_t mCloudfrontExpiration;
        std::mutex mRefreshLock;
        std::string mCloudfrontParams;
        RSA *mRSA;
        
        std::string DecryptFile(const std::string filename, const std::string &password);
        
        void RefreshCloudfrontSignature();
        
        std::string ToCloudfrontSafeString(std::string &str);

        
    };
}
