//
//  TileSource.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/DataSource.h>
#include <CitymapsEngine/Map/Layer/TIleSource/TileFactory.h>

namespace citymaps
{
    enum TileSourceStatus
    {
        TileSourceStatusDataNotFound,
        TileSourceStatusRefused,
        TileSourceStatusTileNotFound,
        TileSourceStatusTimedOut,
        TileSourceStatusCancelled,
        TileSourceStatusInternalServerError,
        TileSourceStatusUnknown
    };
    
    class ITileSourceListener
    {
    public:
        
        virtual void OnTileRetrieveSuccess(const GridPoint& gp, std::shared_ptr<Tile> tile) = 0;
        virtual void OnTileRetrieveFailure(const GridPoint& gp, TileSourceStatus status) = 0;
    };
    
    class TileSource : public IDataSourceListener
    {
    public:
        
        TileSource(IDataSource* dataSource, ITileFactory* tileFactory);
        TileSource();
        
        ~TileSource();
        
        bool IsReady() { return mDataSource->IsReady(); }
        
        void SetDataSource(IDataSource* dataSource);
        void SetTileFactory(ITileFactory* tileFactory);
        
        void SetListener(ITileSourceListener* listener) {mListener = listener; }
        void SetTileLayer(TileLayer* layer) { mTileFactory->SetLayer(layer); }
        
        void SetCacheEnabled(bool enabled);
        
        void BeginRetrieval(const GridPoint& gp);
        void BeginRetrieval(const std::vector<GridPoint>& gp);
        
        TDataSourceOperations GetActiveRetrievals() { return mDataSource->GetActiveOperations();}
        void CancelRetrieval(const GridPoint& gp);
        
        void CancelAll();
        
        void ClearCache(); 
        
        void OnDataRetrieveSuccess(const GridPoint& gridPoint, std::shared_ptr<TByteArray> data);
        void OnDataRetrieveFailure(const GridPoint& gridPoint, DataSourceStatus status);
        
        IDataSource* GetDataSource() { return mDataSource; }
        ITileFactory *GetTileFactory() { return mTileFactory; }
        
    private:
        
        typedef SafeObject<std::set<uint64_t>, std::recursive_mutex> TRetrievalSet;
        
        IDataSource* mDataSource;
        ITileFactory* mTileFactory;
        ITileSourceListener* mListener;
        TRetrievalSet mActiveRetrievals;
        bool mCacheEnabled;
    };
}