//
//  DiskDataSource.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/23/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/DataSource.h>

namespace citymaps
{
    struct DISK_DATA_SOURCE_DESC : public DATA_SOURCE_DESC
    {
        std::string URI;
    };
    
    class DiskDataSource : public BaseDataSource
    {
        
    public:
        
        DiskDataSource(const DISK_DATA_SOURCE_DESC& desc);
        
        virtual bool RetrieveData(const GridPoint& gp);
        
        /** We can't stop a disk based data source
         *  It will still not be sent to the listener, as it will be removed from the active operation set 
        **/
        virtual void Cancel(const GridPoint& gp)
        {
            BaseDataSource::Cancel(gp);
        }
        
        const std::string& GetFilepathURI() const { return mFilepathURI;}
        void SetFilepath(const std::string& filepath) {mFilepathURI = filepath;}
        
        std::string GenerateCachePath(const GridPoint& cell)
        {
            return this->GeneratePath(cell, this->GetFilepathURI());
        }
        
    private:
        
        std::string mFilepathURI;
    };
}
