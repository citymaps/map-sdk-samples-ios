//
//  TileFactory.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Util/MemoryCache.h>

namespace citymaps
{
    class Tile;
    class TileLayer;
    class ITileFactory
    {
        public:
        
        virtual ~ITileFactory(){}
        
        virtual void SetLayer(TileLayer* layer) = 0;
        virtual std::shared_ptr<Tile> RetrieveTileFromCache(const GridPoint& gp) = 0;
        virtual std::shared_ptr<Tile> RetrieveTile(const GridPoint& gp, std::shared_ptr<TByteArray> data) = 0;
        virtual void ClearCache() = 0;
        virtual void SetCacheEnabled(bool enabled) = 0;
    };
    
    
    class BaseTileFactory : public ITileFactory
    {
    public:
        
        BaseTileFactory();
        
        ~BaseTileFactory();
        
        void SetLayer(TileLayer* layer);
        
        std::shared_ptr<Tile> RetrieveTileFromCache(const GridPoint& gp);
        
        /** Will only return a fully complete tile **/
        std::shared_ptr<Tile> RetrieveTile(const GridPoint& gp, std::shared_ptr<TByteArray> data);
        virtual void ClearCache() { mMemoryCache.Clear(); }
        void SetCacheEnabled(bool enabled){ mCacheEnabled = enabled;}

    protected:
        
        BaseTileFactory(const BaseTileFactory& other)
        : mMemoryCache(other.mMemoryCache.GetMaxSize()), mLayer(NULL)
        {
        }
        
        virtual std::shared_ptr<Tile> AllocateTile(const GridPoint& gp, TileLayer* layer) = 0;
        
    private:
        
        MemoryCache<std::shared_ptr<Tile>> mMemoryCache;
        TileLayer* mLayer;
        bool mCacheEnabled;
        
    };
}