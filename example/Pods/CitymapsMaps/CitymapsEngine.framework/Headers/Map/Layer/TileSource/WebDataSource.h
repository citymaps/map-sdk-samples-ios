//
//  WebDataSource.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/DataSource.h>
#include <CitymapsEngine/Core/Network/HTTPConnection.h>

namespace citymaps
{
    struct WEB_DATA_SOURCE_DESC : public DATA_SOURCE_DESC
    {
        WEB_DATA_SOURCE_DESC()
        : MaxConcurrentOperations(2), Timeout(5)
        {}
        std::string TileURL;
        int MaxConcurrentOperations;
        double Timeout;
    };
    
    class WebDataSource : public BaseDataSource,
                          public ICacheFilenameTransformer
    {
        
    public:
        
        typedef SafeObject<std::map<uint64_t, std::shared_ptr<HTTPConnection> >, std::recursive_mutex> TDownloadOperationMap;
        
        WebDataSource(const WEB_DATA_SOURCE_DESC& desc);
        
        WebDataSource(const WebDataSource& src);
        
        ~WebDataSource();
        virtual bool RetrieveData(const GridPoint& gp);
        virtual void Cancel(const GridPoint& gp);
        
        void SetTileURL(const std::string& url) { mTileURL = url; }
        
        const std::string& GetTileURL() const { return mTileURL; }
        
        std::string GenerateCachePath(const GridPoint& cell);
        std::string GenerateCacheFilename(const std::string& url);
        
    private:
        
        TDownloadOperationMap mDownloadOperations;
        std::string mTileURL;

        void CancelOperation(const GridPoint& gp);
        void DownloadComplete(std::shared_ptr<TByteArray> data, GridPoint gp);
        void DownloadFailed(NetworkResponseStatus status, GridPoint gp, const std::string &uri);
    };
}