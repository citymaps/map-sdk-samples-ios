//
//  CanvasLayer.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/Layer.h>

namespace citymaps
{
    class CanvasShape;
    class CanvasShapeGroup;
    
    class CanvasLayer : public Layer
    {
    public:
        CanvasLayer(LAYER_DESC &desc);
        ~CanvasLayer();
        
        void AddShape(CanvasShape *shape);
        void RemoveShape(CanvasShape *shape);
        void RemoveAllShapes();
        
        CanvasShapeGroup* GetDefaultCanvasShapeGroup();
        CanvasShapeGroup* GetCanvasShapeGroup(const std::string& group);
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        bool ProcessTouchEvent(MapEventType type, const Point &p);
        
    private:
        std::map<std::string, CanvasShapeGroup*> mCanvasShapeGroups;
        std::vector<CanvasShape*> mVisibleFeatures;
        std::vector<CanvasShape*> mFeatures;
        double mSecondPassTranslation;
        
        void GenerateVisibleFeaturesList();
    };
};
