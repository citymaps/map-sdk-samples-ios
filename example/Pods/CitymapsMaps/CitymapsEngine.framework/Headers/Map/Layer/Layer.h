#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Map/Core/MapState.h>
#include <CitymapsEngine/Map/Core/mapTypes.h>

namespace citymaps
{
    struct LAYER_DESC
    {
        virtual ~LAYER_DESC() {}
        
        unsigned int MinZoom = 0;
        unsigned int MaxZoom = kMaxZoomLevel;
        bool Visible = true;
    };
    
    class Map;
	class Layer : public Validatable, public std::enable_shared_from_this<Layer>
	{
	public:
		Layer(LAYER_DESC &desc);
		virtual ~Layer();

		virtual void SetMap(Map *map);
        virtual void OnDisable(){}
        virtual void OnResize(int width, int height){}
        virtual void OnNetworkStatusChanged(NetworkStatus status){};
        
		virtual void Update(const MapState& state) = 0;
        virtual void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState) = 0;
        virtual bool RequiresUpdate(const MapState& state);
        virtual void Invalidate();
        
        Map* GetMap() const { return mMap; }
        
        void SetZIndex(float index);
        
        virtual void SetVisible(bool visible);
        virtual bool IsVisible() { return mVisible; }
        
        virtual bool ProcessTouchEvent(MapEventType type, const Point &p) { return false; }
        
        int GetMinZoom() const { return mMinZoom;}
        int GetMaxZoom() const { return mMaxZoom;}
        float GetZIndex() const { return mZIndex;}
        
        
        virtual void LimitMemory(){}
        
	private:
		Map *mMap;
        int mMinZoom;
        int mMaxZoom;
        bool mVisible;
        float mZIndex;
	};
};
