#pragma once

#include <CitymapsEngine/Map/Core/MapState.h>
#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Core/Physics/Shapes/AABBShape.h>
#include <CitymapsEngine/Core/Animation/ValueAnimation.h>

namespace citymaps
{
    struct MARKER_DESC
    {
        Point Location = Point(0,0);
        Point AnchorPoint = Point(0,0);
        real ZIndex = 0;
        bool Visible = true;
        bool Highlighted = false;
        real Alpha = 1.0;
        real Rotation = 0;
        Vector4f HighlightColor = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
        Size ScreenOffset = Size(0,0);
        bool Draggable = false;
        real FadeTime = 0.3; // In seconds
        real Scale = 1.0f;
    };
    
    enum MarkerEvent
    {
        MarkerEventTouchUp,
        MarkerEventTouchDown,
        MarkerEventTap,
        MarkerEventDoubleTap,
        MarkerEventLongPress,
        MarkerEventDragStart,
        MarkerEventDragMove,
        MarkerEventDragEnd
    };
    
    class MeshShape;
    class CollisionGroup;
    class Marker;
    class MarkerManager;
    class Map;
    class IMarkerListener
    {
    	public:

        // Should return true if the event was consumed.
        virtual bool OnMarkerEvent(MarkerEvent event, Marker* marker) = 0;
    };
    
    class Marker
    {
    public:
        Marker(const MARKER_DESC& desc = MARKER_DESC());
        virtual ~Marker();
        
        /* Map */
        void SetMap(Map* map);
        Map* GetMap() const { return mParent ? mParent->GetMap() : mMap; }
        
        /* Setters */
        void SetAnchorPoint(const Point &anchorPoint);
        void SetAttachmentAnchorPoint(const Point& anchorPoint);
        
        void SetAnchorPoint(real x, real y){ this->SetAnchorPoint(Point(x,y));}
        virtual void SetPosition(const Point &location);
        virtual void MoveToScreenPosition(const Point &pixel);
        virtual void SetVisible(bool visible);
        virtual void SetAlpha(real alpha);
        void SetDraggable(bool draggable);
        void SetDragging(bool dragging);
        void SetZIndex(int zIndex);
        void SetHighlightsOnTouch(bool highlightsOnTouch) { mHighlightsOnTouch = highlightsOnTouch; }
        void SetHighlighted(bool highlighted);
        void SetHighlightColor(const Vector4f& color);
        void SetScreenOffset(const Size& offset);
        void SetFadeTime(real time);
        void SetCollisionsActive(bool active);
        void SetRotation(real rotation);
        void SetInteractable(bool interactable) {mInteractable = interactable;}
        void SetCollisionPriority(real priority) { mCollisionPriority = priority;}
        void SetScale(real scale);
        void SetInheritance(bool scale, bool alpha, bool rotation)
        {
            mInheritScale = scale;
            mInheritAlpha = alpha;
            mInheritRotation = rotation;
        }
        virtual void SetMarkerListener(std::shared_ptr<IMarkerListener> listener)
        {
        	mMarkerListener = listener;
        }
        
        /* Getters */
        Size GetSize() const { return mCachedSize;}
        virtual Point GetPosition() const;
        const Point& GetAnchorPoint() const { return mAnchorPoint; }
        const Point& GetAttachmentAnchorPoint() const { return mAttachmentAnchorPoint;}
        real GetZIndex() const { return mZIndex; }
        const Vector4f& GetHighlightColor() const { return mHighlightColor;}
        const Size& GetScreenOffset() const { return mScreenOffset;}
        const Bounds& GetScreenBounds() const { return mBounds; }
        const Bounds& GetCompositeScreenBounds() const { return mCompositeBounds;}
        real GetCollisionPriority() const { return mCollisionPriority;}
        real GetScale() const { return mScale;}
        real GetFadeTime() const { return mFadeTime;}
        real GetCompositeScale() { return !mInheritScale ? mScale : mParent ? mParent->GetCompositeScale() * mScale : mScale;}
        
        real GetAlpha() const
        {
            return mAlpha;
        }
        
        real GetCompositeAlpha() const
        {
            return !mInheritAlpha ? mAlpha : mParent ? mAlpha * mParent->GetCompositeAlpha() : mAlpha;
        }
        
        real GetRotation() const
        {
            return mRotation;
        }
        
        real GetCompositeRotation() const
        {
            return !mInheritRotation ? mRotation : mParent ? mRotation + mParent->GetCompositeRotation() : mRotation;
        }
        
/* Attachents */
        void AddAttachment(Marker* attachment);
        void RemoveAttachment(Marker* attachment);
        size_t GetAttachmentCount() const { return mAttachments.size(); }
        Marker* GetAttachment(unsigned int index);
        void RemoveAllAttachments(bool invalidateMap = true);
        void RemoveFromParent();
            
        void InvalidateMap();
        
        /* Input events */
        virtual bool ContainsPoint(const Point& p);
	// These two methods should be coupled. Use GetMarkerForPosition to determine the furthest child to respond to the event, and call ProcessMarkerEvent on that marker.
        virtual Marker* GetMarkerForPosition(const Point& p);
        virtual bool ProcessMarkerEvent(MarkerEvent event, const Point& p = Point(0,0));
        
        /* Run loop methods */
        virtual void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        
        /* State checks */
        bool AreCollisionsActive() const { return mCollisionShape.IsActive();}
        bool IsVisible() { return mParent ? mVisible && mParent->IsVisible() : mVisible;}
        bool IsDraggable() { return mDraggable && !mParent; }
        bool IsDragging() { return mDragging; }
        bool IsHighlighted() {return mHighlighted;}
        bool IsOnScreen()
        {
            return mOnScreen;
        }
        bool IsInteractable() { return mInteractable;}
        bool HighlightsOnTouch() { return mHighlightsOnTouch; }
        
        virtual bool IsRenderable();
        
        /* Collision */
        bool Intersects(const Marker &other) { return mBounds.IntersectsBounds(other.mBounds); }
        bool Intersects(const Bounds &bounds) { return mBounds.IntersectsBounds(bounds); }
        virtual void AddToCollisionGroup(CollisionGroup* group);
        virtual void RemoveFromCollisionGroup(CollisionGroup* group);
        virtual bool CollidesWith(CollisionGroup* group);
        
        void SetMarkerManager(MarkerManager *manager) { mManager = manager; }
        MarkerManager* GetMarkerManager() { return mManager; }
        
        static Point CalculateAnchoredMarkerPosition(const Point& p, const Size& size, const Point& anchor);
        
        const Point& GetScreenPosition() { return mScreenPosition; }

        Point CalculateAttachmentMarkerPosition(const Point &markerAnchorPoint);
        virtual Size CalculateSize() = 0;
        
        // Animations
        
        void StartAnimation(std::unique_ptr<Animation> animation, const std::string& key)
        {
            if (animation->IsStarted())
            {
                throw Exception(BadParameterException, "Marker Animation", "The animation has already been started. It could already be owned by another marker.");
            }
            std::lock_guard<std::recursive_mutex> lock(mAnimationMutex);
            mAnimations[key]= std::move(animation);
            
            this->InvalidateMap();
        }
        
        void CancelAnimation(const std::string& key)
        {
            std::lock_guard<std::recursive_mutex> lock(mAnimationMutex);
            auto iter = mAnimations.find(key);
            if (iter != mAnimations.end())
            {
                iter->second->Cancel();
            }
        }
        
        void EndAnimation(const std::string& key)
        {
            std::lock_guard<std::recursive_mutex> lock(mAnimationMutex);
            auto iter = mAnimations.find(key);
            if (iter != mAnimations.end())
            {
                iter->second->End();
            }
        }
        
        void CancelAllAnimations()
        {
            std::lock_guard<std::recursive_mutex> lock(mAnimationMutex);
            for(const auto& pair : mAnimations)
            {
                pair.second->Cancel();
            }
        }
        
        void EndAllAnimations()
        {
            std::lock_guard<std::recursive_mutex> lock(mAnimationMutex);
            for(const auto& pair : mAnimations)
            {
                pair.second->End();
            }
        }
        
    protected:
        
        virtual void OnRender(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState) = 0;
        
        void SetAlphaInternal(real alpha)
        {
            mAlpha = Util::Clamp(alpha, static_cast<real>(0), static_cast<real>(1));
        }
    
        void SetAnchorInternal(real u, real v)
        {
            mAnchorPoint.x = u;
            mAnchorPoint.y = v;
        }
        
        void SetMercatorPosition(const Point &point)
        {
            mPoint = point;
            mPointIsLonLat = false;
        }
    
        // Used by parent markers to set their children's position.
        void SetScreenPosition(const Point& p)
        {
            mScreenPosition = p;
        }
    
        const Point& GetMercatorPosition()
        {
            return mPoint;
        }
        
        virtual void SetScreenBounds(const Bounds& b)
        {
            mBounds = b;
        }
        
    private:
        
        Map *mMap;
        MarkerManager *mManager;
        Marker* mParent;
        
        // Attachments
        std::recursive_mutex mAttachmentMutex;
        std::vector<Marker*> mAttachments;
        
        std::recursive_mutex mAnimationMutex;
        std::map<std::string, std::unique_ptr<Animation>> mAnimations;
        
        // Marker Properties.
        Point mPoint;
        Point mScreenPosition;
        Bounds mBounds;
        Bounds mCompositeBounds;
        bool mOnScreen;
        
        AABBShape mCollisionShape;
        
        MeshShape* mDebugShape;
        
        Point mAnchorPoint;
        Point mAttachmentAnchorPoint;
        Size mCachedSize;
        real mZIndex;
        bool mInteractable;
        bool mZIndexSet;
        bool mVisible;
        bool mHighlighted;
        bool mHighlightsOnTouch;
        bool mPointIsLonLat;
        bool mDraggable;
        bool mDragging;
        real mAlpha;
        bool mAlphaSet;
        real mRotation;
        real mFadeTime;
        real mScale;
        Vector4f mHighlightColor;
        Size mScreenOffset;
        real mCollisionPriority;
        bool mInheritAlpha;
        bool mInheritRotation;
        bool mInheritScale;
        std::shared_ptr<IMarkerListener> mMarkerListener;
        
        void UpdateAlpha(const MapState& state);
        void UpdateAnimations(const MapState& state);
        void SetParent(Marker* parent);
    };
};
