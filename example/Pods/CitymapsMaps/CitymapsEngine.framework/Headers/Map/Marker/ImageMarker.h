//
//  ImageMarker.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 1/21/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Marker/Marker.h>

namespace citymaps
{
    class Image;
    struct IMAGE_MARKER_DESC : public MARKER_DESC
    {
        std::shared_ptr<Image> MarkerImage = nullptr;
        Size MarkerSize = Size(0,0);
        bool Flat = false;
    };
    
    class Sprite;
    class ImageMarker : public Marker
    {
    private:
        
        enum ImageMarkerResourceType
        {
            ImageMarkerResourceNone,
            ImageMarkerResourceKey,
            ImageMarkerResourceAsset,
            ImageMarkerResourceFile
        };
        
    public:
        
        ImageMarker(const IMAGE_MARKER_DESC& desc = IMAGE_MARKER_DESC());
        ~ImageMarker();
        
        // If ImageKey is set, image will replace the image for all ImageMarkers using that key. This marker will begin using that key.
        // Else, we just set the image on this marker.
        void SetImage(std::shared_ptr<Image> image, const std::string& imageKey = "");
        
        // Loads an image from the cache using the given key. Will not display anything if no resource exists for that key.
        void SetImageFromKey(const std::string& key);
        
        // Loads an image from the resource file, caching it using the resourceFile string.
        void SetImageFromResource(const std::string& resourceFile);
        
        // Loads an image from the file, caching it using the file string.
        void SetImageFromFile(const std::string& file);
        
        void SetSize(const Size& size);
        
        void SetFlat(bool flat);
        
        bool IsFlat() const { return mFlat;}
        
        std::shared_ptr<Image> GetImage();
        
        virtual void Update(const MapState& state);
        
    protected:
        
        virtual void OnRender(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        virtual Size CalculateSize();
        
    private:

        bool mFlat;
        bool mSizeSet;
        Size mSize;
        double mMapResolution;
        
        Sprite *mSprite;
        bool mSpriteShared;
        
        // Pending resource
        std::shared_ptr<Image> mImage;
        std::string mImageString;
        ImageMarkerResourceType mPendingResourceType;
        
        void UpdateSprite(IGraphicsDevice *device, std::shared_ptr<Image> newImage, const std::string& key, ImageMarkerResourceType resourceType);
        void DiscardSprite();
    };
}