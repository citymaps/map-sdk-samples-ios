//
//  MarkerManager.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Core/MapState.h>

#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>

#include <CitymapsEngine/Core/Input/InputReader.h>
#include <CitymapsEngine/Map/Input/MapMovementController.h>

namespace citymaps
{
    class Map;
    class Marker;
    class MarkerGroup;
    
    typedef std::vector<Marker *> TMarkerList;
	typedef std::map<std::string, MarkerGroup *> TMarkerGroupMap;
    
    class MarkerManager
    {
    public:
        MarkerManager(Map *map);
        ~MarkerManager();
        
        const TMarkerList& GetVisibleMarkers() const { return mVisibleMarkers; }
        
        MarkerGroup* GetMarkerGroup(const std::string &name);

        void AddMarker(Marker *marker);
        void RemoveMarker(Marker *marker);
        void RemoveAllMarkers();
        
        void SetDraggingMarker(Marker *marker);
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        bool ProcessTouchEvent(const TouchEvent &e);
        bool ProcessMapTouchEvent(const MapTouchEvent &e);
        
    private:
        Map *mMap;
        TMarkerList mMarkers;
        TMarkerList mVisibleMarkers;
        TMarkerGroupMap mMarkerGroups;
        Marker *mFocusedMarker;
        Marker *mDraggingMarker;
        bool mAcceptedLastTouchDown;
        
        Marker* GetMarkerAtPoint(const Point& p);
    };
};
