//
//  UserLocation.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/9/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

namespace citymaps
{
    struct UserLocation
    {
        UserLocation() :
        position(0,0), accuracy(0), direction(0), altitude(0) {}
        
        Point position;
        double accuracy;
        double direction;
        double altitude;
    };
}