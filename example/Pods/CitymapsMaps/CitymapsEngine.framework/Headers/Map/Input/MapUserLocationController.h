//
//  MapUserLocationController.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/GPS/GPSReader.h>
#include <CitymapsEngine/Core/Util/Image.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasCircle.h>
#include <CitymapsEngine/Map/Animation/MarkerAnimator.h>
#include <CitymapsEngine/Map/Input/UserLocation.h>

namespace citymaps
{
    class ImageMarker;
    class Map;
    class IMapListener;
    class IGPSPredictor;
    
    class MapUserLocationController : public IGPSListener
    {
    public:
        MapUserLocationController(Map* map, std::shared_ptr<Image> locationImage);
        ~MapUserLocationController();
        
        void SetListener(IMapListener* listener) { mListener = listener; }
        
        void SetLocationImage(std::shared_ptr<Image> locationImage);
        
        int AddRegion(const Region &region);
        void RemoveRegion(int index);
        void RemoveAllRegions();
        const Region* GetRegionAtIndex(int index);

        void SetLocationMarkerEnabled(bool enabled);
        void SetLocationAccuracyEnabled(bool enabled);
        
        bool IsLocationMarkerEnabled()
        {
            return mLocationMarkerEnabled &&
            mLocationStatus == LocationStatusEnabled;
        }
	bool IsLocationAccuracyEnabled()
        {
            return mLocationAccuracyEnabled;
        }

	void SetGPSPredictor(IGPSPredictor *predictor);
        /* IGPSListener Methods */
        void OnPositionChange(const Point &position, double accuracyMeters);
        void OnDirectionChange(double direction);
        void OnAltitudeChange(double altitude);
        void OnLocationStatusChange(LocationStatus status);
        
        const UserLocation& GetUserLocation() const { return mUserLocation; }
        
        Point GetMarkerPosition() const;
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice* device, RenderState& perspectiveState, RenderState& orthoState);
        
    private:
        UserLocation mUserLocation;
        ImageMarker *mLocationMarker;
        bool mMarkerAdded;
        Map* mMap;
        IMapListener* mListener;
        std::vector<Region> mRegions;
        CanvasCircle* mAccuracyCircle;
        MarkerAnimator mAnimator;
        bool mDisplayAccuracyCircle;
        bool mPositionChanged;
        bool mDirectionChanged;
        bool mLocationMarkerEnabled;
        bool mLocationAccuracyEnabled;
        LocationStatus mLocationStatus;
        
        IGPSPredictor *mGPSPredictor;
    };
};
