//
//  GPSPredictor.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/4/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Input/UserLocation.h>

namespace citymaps
{
    class IGPSPredictor
    {
    public:
        
        virtual void BeginSimulation(const Point &startPosition, const Point &startVelocity) = 0;
        virtual void Update(int deltaMillis) = 0;
        virtual void OnLocationUpdate(const UserLocation &newPosition) = 0;
        
        virtual UserLocation GetCurrentState() = 0;
    };
};