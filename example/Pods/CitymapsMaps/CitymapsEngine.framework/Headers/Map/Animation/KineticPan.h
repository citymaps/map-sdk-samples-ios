//
//  KineticPan.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/1/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    struct KineticPanPoint
    {
        Vector3 point = Vector3(0,0,0);
        double time = 0;
    };
    
    class IKineticPanListener
    {
    public:
        virtual void OnKineticMoveStart() = 0;
        virtual void OnKineticMoveBy(const Vector3 &delta) = 0;
        virtual void OnKineticMoveEnd() = 0;
    };
    
    class KineticPan
    {
    public:
        KineticPan(IKineticPanListener *listener);
        ~KineticPan();
        
        void Start(const Point &point);
        void Move(const Point &point);
        void End(const Point &point);
        void Stop();
        bool IsAnimating() const {return mIsAnimating;}
        void Update(int deltaMS);
        
    private:
        IKineticPanListener *mListener;
        std::vector<KineticPanPoint> mPoints;
        
        double mElapsedTime;
        Vector3 mVelocity;
        Vector3 mKineticOffset;
        Vector3 mLastOffset;
        //Vector3 mVelocity;
        //Vector3 mAcceleration;
        bool mIsAnimating;
        
        void AddPoint(const Point &point);
    };
};
