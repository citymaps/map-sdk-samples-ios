//
//  MarkerAnimator.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 5/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Marker/Marker.h>

namespace citymaps
{
    struct MARKER_ANIMATION_DESC
    {
        Point Position;
    };
    
    class MarkerAnimator
    {
    public:
        MarkerAnimator();
        ~MarkerAnimator();
        
        void Animate(Marker *marker, const MARKER_ANIMATION_DESC &animation, int durationMillis);
        
        void Update(int deltaMillis);
        
        bool IsAnimating() { return mAnimating; }
        
    private:
        int mCurrentTime;
        int mDuration;
        MARKER_ANIMATION_DESC mStart;
        MARKER_ANIMATION_DESC mEnd;
        bool mAnimating;
        Marker *mMarker;
    };
};
