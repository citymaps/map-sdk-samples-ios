//
//  Projection.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    enum ProjectionType
    {
        ProjectionMercator,
        ProjectionLonLat
    };
    
    class Projection
    {
    public:
        virtual Point Project(const Point &point) = 0;
        virtual Point Unproject(const Point &point) = 0;
        virtual Bounds Project(const Bounds &bounds) = 0;
        virtual Bounds Unproject(const Bounds &bounds) = 0;
        
        virtual Bounds GetMaxBounds() = 0;
        
        static Projection* Create(ProjectionType type);
    };
};
