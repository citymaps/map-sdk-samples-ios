//
//  LongLatProjection.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Util/Projection/Projection.h>

namespace citymaps
{
    static const Bounds kLonLatMaxBounds(-180, -90, 180, 90);
    
    class LonLatProjection : public Projection
    {
    public:
        LonLatProjection()
        {
        }
        
        ~LonLatProjection()
        {
        }
        
        Point Project(const Point &point) { return point; }
        Point Unproject(const Point &point) { return point; }
        Bounds Project(const Bounds &bounds) { return bounds; }
        Bounds Unproject(const Bounds &bounds) { return bounds; }
        Bounds GetMaxBounds() { return kLonLatMaxBounds; }
    };
};