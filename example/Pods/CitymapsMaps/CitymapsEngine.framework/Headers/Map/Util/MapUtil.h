//
//  MapUtil.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/2/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    static const real kDotsPerInch = 72.0;
    static const real kInchesPerMeter = 39.3701;
    static const real kMetersPerMile = 1609.344;
    static const real kWorldResolution = 156543.03390625;
    static const real kWorldRadiusKM = 6373.0;
    
    class MapUtil
    {
    public:
        static real GetResolutionFromScale(real scale);
        static real GetScaleFromResolution(real res);
        static real GetResolutionForZoom(real zoom);
        static real GetHaversineDistance(const Point &p1, const Point &p2);
        static real GetBearing(const Point &p1, const Point &p2);
        static real GetZoomFromResolution(real res);
    };
}
