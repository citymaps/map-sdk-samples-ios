//
//  SystemFontReaderIOS.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/27/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Font/SystemFontReader.h>

namespace citymaps
{
    template<>
    class SystemFontReader<PlatformTypeIOS>
    {
    public:
        static void CopyFonts(const std::vector<SystemFontFile> &files);
    };
};
