#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Font/Font.h>
#include <CitymapsEngine/Core/Font/Types.h>

namespace citymaps
{
	typedef std::map<uint64_t, Font *> TFontMap;
    typedef std::map<std::string, FONT_DESC> TFontDescMap;
    
	class FontManager
	{
	public:
		static Font* AddFont(FONT_DESC &fontDesc);
		static Font* GetFont(FONT_DESC &fontDesc);
        static Font* GetFont(const std::string &name, int size);
        static Font* GetDefaultFont();
        static void LoadSystemFont(const std::string &name);
        static void LoadFontConfig(const std::string &filename);

    public:
        
        static const std::string kSystemFont;
        static const int kDefaultFontSize;
        
	private:
		static TFontMap mFonts;
        static TFontDescMap mConfigFonts;
        
        static void CopySystemFonts(const std::string &name);
	};
};