//
//  Interpolators.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 11/4/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    struct Interpolator
    {
        Interpolator(){}
        virtual ~Interpolator(){}
        virtual real operator()(real t) = 0;
        
        real Interpolate(real t)
        {
            return this->operator()(t);
        }
    };
    
    struct LinearInterpolator : public Interpolator
    {
        real operator()(real t)
        {
            return t;
        }
    };
    
    struct QuadraticEaseInInterpolator : public Interpolator
    {
        real operator()(real t)
        {
            return t * t;
        }
    };
    
    struct QuadraticEaseOutInterpolator : public Interpolator
    {
        real operator()(real t)
        {
            return -t * (t - 2);
        }
    };
    
    struct QuadraticEaseInOutInterpolator : public Interpolator
    {
        real operator()(real t)
        {
            if (t < 0.5)
            {
                return 2 * t * t;
            }
            else
            {
                return (-2 * t * t) + (4 * t) - 1;
            }
        }
    };
    
    struct OvershootInterpolator : public Interpolator
    {
        real tension = 2.0;
        
        real operator()(real t)
        {
            t -= 1.0f;
            return t * t * ((tension + 1.0) * t + tension) + 1.0;
        }
    };
    
    struct AnticipateInterpolator : public Interpolator
    {
        real tension = 2.0;
        
        real operator()(real t)
        {
            return t * t * ((tension + 1.0) * t - tension);
        }
    };
}
