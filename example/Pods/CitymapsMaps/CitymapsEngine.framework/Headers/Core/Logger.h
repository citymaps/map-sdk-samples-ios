#pragma once

#ifdef __IOS__
    #include <CitymapsEngine/Core/Util/Platform/iOS/LoggerIOS.h>
#endif

#ifdef __ANDROID__
    #include <CitymapsEngine/Core/Util/Platform/Android/LoggerAndroid.h>
#endif