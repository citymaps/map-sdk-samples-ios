#pragma once

#include <locale>
#include <iomanip>

#include <rapidjson/stringbuffer.h>
#include <rapidjson/document.h>
#include <glm/gtx/norm.hpp>
#include <Poco/Base64Encoder.h>

#include <CitymapsEngine/Core/Util/LonLatWriter.h>
#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/Format/format.h>

namespace citymaps
{
	class Util
	{
	public:
        static int NextPowerOf2(int num);
        static void AtomicIncrement(volatile int32_t *value);
        static void AtomicDecrement(volatile int32_t *value);
        static double GetSystemTime();
        static double GetClockTime();
        
        template <typename T>
        static int sgn(T val) {
            return (T(0) < val) - (val < T(0));
        }
        
        static double MinimumAngle(double start, double dest)
        {
            double angle = std::fmod(dest - start, 360.0);
            
            if(angle > 180)
            {
                angle = -360 + angle;
            }
            else if(angle < -180)
            {
                angle += 360;
            }
            
            return angle;
        };
        
        static double ClampAngle(double angle)
        {
            while (angle > 360)
            {
                angle -= 360;
            }
            
            while (angle < 0)
            {
                angle += 360;
            }
            
            return angle;
        }
        
        static double DistanceFromSegment(const Point& segmentBegin, const Point& segmentEnd, const Point& point)
        {
            Vector2 v(segmentBegin.x, segmentBegin.y);
            Vector2 w(segmentEnd.x, segmentEnd.y);
            Vector2 p(point.x, point.y);
            
            const double length2 = glm::distance2(v, w);
            if (length2 == 0.0)
                return glm::distance(p, v);
            
            const double t = glm::dot(p - v, w - v) / length2;
            if (t < 0.0)
            {
                return glm::distance(p, v);       // Beyond the 'v' end of the segment
            }
            else if (t > 1.0)
            {
                return glm::distance(p, w);  // Beyond the 'w' end of the segment
            }
            
            const Vector2 projection = v + t * (w - v);  // Projection falls on the segment
            return glm::distance(p, projection);
        }
        
        /**
         Returns -1 if point projected before p1
         Returns 0 if point projected onto the segment, and the result in outPoint
         Returns 1 if point projected after p2
         */
        
        static int ProjectPointOntoSegment(const Point& segmentBegin, const Point& segmentEnd, const Point& point, Point& outPoint)
        {
            Vector2 v(segmentBegin.x, segmentBegin.y);
            Vector2 w(segmentEnd.x, segmentEnd.y);
            Vector2 p(point.x, point.y);
            
            const double length2 = glm::distance2(v, w);
            if (length2 == 0.0)
            {
                outPoint = point;
                return -1;
            }
            
            const double t = glm::dot(p - v, w - v) / length2;
            if (t < 0.0)
            {
                return -1;
            }
            else if (t > 1.0)
            {
                return 1;
            }
            
            const Vector2 projection = v + t * (w - v);  // Projection falls on the segment
            
            outPoint = Point(projection.x, projection.y);
            return 0;
        }
        
        static double AngleBetweenPoints(const Point& p1, const Point& p2)
        {
            real dx = p2.x - p1.x;
            real dy = p2.y - p1.y;
            
            if(dx == 0 && dy == 0)
            {
                return 0.0;
            }
            else
            {
                real angle = atan(dy / dx);
                return glm::degrees(angle);
            }
        }
        
        template <typename T>
        static T Lerp(const T& max, const T& min, const T& value)
        {
            return min * (static_cast<T>(1) - value) + max * value;
        }
        
        static float ByteToFloatTable[256];
        static byte FloatToByte(float f)
        {
            union { float f; uint32_t i; } u;
            
            u.f = 32768.0f + f * (255.0f / 256.0f);
            return (byte)u.i;
        }
        
        template <typename StringType = std::string, typename CharType = char>
        static StringType ReadStringFromStream(std::stringstream &stream)
        {
            CharType c;
            StringType outStr;
            
            while (true) {
                stream.read((char *)&c, sizeof(CharType));
                
                if (c == 0) {
                    return outStr;
                }
                
                outStr.push_back(c);
            }
        }
        
        template <typename StringType = std::string>
        static void StringToUpper(StringType &string)
        {
            for (int i = 0; i < string.size(); i++)
            {
                string[i] = std::toupper(string[i]);
            }
        }
        
        template <typename T>
        static uint64_t Hash(T value)
        {
            std::hash<T> hash_fn;
            return hash_fn(value);
        }
        
        template <typename T>
        static bool WithinRange(T min, T max, T value)
        {
            return (min < value && max > value);
        }
        
        static std::string& StringReplace(std::string &subject, std::string search, std::string replace)
        {
            size_t pos = 0;
            while ((pos = subject.find(search, pos)) != std::string::npos) {
                subject.replace(pos, search.length(), replace);
                pos += replace.length();
            }
            
            return subject;
        }
        
        static void StringSplit(std::vector<std::string>& tokens, const std::string& string, const std::string& delimiter)
        {
            size_t delimSize = delimiter.size();
            auto npos = std::string::npos;
            
            if (delimSize == 0)
                return;
            
            size_t start = 0, end = 0;
            
            while (end != npos)
            {
                end = string.find(delimiter, start);
                
                // If at end, use length=maxLength.  Else use length=end-start.
                tokens.push_back( string.substr( start, (end == npos) ? npos : end - start));
                
                // If at end, use start=maxSize.  Else use start=end+delimiter.
                start = ( ( end > (std::string::npos - delimSize) ) ? npos : end + delimSize);
            }
        }
        
        template <typename T>
        static T Clamp(T val, T min, T max)
        {
            return (val < min ? min : (val > max ? max : val));
        }
        
        template <typename T>
        static T Wrap(T val, T min, T max)
        {
            if (val < min)
            {
                return max + (val - min);
            }
            
            if (val > max)
            {
                return min + (val - max);
            }
            
            return val;
        }
        
        template <typename T>
        static void VectorRemoveAtIndex(std::vector<T> &vector, int index)
        {
            vector.erase(vector.begin() + index);
        }
        
        static void SplitBounds(const Bounds& bounds, std::vector<Bounds>& sections, int rows, int columns)
        {
            float width = bounds.Width();
            float height = bounds.Height();
            float sectionWidth = width / columns;
            float sectionHeight = height / rows;
            
            for(int i = 0; i < rows; ++i)
            {
                float y1 = bounds.min.y + sectionHeight * i;
                float y2 = y1 + sectionHeight;
                
                for(int j = 0; j < columns; ++j)
                {
                    float x1 = bounds.min.x + sectionWidth * j;
                    float x2 = x1 + sectionWidth;
                    
                    sections.push_back(Bounds(x1, y1, x2, y2));
                }
            }
        }
        
        static std::string URLSafeBase64Encode(const std::string &str)
        {
            std::ostringstream stream;
            Poco::Base64Encoder base64(stream);
            base64.rdbuf()->setLineLength(INFINITY);
            base64 << str;
            base64.close();
            
            std::string safeStr = stream.str();
            
            Util::StringReplace(safeStr, "+", "-");
            Util::StringReplace(safeStr, "=", "_");
            Util::StringReplace(safeStr, "/", "~");
            
            return safeStr;
        }
        
        static std::string ByteArrayToHexString(const TByteArray &data)
        {
            std::stringstream ss;
            ss << std::hex << std::setfill('0');
            
            for (auto b : data) {
                ss << std::setw(2) << (int)b;
            }
            
            return ss.str();
        }
        
        static std::string RapidJsonToString(rapidjson::Value& value)
        {
            // Write the json to a string.
            rapidjson::StringBuffer buffer;
            LonLatWriter<rapidjson::StringBuffer> writer(buffer);
            value.Accept(writer);
            
            return std::string(buffer.GetString(), buffer.GetSize());
        }
        
        template <typename... Args>
        static std::string Format(const char *format, const Args & ... args)
        {
            return fmt::sprintf(format, args...);
        }
        
        template<typename T>
        static T LinearTween(double time, double duration, T start, T change)
        {
            return change * (time / duration) + start;
        }
        
        template<typename T>
        static T QuadraticEaseIn(double time, double duration, T start, T change)
        {
            time /= duration;
            return change * (time * time) + start;
        }
        
        template<typename T>
        static T QuadraticEaseOut(double time, double duration, T start, T change)
        {
            time /= duration;
            return (change * -1.0) * time * (time - 2) + start;
        }
        
        template<typename T>
        static T QuadraticEaseInOut(double time, double duration, T start, T change)
        {
            time /= duration * 0.5;
            if (time < 1)
            {
                return change * 0.5 * time * time * time + start;
            }
            time -= 2;
            return (change * 0.5) * (time * time * time + 2) + start;
        }
    };
};
