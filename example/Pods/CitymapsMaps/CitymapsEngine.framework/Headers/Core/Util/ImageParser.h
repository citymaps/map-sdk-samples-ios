/*
 * ImageParser.h
 *
 *  Created on: Jul 24, 2013
 *      Author: eddiekimmel
 */

#include <CitymapsEngine/Core/Disk/File.h>
#pragma once

namespace citymaps
{
	struct ImageBitmap
	{
        ImageBitmap()
        :Bytes(), ImageSize(0,0)
        {
        }

        ImageBitmap(const TByteArray& data, const Size& size)
        :Bytes(data), ImageSize(size)
        {
        }
        
        ImageBitmap(const Size &size) :
            ImageSize(size)
        {
            Bytes.resize(glm::ceil(ImageSize.width * ImageSize.height * 4));
        }
        
        ImageBitmap(int width, int height) :
            ImageSize(width, height)
        {
            Bytes.resize(ImageSize.width * ImageSize.height * 4);
        }

        TByteArray Bytes;
        Size ImageSize;
	};

	class ImageParser
	{
    public:
        
        static std::shared_ptr<ImageBitmap> Parse(const TByteArray& array, ImageType type, bool bitmapSizeImmutable = false);
        
        static ImageType ParseImageType(const TByteArray &array);
        
        static std::shared_ptr<ImageBitmap> ParsePNG(const File& file, bool bitmapSizeImmutable = false)
        {
            const TByteArray& data = file.Data();
            return ParsePNG(&data[0], data.size(), bitmapSizeImmutable);
        }
        
        static std::shared_ptr<ImageBitmap> ParsePNG(const TByteArray& bytes, bool bitmapSizeImmutable = false)
        {
            return ParsePNG(&bytes[0], bytes.size(), bitmapSizeImmutable);
        }
        
        static std::shared_ptr<ImageBitmap> ParsePNG(const unsigned char* bytes, size_t size, bool bitmapSizeImmutable = false);
        
        static std::shared_ptr<ImageBitmap> ParseJPEG(const File& file, bool bitmapSizeImmutable = false)
        {
            const TByteArray& data = file.Data();
            return ParseJPEG(&data[0], data.size(), bitmapSizeImmutable);
        }
        
        static std::shared_ptr<ImageBitmap> ParseJPEG(const TByteArray& bytes, bool bitmapSizeImmutable = false)
        {
            return ParseJPEG(&bytes[0], bytes.size(), bitmapSizeImmutable);
        }
        
        static std::shared_ptr<ImageBitmap> ParseJPEG(const unsigned char* bytes, size_t size, bool bitmapSizeImmutable = false);
        
        static std::shared_ptr<ImageBitmap> GetCachedImage(int width, int height);
        
	};
}
