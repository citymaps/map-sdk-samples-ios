//
//  PerfTimer.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/2/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    struct PerfTimerEvent
    {
        double time;
        std::string eventName;
    };
    
    class PerfTimer
    {
    public:
        void Start();
        void End();
        void Print();
        
        void AddEvent(const std::string &event);
        
    private:
        std::vector<PerfTimerEvent> mEvents;
    };
};
