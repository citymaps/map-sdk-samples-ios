//
//  ObjectPool.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/18/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    static const int kDefaultPoolSize = 50;
    
    enum ObjectPoolReturnStrategy
    {
        ObjectPoolReturnAtFront,
        ObjectPoolReturnAtBack
    };
    
    template <typename T>
    using ObjectCreateCallback = std::function<T*()>;
    
    template <typename T>
    class ObjectPool
    {
        struct ObjectPoolRef
        {
            ObjectPool<T>* Pool;
            std::mutex Mutex;
        };
        struct ObjectPoolDeleter
        {
            std::shared_ptr<ObjectPoolRef> PoolRef;
            
            ObjectPoolDeleter(std::shared_ptr<ObjectPoolRef> ref)
            :PoolRef(ref)
            {}
            
            void operator()(T* obj) const;
        };
        
        friend class ObjectPoolDeleter;
        
    public:

        ObjectPool(int defaultSize = kDefaultPoolSize, ObjectPoolReturnStrategy strategy = ObjectPoolReturnAtFront);
        ObjectPool(ObjectCreateCallback<T> objectCreateCallback, int defaultSize = kDefaultPoolSize, ObjectPoolReturnStrategy strategy = ObjectPoolReturnAtFront);
        ~ObjectPool();
        
        std::shared_ptr<T> GetFreeObject();
                                                                       
        void GetFreeObjects(unsigned int num, std::vector<std::shared_ptr<T> >& ret);

        void Apply(const std::function<void(T*)>& function);
        
    private:
        
        std::vector<T*> mObjectPool;
        std::list<T*> mFreeList;
        ObjectCreateCallback<T> mObjectCreateCallback;
        std::shared_ptr<ObjectPoolRef> mRef;
        ObjectPoolReturnStrategy mReturnStrategy;
        
        int mSize;
        int mFreeListSize;
        std::mutex mLock;
        
        void ReturnObject(T* object);
        void ExtendPool(int amount);
    };
}

#include <CitymapsEngine/Core/Util/ObjectPool.inl>

