//
//  Preferences.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/15/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <string>
#include <rapidjson/document.h>

namespace citymaps
{
    static const std::string kDefaultPreferencesFilename = "citymapsengine_preferences_file.json";

    class Preferences
    {
    public:
        
        static int GetInt(const std::string& key, int defaultValue = 0);
        static float GetFloat(const std::string& key, float defaultValue = 0.f);
        static bool GetBool(const std::string& key, bool defaultValue = false);
        static std::string GetString(const std::string& key, const std::string& defaultValue = "");
        
        static void SetInt(const std::string& key, int value);
        static void SetFloat(const std::string& key, float value);
        static void SetBool(const std::string& key, bool value);
        static void SetString(const std::string& key, const std::string& value);
        
        static void Synchronize();
        
    private:
        
        static rapidjson::Document msJsonFile;
        static bool msJsonLoaded;
        static void LoadFile();
    };
}
