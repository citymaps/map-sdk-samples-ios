//
//  ObjectPool.cpp
//  vectormap2
//
//  Created by Adam Eskreis on 7/18/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

namespace citymaps
{
    template <typename T>
    void ObjectPool<T>::ObjectPoolDeleter::operator()(T* obj) const
    {
        PoolRef->Mutex.lock();
        
        if (PoolRef->Pool != NULL)
        {
            PoolRef->Pool->ReturnObject(obj);
        }
        else
        {
            delete obj;
        }
        
        PoolRef->Mutex.unlock();
    }
    
    template <typename T>
    ObjectPool<T>::ObjectPool(int defaultSize, ObjectPoolReturnStrategy strategy)
    :ObjectPool<T>([](){return new T();}, defaultSize, strategy)
    {
    }
    
    template <typename T>
    ObjectPool<T>::ObjectPool(ObjectCreateCallback<T> objectCreateCallback, int defaultSize, ObjectPoolReturnStrategy strategy) :
        mSize(0),
        mFreeListSize(0),
        mObjectCreateCallback(objectCreateCallback),
        mReturnStrategy(strategy)
    {
        mRef = std::make_shared<ObjectPoolRef>();
        mRef->Pool = this;
        
        this->ExtendPool(defaultSize);
    }
    
    template <typename T>
    ObjectPool<T>::~ObjectPool()
    {
        std::lock_guard<std::mutex> lock(mLock);
        std::lock_guard<std::mutex> refLock(mRef->Mutex);

        mRef->Pool = NULL;
        
        for (T* obj : mFreeList)
        {
            delete obj;
        }
        
        mFreeListSize = 0;
        mFreeList.resize(0);
    }
     
    template <typename T>
    std::shared_ptr<T> ObjectPool<T>::GetFreeObject()
    {
        std::lock_guard<std::mutex> lock(mLock);
        if (mFreeListSize == 0)
        {
            this->ExtendPool(mSize);
        }
        
        T* obj = mFreeList.front();
        mFreeList.pop_front();
        mFreeListSize--;
        
        return std::shared_ptr<T>(obj, ObjectPoolDeleter(mRef));
    }
    
    template <typename T>
    void ObjectPool<T>::GetFreeObjects(unsigned int num, std::vector<std::shared_ptr<T> >& ret)
    {
        std::lock_guard<std::mutex> lock(mLock);
        
        if (mFreeListSize < num)
        {
            int resizeAmount = num - mFreeListSize + mSize;
            this->ExtendPool(resizeAmount);
        }
        
        ret.reserve(num);
        for(unsigned int i = 0; i < num; ++i)
        {
            T* obj = mFreeList.front();
            mFreeList.pop_front();
            mFreeListSize--;
            ret.push_back(std::shared_ptr<T>(obj, ObjectPoolDeleter(mRef)));
        }
        
    }
    
    template <typename T>
    void ObjectPool<T>::Apply(const std::function<void(T*)>& function)
    {
        std::lock_guard<std::mutex> lock(mLock);
        for (T* obj : mObjectPool)
        {
            function(obj);
        }
    }
    
    template <typename T>
    void ObjectPool<T>::ReturnObject(T* object)
    {
        std::lock_guard<std::mutex> lock(mLock);
        
        if (mReturnStrategy == ObjectPoolReturnAtFront) {
            mFreeList.push_front(object);
        } else if(mReturnStrategy == ObjectPoolReturnAtBack) {
            mFreeList.push_back(object);
        }
        
        mFreeListSize++;
    }
    
    template <typename T>
    void ObjectPool<T>::ExtendPool(int amount)
    {
        mObjectPool.reserve(mObjectPool.size() + amount);
        mFreeListSize += amount;
        for(int i = 0; i < amount; i++)
        {
            T* obj = mObjectCreateCallback();
            mObjectPool.push_back(obj);
            mFreeList.push_back(obj);
        }
        
        mSize += amount;
    }
};
