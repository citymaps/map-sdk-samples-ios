//
//  MemoryCache.h
//  vectormap2
//
//  Created by Lion User on 08/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/SafeObject.h>

namespace citymaps
{
    static const int kMemoryCacheDefaultSize = 50;
    
    template <typename T>
    using ObjectRemovedCallback = std::function<void(T)>;
    
    template <typename T>
    class MemoryCache
    {
    public:
        MemoryCache(int maxSize = kMemoryCacheDefaultSize);
        virtual ~MemoryCache();
        
        void AddObject(T object, uint64_t key);
        T RetrieveObject(uint64_t key);
        void RemoveObject(T object);
        void RemoveObjectForKey(uint64_t key);
        void Clear();
        
        int GetMaxSize() const { return mMaxSize;}
        
    private:
        struct MemoryCacheNode
        {
            MemoryCacheNode(T _value, uint64_t _key) :
            value(_value), key(_key) {}
            
            T value;
            uint64_t key;
        };
        
        int mMaxSize;
        SafeObject<std::list<MemoryCacheNode>> mCacheNodes;
    };
};

#include <CitymapsEngine/Core/Util/MemoryCache.inl>
