#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/OperationQueue.h>

namespace citymaps
{
	typedef std::function<void()> OperationCallback;

	class AnonymousOperation : public Operation
	{
	public:
		AnonymousOperation(OperationCallback operation);
		~AnonymousOperation();

		void Start();
        void Cancel();

	private:
		OperationCallback mCallback;
	};

};