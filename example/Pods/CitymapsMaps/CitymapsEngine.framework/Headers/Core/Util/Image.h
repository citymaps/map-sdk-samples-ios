//
//  Image.h
//  vectormap2
//
//  Created by Lion User on 14/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Disk/ImageResource.h>
#include <CitymapsEngine/Core/Util/ImageParser.h>
#include <CitymapsEngine/Core/Util/ObjectPool.h>

namespace citymaps
{
    enum ImageBlendFunction
    {
        ImageBlendSrcAlpha,
        ImageBlendOne,
        ImageBlendOneMinusSrcAlpha
    };
    
    class Image
    {
        friend class ImageParser;
        
    public:

        Image();
        Image(const Image &other);
        Image(Image *other);
        Image(const File& file, ImageType type = ImageTypeUnknown);
        Image(const TByteArray& data, ImageType type = ImageTypeUnknown, const Vector2i& bitmapSize = Vector2i(0,0));
        Image(const TByteArray& bitmapData, const Size& size, float scale = 1.0f);
        Image(const ImageResource& imageResource);
        Image(const std::string &resource);
        Image(const Size &size);

        ~Image();
        
        void LoadFromData(const TByteArray& data, ImageType type = ImageTypeUnknown, const Vector2i& bitmapSize = Vector2i(0,0));
        
        void Resize(const Size& newSize);
        void Resize(const Size& newSize, Image &outImage);
        void Crop(Bounds bounds);
        void GetSubImage(Bounds bounds, Image &outImage);
        void ApplyAlphaMask(Image &mask);
        void EmbedImage(Image &innerImage, Bounds bounds, ImageBlendFunction srcFunction = ImageBlendSrcAlpha, ImageBlendFunction destFunction = ImageBlendOneMinusSrcAlpha);
        
        void FillImage(const Vector4f& color);
        void TintImage(const Vector4f& color);
        void TintImage(const Vector4f& color, Image &outImage);
        
        void SetAlpha(real alpha);
        void SetRoundedCorners(real radius);
        
        void Save(std::string filename);
        
        Vector4f GetColorAtPixel(const Vector2i& pixel);
        
        const TByteArray& GetImageData() const { return mImageBitmap->Bytes; }
        const byte* GetImageDataPointer() const { return mImageBitmap ? &mImageBitmap->Bytes[0] : NULL;}
        
        const Size& GetImageSize() const { return mImageBitmap->ImageSize; }
        const Vector2i GetImageSizeInt() const { return Vector2i(glm::round(mImageBitmap->ImageSize.width), glm::round(mImageBitmap->ImageSize.height)); }
        Vector2i GetDataSize() const
        {
        	return ConvertToDataCoords(mImageBitmap->ImageSize);
        }
        
        static void RegisterImagePoolSize(const Vector2i& size, int defaultSize = 50);
        static void RegisterImagePoolSize(int x, int y, int defaultSize = 50);
        
        static float DetermineAlpha(float alpha, ImageBlendFunction func);
        
    private:
        
        typedef ObjectPool<ImageBitmap> ImagePool;
        static std::map<uint64_t, ImagePool*> msImagePools;
        
    private:
        std::shared_ptr<ImageBitmap> mImageBitmap;
        float mImageScale;
        bool mSizeImmutable;
        
        Vector2i ConvertToDataCoords(const Size& s) const
        {
        	return Vector2i(glm::round(s.width * mImageScale), glm::round(s.height * mImageScale));
        }
        void Load(const TByteArray& data, ImageType type);
        
        void ResizeToDataSize(const Vector2i& dataSize);
        
        void ResizeToDataSize(const Vector2i& dataSize, Image& outImage);
        
        // Returns True if the pixel was opaque.
        bool RoundedCornersTest(int x, int y, float r, float d);
        
        static std::shared_ptr<ImageBitmap> GetCachedImage(const Vector2i& size);
        static std::shared_ptr<ImageBitmap> GetCachedImage(int x, int y);

        
    };
};
