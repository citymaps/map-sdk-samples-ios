//
//  AppData.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 3/19/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <string>
#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    class AppData
    {
    public:
        
        static std::string GetIdentifier();
    };
    
    template <typename Platform = CurrentPlatform>
    class AppDataImpl
    {
    public:
        static std::string GetIdentifier();
    };
};
