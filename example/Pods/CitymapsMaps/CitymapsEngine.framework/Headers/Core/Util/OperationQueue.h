//
//  OperationQueue.h
//  vectormap2
//
//  Created by Lion User on 05/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <condition_variable>
#include <CitymapsEngine/CitymapsEngine.h>
#include <Poco/ThreadPool.h>

namespace citymaps
{
    class OperationQueue;
    class Operation
    {
        friend class OperationQueue;
        
    public:
        Operation() :
            mExecuting(false), mCancelled(false), mFinished(false), mPriority(0) {}
        
        virtual ~Operation() {}
        
        virtual void Start() = 0;
        virtual void Cancel() = 0;
        
        void SetOperationQueue(OperationQueue *queue) { mQueue = queue; }
        OperationQueue * GetOperationQueue() { return mQueue; }
        
        bool IsExecuting() { return mExecuting; }
        bool IsCancelled() { return mCancelled; }
        bool IsFinished() { return mFinished; }
        
        bool IsHigherPriorityThan(Operation &other)
        {
            return mPriority > other.mPriority;
        }
        
    protected:
        void SetExecuting(bool executing) { mExecuting = executing; }
        void SetCancelled(bool cancelled) { mCancelled = cancelled; }
        void SetFinished(bool finished) { mFinished = finished; }
        
    private:
        bool mExecuting;
        bool mCancelled;
        bool mFinished;
        uint32_t mPriority;
        OperationQueue *mQueue;
        
        void SetPriority(uint32_t priority) { mPriority = priority; }
    };
    
    class OperationRunnable : public Poco::Runnable
    {
    public:

        OperationRunnable(OperationQueue* queue)
        :mOperationQueue(queue)
        {}

        void run();

    private:
        
        OperationQueue* mOperationQueue;
    };
    
    typedef std::priority_queue<std::shared_ptr<Operation>> TOperationQueue;
    typedef std::vector<std::shared_ptr<Operation>> TOperationList;
    
    static const int kDefaultMaxOperationQueueThreads = 4;
    
    class OperationQueue
    {
        friend class OperationRunnable;
    public:

        OperationQueue(int maxThreads = kDefaultMaxOperationQueueThreads);
        ~OperationQueue();
        
        void AddOperation(std::shared_ptr<Operation> operation, uint32_t priority = 0);
        void CancelAllOperations();
        void WaitForOperations();
        
        size_t NumOperations() { return mOperationQueue.size(); }
        size_t NumActiveOperations() {return mActiveOperations.size(); }

        bool IsEmpty()
        {
            std::lock_guard<std::recursive_mutex> lock(mQueueLock);
            return mOperationQueue.size() == 0 && mActiveOperations.size() == 0;
        }
        
        bool HasRunningOperations()
        {
            std::lock_guard<std::recursive_mutex> lock(mQueueLock);
            return mOperationQueue.size() == 0 && mActiveOperations.size() == 0;
        }
        
    private:
        
        std::recursive_mutex mQueueLock;
        Poco::ThreadPool* mThreadPool;
        OperationRunnable mRunnable;
        TOperationQueue mOperationQueue; 
        TOperationList mActiveOperations;
        int mMaxThreads;
        int mNumOperations;

        std::condition_variable_any mWaitCondition;
        void ProcessQueue();
        
        static void ThreadCallback(OperationQueue *queue);
        
    };
}

namespace std
{
    template<>
    struct less<citymaps::Operation>
    {
    public:
        bool operator()(std::shared_ptr<citymaps::Operation> current, std::shared_ptr<citymaps::Operation> other)
        {
            return other->IsHigherPriorityThan(*current);
        }
    };
};
