//
//  CEUtil.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <CitymapsEngine/CitymapsEngine.h>
namespace citymaps
{
    class CEUtil
    {
    public:
        
        static void DataFromUIImage(UIImage* image, TByteArray& outData, Size& outSize, CGFloat& outscale);
        static Vector4f RGBAFromUIColor(UIColor* color);
        
        static BOOL GetMemoryStats(size_t& totalMem, size_t& usedMem, size_t& freeMem);
    };
}
