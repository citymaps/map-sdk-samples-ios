//
//  LoggerIOS.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/15/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <string>
#include <CitymapsEngine/Core/Util/Format/format.h>

namespace citymaps
{
    class Logger
    {
    public:
        
        template <typename... Args>
        static void Log(const std::string& format, const Args & ... args)
        {
            try {
                Log(fmt::sprintf(format, args...));
            } catch (std::exception e){
                
            }
            
        }
        
        template <typename... Args>
        static void Alert(const std::string& format, const Args & ... args)
        {
            try {
                Alert(fmt::sprintf(format, args...));
            } catch (std::exception e){
                
            }
        }
        static void Log(const std::string& message);
        static void Alert(const std::string& message);
        
    };
}
