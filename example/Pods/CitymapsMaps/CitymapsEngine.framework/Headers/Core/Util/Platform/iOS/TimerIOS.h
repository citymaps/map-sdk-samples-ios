/*
 * TimerIOS.h
 *
 *  Created on: Jul 29, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/PlatformTimer.h>

namespace citymaps
{
	template<>
	class PlatformTimer<PlatformTypeIOS> : public BasePlatformTimer
	{
    public:
        PlatformTimer(int interval, Timer* timer);
        ~PlatformTimer();
        void Start();
        
        void Stop();
        
    private:
        void *mObjCTimer;

	};
}
