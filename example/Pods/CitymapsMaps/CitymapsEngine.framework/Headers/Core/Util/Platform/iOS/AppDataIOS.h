//
//  AppDataIOS.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 3/19/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Util/AppData.h>

namespace citymaps
{
    template <>
    class AppDataImpl<PlatformTypeIOS>
    {
    public:
        static std::string GetIdentifier();
    };
};