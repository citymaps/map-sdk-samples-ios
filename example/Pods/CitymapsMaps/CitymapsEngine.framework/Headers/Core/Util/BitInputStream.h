#pragma once

namespace citymaps
{
    class BitInputStream
    {
    public:
        
        BitInputStream(const unsigned char* data)
        : mPtr(data), mIndex(0), mBit(0)
        {
        }
        
        template<typename T>
        T Read(unsigned int num)
        {
            T value;
            
            while (num >= (8 - mBit))
            {
                int read = 8 - mBit;
                value <<= read;
                value |= ReadRestOfByte();
                num -= read;
            }
            
            value <<= num;
            value |= ReadBits(num);
            return value;
        }
        
    private:
        const unsigned char* mPtr;
        unsigned int mIndex;
        unsigned int mBit;
        
        unsigned char ReadRestOfByte()
        {
            unsigned char value = mPtr[mIndex];
            value <<= mBit;
            value >>= mBit;
            
            mIndex++;
            mBit = 0;
            return value;
        }
        
        unsigned char ReadBits(unsigned int num)
        {
            unsigned char remaining = 8 - mBit;
            
            unsigned char value = mPtr[mIndex];
            value <<= mBit;
            value >>= mBit;
            value >>= remaining - num;
            mBit = (mBit + num) % 8;
            
            if (remaining == num)
                mIndex++;
            
            return value;
        }
    };
}