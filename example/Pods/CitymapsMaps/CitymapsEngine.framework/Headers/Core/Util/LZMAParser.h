//
//  LZMAParser.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 2/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/ObjectPool.h>

namespace citymaps
{
    class LZMAParser
    {
    public:
        static bool Inflate(const TByteArray &inData, TByteArray &outData);
        
        static ObjectPool<TByteArray> sMemoryPool;
    };
};


