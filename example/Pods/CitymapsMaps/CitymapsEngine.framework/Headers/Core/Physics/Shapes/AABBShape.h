//
//  AABBShape.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/4/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Physics/Shapes/GeometricShape.h>

namespace citymaps
{
    class AABBShape : public BaseGeometricShape
    {
    public:
        AABBShape();
        AABBShape(const BoundingBox &extents);
        AABBShape(const Vector3 &center, const Vector3 &dimensions);
        
        void SetCenter(const Vector3 &center)
        {
            mCenter = center;
            this->CalculateBoundingBox();
        }
        
        void SetDimensions(const Vector3 &dimensions)
        {
            mDimensions = dimensions;
            this->CalculateBoundingBox();
        }
        
        void Set(const Vector3 &center, const Vector3 &dimensions)
        {
            mCenter = center;
            mDimensions = dimensions;
            this->CalculateBoundingBox();
        }
        
        const Vector3& GetCenter() const { return mCenter; }
        const Vector3& GetDimensions() const { return mDimensions; }
        
        bool Contains(const Vector3 &point) const;
        bool Contains(const BoundingBox &box) const;
        bool Intersects(const BoundingBox &box) const;
        
        bool Intersects(const IGeometricShape* other) const { return other->Intersects(this); }
        
        bool Intersects(const AABBShape* other) const
        {
            return IntersectionTest::AABBIntersectsAABB(this, other);
        }
        
        bool Intersects(const OBBShape* other) const
        {
            return IntersectionTest::AABBIntersectsOBB(this, other);
        }
        
        bool Intersects(const CapsuleShape* other) const
        {
            return IntersectionTest::AABBIntersectsCapsule(this, other);
        }
        
    private:
        Vector3 mCenter;
        Vector3 mDimensions;
        
        void CalculateBoundingBox()
        {
            BoundingBox &box = this->GetMutableBoundingBox();
            box.min = (mCenter + this->GetPosition()) - mDimensions;
            box.max = (mCenter + this->GetPosition()) + mDimensions;
        }
    };
};