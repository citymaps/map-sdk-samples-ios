//
//  CollisionGroup.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/4/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/EngineTypes.h>
#include <CitymapsEngine/Core/Physics/Shapes/BoundingBox.h>

#include <mutex>
#include <string>
#include <list>

namespace citymaps
{
    class IGeometricShape;
    class CollisionGroup
    {
    public:
        
        CollisionGroup(const std::string& name = "CitymapsAnonCollisionGroup");
        ~CollisionGroup();
        void AddGeometricShape(IGeometricShape* shape);
        void RemoveGeometricShape(IGeometricShape* shape);
        void Clear();
        bool Intersects(IGeometricShape* shape);
        bool Intersects(const BoundingBox& shape);
        bool IntersectingShapes(IGeometricShape* testShape, std::vector<IGeometricShape*>& collidingShapes);
        
        bool Contains(const Vector3& point);
        bool Contains(const BoundingBox& point);
        size_t Size()
        {
            std::lock_guard<std::mutex> lock(mMutex);
            return mShapes.size();
        }
        
    private:
        
        std::string mName;
        std::mutex mMutex;
        std::list<IGeometricShape*> mShapes;
        IGeometricShape* mShapeOfLastCollision;
    };
}