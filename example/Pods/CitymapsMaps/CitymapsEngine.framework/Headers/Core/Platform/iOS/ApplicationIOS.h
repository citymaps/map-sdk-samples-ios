//
//  ApplicationIOS.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#import <Foundation/Foundation.h>
#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Application.h>

namespace citymaps
{
    class ApplicationIOS : public Application<PlatformTypeIOS>
    {
    public:
        
       ApplicationIOS();
       virtual ~ApplicationIOS();
    
       bool Initialize(int width, int height, int immediateJobThreads = 2, int longTermJobThreads = 1);
        
    private:
        
        static IGraphicsDevice* sGraphicsDevice;
        
    };
};
