//
//  CELocationParams.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 This is the set of parameters used when activating GPS tracking with [CEWindowIOS startUpdatingLocationWithParams:].
 */
@interface CELocationParams : NSObject

typedef enum{
    kCELocationAccuracyCoarse,
    kCELocationAccuracyFine,
} CELocationAccuracy;

/** 
 * @name Initializers
 *  
 */

/** Initializes a new CELocationParams. The default accuracy is kCELocationAccuracyCoarse
 */
-(id)init;

/** Initializes a new CELocationParams for the given parameters.
 
 @param accuracy Should be set to either kCELocationAccuracyCoarse or kCELocationAccuracyFine
 @param minDistance The minimum distance (in meters) the user must have moved to trigger an event.
 @param minTime The minimum time (in seconds) between events.
 */
-(id)initWithAccuracy:(CELocationAccuracy)accuracy minDistance:(int)minDistance minTime:(float)minTime;

/**
 * @name Properties
 *  
 */

/** The accuracy parameter. It has a value of either kCELocationAccuracyCoarse or kCELocationAccuracyFine */
@property (nonatomic, assign) CELocationAccuracy accuracy;

/** The minimum distance (in meters) the user must have moved to trigger an even. */
@property (nonatomic, assign) double minDistance;

/** The minimum time (in seconds) between events. */
@property (nonatomic, assign) float minTime;

@end
