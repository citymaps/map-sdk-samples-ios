//
//  HTTPConnection.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Network/HTTPRequest.h>
#include <CitymapsEngine/Core/Network/HTTPResponse.h>
#include <CitymapsEngine/Core/Network/Socket.h>
#include <CitymapsEngine/Core/Util/OperationQueue.h>
#include <CitymapsEngine/Core/Util/Disk/FileCache.h>
#include <CitymapsEngine/Core/Util/ObjectPool.h>

namespace citymaps
{
    typedef std::function<void(const HTTPResponse &)> THTTPSuccessCallback;
    typedef std::function<void(NetworkResponseStatus, const HTTPRequest &)> THTTPFailureCallback;
    
    static const int kDefaultMaxHTTPThreads = 5;
    static const std::string kHTTPCacheDir = "Network/";
    static const uint32_t LOG_BIT = 1 << 0;
    
    class HTTPConnectionImpl;
    
    class ICacheFilenameTransformer
    {
    public:
        virtual std::string GenerateCacheFilename(const std::string& url) = 0;
    };
    
    class HTTPConnection
    {
        friend class HTTPConnectionImpl;
        
    public:
        HTTPConnection();
        ~HTTPConnection();
        
        NetworkResponseStatus Execute(HTTPRequest &request, HTTPResponse &outResponse, double timeout = kDefaultTimeout, uint32_t flags = LOG_BIT);
        bool ExecuteAsync(HTTPRequest &request, THTTPSuccessCallback success, THTTPFailureCallback failure, double timeout = kDefaultTimeout, uint32_t priority = 0);
        
        void Cancel(bool clearCallbacks = false);
        bool IsCancelled();
        
        void AddCacheFilenameTransformer(ICacheFilenameTransformer *transformer);
        
        // Causes the connection to not be cancelled if this HTTPConnection goes out of scope.
        void Detach();
        
        static FileCache* GetFileCache();
        
        static void Initialize();
        static void CancelAllDownloads();
        static void SetNetworkCacheEnabled(bool enabled);

        static bool IsNetworkAvailable();
        static bool IsNetworkCacheEnabled();
        static std::shared_ptr<HTTPConnection> New();
        
        
    private:
        std::shared_ptr<HTTPConnectionImpl> mImpl;
        bool mDetached;
        
        static ObjectPool<HTTPConnectionImpl> *msConnectionPool;
    };
};
