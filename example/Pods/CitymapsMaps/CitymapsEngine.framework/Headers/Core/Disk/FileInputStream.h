//
//  FileInputStream.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/3/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <string>
#include <fstream>
#include <CitymapsEngine/Core/EngineTypes.h>

namespace citymaps
{
    class FileInputStream
    {
        
    public:
        
        FileInputStream(const std::string& filepath);
        
        ~FileInputStream();
        
        bool Open();
        
        void Close();
        
        size_t Read(void* buffer, size_t size);
        
        const std::string& GetFilepath() {return mFilepath;}
        
    private:
        
        std::ifstream mStream;
        std::string mFilepath;
    };
}