//
//  FileOutputStream.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/3/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <string>
#include <fstream>
#include <CitymapsEngine/Core/EngineTypes.h>

namespace citymaps
{
    class FileOutputStream
    {
        
    public:
        
        FileOutputStream(const std::string& filepath);
        
        FileOutputStream();
        
        bool Open();
        
        void Close();
        
        FileOutputStream& Write(const void* buffer, int size);
        
        const std::string& GetFilepath() {return mFilepath;}
        
        template <typename T>
        FileOutputStream& operator << (const T& output)
        {
            if (mStream.is_open())
            {
                mStream << output;
            }
            return *this;
        }
        
    private:
        
        std::ofstream mStream;
        std::string mFilepath;
    };
}