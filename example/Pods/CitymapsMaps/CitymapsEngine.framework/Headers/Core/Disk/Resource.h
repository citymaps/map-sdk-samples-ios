/*
 * Resource.h
 *
 *  Created on: Jul 15, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <string>

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
	class Resource
	{
    public:
        
        Resource(const std::string& file, bool loadNow = true);
        
        ~Resource();
        
        bool Load();
        
        const TByteArray& Data() const;
        
        const std::string& GetFilePath() const
        {
            return mFilePath;
        }
        
        const std::string& StringData();
        
    private:
        
        TByteArray mData;
        std::string mFilePath;
        std::string mStringData;
	};
}
