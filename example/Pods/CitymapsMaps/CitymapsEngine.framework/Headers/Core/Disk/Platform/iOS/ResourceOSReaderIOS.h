/*
 * ResourceOSReaderIOS.h
 *
 *  Created on: Jul 15, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <string>

#include <CitymapsEngine/CitymapsEngine.h>

#include <CitymapsEngine/Core/Disk/ResourceOSReader.h>

namespace citymaps
{
	template<>
	class ResourceOSReader<PlatformTypeIOS>
	{
    public:
        static std::string GetPathForResource(const std::string& file);
        static TByteArray LoadResource(const std::string& file);
        static TByteArray LoadImageResource(const std::string& file, Size& outSize, float& outScale);
        static std::string GetCacheDirectory();
	};

}
