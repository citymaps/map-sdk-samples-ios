#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
	class Camera
	{
	public:
        Camera(Matrix4 projectionMatrix, Size screenSize);
		virtual ~Camera() {}
        
        virtual void MoveBy(Vector3 delta);
        virtual void MoveTo(Vector3 position);
        
        virtual void RotateBy(Vector3 delta);
        virtual void RotateTo(Vector3 rotation);
        
        virtual void ScaleBy(real delta);
        virtual void ScaleTo(real scale);
        
        virtual void ShearBy(real delta);
        virtual void ShearTo(real shear);
        
        Point Project(Vector3 point);
        Vector3 Unproject(Point screenPos, real zPos);
        
        const real GetShear() const { return mShear; }
        
        virtual void Update();
        
        virtual void Resize(const Size& screenSize)
        {
            mScreenSize = screenSize;
            mViewChanged = true;
        }

        const Matrix4& GetProjectionMatrix() const { return mProjectionMatrix; }
        const Matrix4& GetViewMatrix() const { return mInverseViewMatrix; }
        const Matrix4& GetOriginViewMatrix() const { return mOriginViewMatrix; }
        
        const Vector3& GetViewPosition() const { return mViewPosition; }
        const Vector3& GetPosition() const { return mPosition; }
        const Vector3& GetRotation() const { return mRotation; }
        
    protected:
        
        const real GetScale() const { return mScale; }
        
        const Size& GetScreenSize() const { return mScreenSize; }
        const Matrix4& GetRawViewMatrix() const { return mViewMatrix; }
        void SetViewMatrix(const Matrix4 &viewMatrix)
        {
            mViewMatrix = viewMatrix;
            mInverseViewMatrix = glm::inverse(mViewMatrix);
        }
        
        void SetProjectionMatrix(Matrix4 m) { mProjectionMatrix = m; }

	private:
		Matrix4 mViewMatrix;
		Matrix4 mInverseViewMatrix;
		Matrix4 mProjectionMatrix;
		Matrix4 mViewProjectionMatrix;
		Matrix4 mInverseViewProjectionMatrix;
        Matrix4 mOriginViewMatrix;
		Vector3 mPosition;
        Vector3 mViewPosition;
		Vector3 mRotation;
		real mScale;
		real mShear;
		Bounds mBounds;
		Size mScreenSize;
        bool mViewChanged;
	};
};
