//
//  VertexBufferOpenGL.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <mutex>
#include <condition_variable>

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/VertexBuffer.h>

namespace citymaps
{
    template<>
    class VertexBuffer<RendererTypeOpenGL> : public BaseVertexBuffer
    {
    public:
        VertexBuffer(IGraphicsDevice* device);
        ~VertexBuffer();
        
        void Initialize(VertexBufferType type, VertexBufferUsage usage);
        
        void SetData(const void *data, const size_t size);
        void SubData(const void* data, const size_t size, const size_t offset);
        
        void Activate();
        void Activate(int offset);
        void Deactivate();

        void Invalidate()
        {
            Validatable::Invalidate();
            mBufferId = 0;
        }
        
        void SetReady(bool ready)
        {
            mIsReady = ready;
        }
        
        bool IsReady() const
        {
            return mIsReady;
        }
        
        VertexBufferType GetType() const
        {
            return mEngineType;
        }
        
    private:

        void SetDataFromOurData();

    private:
        
        VertexBufferType mEngineType;
        GLuint mBufferId;
        GLuint mType;
        GLuint mUsage;
        GLuint mDataSize;
        bool mIsReady;
        std::mutex mLock;
    };
    
    typedef VertexBuffer<RendererTypeOpenGL> VertexBufferOpenGL;
};
