//
//  GraphicsDeviceOpenGL.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#ifdef __ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/OpenGL/VertexBufferOpenGL.h>

namespace citymaps
{
    static const int OpenGLPrimitiveTypes[5] = {
        GL_TRIANGLES,
        GL_TRIANGLE_STRIP,
        GL_LINES,
        GL_LINE_STRIP,
        GL_POINTS
    };
    
    static const int OpenGLIndexTypes[5] = {
        0,
        GL_UNSIGNED_BYTE,
        GL_UNSIGNED_SHORT,
        3,
        GL_UNSIGNED_INT
    };
    
    template<>
    class GraphicsDevice<RendererTypeOpenGL> : public BaseGraphicsDevice
    {
    public:
        GraphicsDevice(GRAPHICS_DEVICE_DESC *desc);
        ~GraphicsDevice();
        
        bool Initialize(const Size&size);
        
        void SetBlendFunction(BlendFunction sourceFunc, BlendFunction destFunc);
        void EnableBlending();
        void DisableBlending();
        
        void DisableDepthTest();
        void EnableDepthTest();
        void DisableDepthWrite();
        void EnableDepthWrite();
        
        void EnableStencilTest();
        void DisableStencilTest();
        void SetStencilFunction(StencilTest test, int value, int mask);
        void SetStencilOperation(StencilOp failStencil, StencilOp failDepth, StencilOp pass);
        void SetStencilMask(int mask);
        
        void ClearColorBuffer();
        void ClearDepthBuffer();
        void ClearStencilBuffer();
        
        void Resize(const Size& size);

        void PreRender();
        void PostRender();
        
        void Draw(PrimitiveType type, int count, int start);
        void DrawIndexed(PrimitiveType type, IndexType indexType, int count, int start);
        
        IShader* CreateShader(const std::string &filename, ShaderType type);
        IShader* CreateShader(const TByteArray &data, ShaderType type);
        IShaderProgram* CreateShaderProgram();
        
        IVertexBuffer* CreateVertexBuffer(VertexBufferType type, VertexBufferUsage usage);

        ITexture2D* CreateTexture2D(const TEXTURE_DESC &desc);
        ITexture2D* CreateTexture2D(const Image *image, const TEXTURE_DESC& desc);
        ITexture2D* CreateTexture2D(const ImageResource& resource, const TEXTURE_DESC& desc);
        ITexture2D* CreateTexture2D(const byte *data, const Vector2i &size, const TEXTURE_DESC& desc);
        
    private:
        
        bool mBlendingEnabled;
        bool mDepthTestEnabled;
        bool mDepthWriteEnabled;
        bool mStencilTestEnabled;
    };
};
