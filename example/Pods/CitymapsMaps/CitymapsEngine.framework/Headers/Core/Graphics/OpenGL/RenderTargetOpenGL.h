//
//  RenderTargetOpenGL.h
//  vectormap2
//
//  Created by Eddie Kimmel on 7/31/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//
#pragma once

#include <CitymapsEngine/Core/Graphics/RenderTarget.h>

namespace citymaps
{
    template<>
    class RenderTarget<RendererTypeOpenGL> : public BaseRenderTarget
    {
    public:
        
        RenderTarget(int width, int height, ITexture2D* colorAttachment, DepthAttachment depthAttachment);
        
        void Activate();
        
        void Disable();
        
        ITexture2D* GetColorAttachment();
        
    private:
        
        void CheckStatus();
        
    private:
        
        unsigned int mFboID;
        ITexture2D* mColorAttachment;
        unsigned int mDepthAttachmentID;
        int mPreviousFBOBinding;
    };
}