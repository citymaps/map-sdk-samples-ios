//
//  VertexBuffer.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Types.h>
#include <CitymapsEngine/Core/Graphics/GraphicsObject.h>

namespace citymaps
{
	enum VertexBufferType
	{
		VertexBufferTypeVertexData,
		VertexBufferTypeIndexData,
        VertexBufferTypeNumTypes
	};

	enum VertexBufferUsage
	{
		VertexBufferUsageStatic,
		VertexBufferUsageDynamic,
		VertexBufferUsageStream,
        VertexBufferUsageNumUsages,
	};

    class IGraphicsDevice;
    
    class IVertexBuffer : public GraphicsObject
    {
    public:

        IVertexBuffer(IGraphicsDevice* device):GraphicsObject(device){}
        virtual void SetData(const void *data, const size_t size) = 0;
        virtual void SubData(const void* data, const size_t size, const size_t offset) = 0;
        virtual void Activate() = 0;
        virtual void Activate(int offset) = 0;
        
        virtual void SetReady(bool ready) = 0;
        virtual bool IsReady() const = 0;

        virtual VertexBufferType GetType() const = 0;
        
        virtual ~IVertexBuffer() {};
    };
    
    class BaseVertexBuffer : public IVertexBuffer
    {
    public:
        
        BaseVertexBuffer(IGraphicsDevice* device)
        :IVertexBuffer(device)
        {
            
        }
        
        virtual ~BaseVertexBuffer()
        {
            
        }

    protected:
        TByteArray& GetData() {return mData;}

    private:

        TByteArray mData;
    };
    
    template <typename Renderer>
    class VertexBuffer : public BaseVertexBuffer
    {
    };
};
