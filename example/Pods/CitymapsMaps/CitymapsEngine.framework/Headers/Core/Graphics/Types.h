//
//  Types.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    enum PrimitiveType
    {
        PrimitiveTypeTriangleList,
        PrimitiveTypeTriangleStrip,
        PrimitiveTypeLineList,
        PrimitiveTypeLineStrip,
        PrimitiveTypePointList
    };
    
    enum IndexType
    {
        IndexTypeUnsignedByte = 1,
        IndexTypeUnsignedShort = 2,
        IndexTypeUnsignedInt = 4
    };
    
    struct GRAPHICS_DEVICE_DESC
    {
    };
    
};
