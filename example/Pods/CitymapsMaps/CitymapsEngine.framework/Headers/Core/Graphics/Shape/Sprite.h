/*
 * Sprite.h
 *
 *  Created on: Jul 18, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <memory>

#include <CitymapsEngine/CitymapsEngine.h>

#include <CitymapsEngine/Core/Graphics/Shape.h>
#include <CitymapsEngine/Core/Util/Image.h>

namespace citymaps
{
	class Sprite : public Shape
	{
    public:
        
        Sprite(const std::string& effect, std::shared_ptr<Image> image, const TEXTURE_DESC& desc = TEXTURE_DESC());
        
        // This will create a copy of the image, not take ownership.
        Sprite(const std::string& effect, Image* image, const TEXTURE_DESC& desc = TEXTURE_DESC());
        
        ~Sprite();
        
        void SetImage(std::shared_ptr<Image> image)
        {
            if (mImage != image)
            {
                mImage = image;
                mImageChanged = true;
            }
        }
        
        std::shared_ptr<Image> GetImage()
        {
            return mImage;
        }
        
        void SetAlpha(float alpha)
        {
            mAlpha = alpha;
        }
        
        void SetPosition(real x, real y)
        {
            mDimensions.x = x;
            mDimensions.y = y;
        }
        
        void SetPosition(const Point& p)
        {
            mDimensions.x = p.x;
            mDimensions.y = p.y;
        }
        
        void SetSize(real width, real height)
        {
            mDimensions.z = width;
            mDimensions.w = height;
        }
        
        void SetSize(const Size& size)
        {
            mDimensions.z = size.width;
            mDimensions.w = size.height;
        }
        
        Size GetSize() const
        {
            return Size(mDimensions.z, mDimensions.w);
        }
        
        void SetWidth(real width)
        {
            mDimensions.z = width;
        }
        
        void SetHeight(real height)
        {
            mDimensions.w = height;
        }
        
        void SetTextureDimensions(float minX, float minY, float texCoordWidth, float texCoordHeight)
        {
            mTextureDimensions.x = minX;
            mTextureDimensions.y = minY;
            mTextureDimensions.z = texCoordWidth;
            mTextureDimensions.w = texCoordHeight;
        }
        
        void SetAnchorPoint(float x, float y)
        {
            mAnchorPoint.x = x;
            mAnchorPoint.y = y;
        }
        
        void SetAnchorPoint(const Point& p)
        {
            mAnchorPoint.x = p.x;
            mAnchorPoint.y = p.y;
        }
        
        void SetBlendColor(float r, float g, float b, float a)
        {
            mBlendColor.r = r;
            mBlendColor.g = g;
            mBlendColor.b = b;
            mBlendColor.a = a;
        }
        
        void SetBlendColor(const Vector4f& color)
        {
            mBlendColor = color;
        }
        
        void SetRotation(float rotation)
        {
            if (rotation != mRotation)
            {
                mRotation = rotation;
                
                mRotationValues[0] = sin(glm::radians(rotation));
                mRotationValues[1] = cos(glm::radians(rotation));
            }
        }
        
        void SetScale(float scale)
        {
            mScale = scale;
        }
        
        ITexture2D* GetTexture()
        {
            return mTexture;
        }
        
        float GetAlpha() const
        {
            return mAlpha;
        }
        
        float GetX() const
        {
            return mDimensions.x;
        }
        
        float GetY() const
        {
            return mDimensions.y;
        }
        
        float GetWidth() const
        {
            return mDimensions.z;
        }
        
        float GetHeight() const
        {
            return mDimensions.w;
        }
        
        Vector4f GetTextureDimensions() const
        {
            return mTextureDimensions;
        }
        
        Vector2f GetAnchorPoint() const 
        {
            return mAnchorPoint;
        }
        
        Vector4f GetBlendColor() const
        {
            return mBlendColor;
        }
        
        float GetScale() const
        {
            return mScale;
        }
        
        bool IsReady() { return true; }
        
        void Draw(IGraphicsDevice *device, RenderState &state, PrimitiveType primitiveType = PrimitiveTypeTriangleList);
        
    private:
        
        enum GlobalDataUsage
        {
        	MVP, Alpha, Texture, Dimensions, TextureDimensions, AnchorPoint, BlendColor, Rotation, NumGlobalData
        };
        
        void LoadGlobalDataIndices(IGraphicsDevice* device);
        
    private:

        /**
         * This are controlled by the GraphicsContext. We are not responsible for cleaning.
         */
        IVertexBuffer* mSharedBuffer;
        
        
        ITexture2D* mTexture;
        std::shared_ptr<Image> mImage;
        bool mImageChanged;
        TEXTURE_DESC mTextureDesc;
        
        Vector4f mDimensions;
        Vector4f mTextureDimensions;
        Vector4f mBlendColor;
        Vector2f mRotationValues;
        
        float mAlpha;
        float mRotation;
        float mScale;
        int mTexUnit;
        bool mGlobalIndicesLoaded;
        
        Vector2f mAnchorPoint;
        
        int mGlobalDataIndices[NumGlobalData];

        
	};
}
