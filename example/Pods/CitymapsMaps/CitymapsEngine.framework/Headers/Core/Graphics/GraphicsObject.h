//
//  GraphicsObject.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/20/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/EngineTypes.h>
#include <atomic>

namespace citymaps
{
    class IGraphicsDevice;
    class GraphicsObject : public Validatable
    {
        friend class GraphicsContext;
        friend class BaseGraphicsDevice;
        
    public:
        
        GraphicsObject(IGraphicsDevice* device):mGraphicsDevice(device), mRetainCount(1), mShouldBackup(true){}
        
        virtual void PostGraphicsJob(std::function<void()> job);
        
        virtual void Activate() {};
        virtual void Deactivate() {};
        void Release();
        
    protected:
        
        virtual ~GraphicsObject() = 0;
        
        IGraphicsDevice* GetGraphicsDevice()
        {
            return mGraphicsDevice;
        }
        
        bool ShouldBackup() const { return mShouldBackup; }
        
    private:
        
        void Retain()
        {
            mRetainCount++;
        }
        
        void SetShouldBackup(bool backup)
        {
            mShouldBackup = backup;
        }
        
    private:
        
        std::atomic<int> mRetainCount;
        bool mShouldBackup;
        IGraphicsDevice* mGraphicsDevice;
    };
}
