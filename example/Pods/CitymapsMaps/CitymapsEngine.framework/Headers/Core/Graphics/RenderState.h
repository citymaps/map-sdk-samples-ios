//
//  RenderState.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/5/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <citymapsengine/Core/Graphics/Material.h>

namespace citymaps
{
	class RenderState
	{
			typedef std::vector<const ShaderData*> ShaderDataVector;

		public:
			RenderState()
			:mShaderDataVector(50)
			{
			}

			~RenderState()
			{
			}

			void SetProjection(const Matrix4 &projection)
			{
				if (mProjection != projection)
				{
					mProjection = projection;
					mWVP = static_cast<Matrix4f>(mProjection * mTopTransform);
                    mMVPShaderData.SetData(mWVP);
				}
			}

			Matrix4& GetProjection()
			{
				return mProjection;
			}

			void PushTransform(const Matrix4 &transform)
			{
				mTransformStack.push(mTopTransform * transform);
				mTopTransform = mTransformStack.top();
				mWVP = static_cast<Matrix4f>(mProjection * mTopTransform);
                
                mMVPShaderData.SetData(mWVP);

			}
			void PopTransform()
			{
				if (mTransformStack.size() > 0)
				{
					mTransformStack.pop();

					if (mTransformStack.size() > 0)
					{
						mTopTransform = mTransformStack.top();
						mWVP = static_cast<Matrix4f>(mProjection * mTopTransform);
                        mMVPShaderData.SetData(mWVP);
					} else
					{
						mTopTransform = glm::mat4();
						mWVP = static_cast<Matrix4f>(mProjection);
                        mMVPShaderData.SetData(mWVP);
					}
				}
			}

			const Matrix4& GetTransform()
			{
				return mTopTransform;
			}

			const Matrix4f& GetWorldViewProjection()
			{
				return mWVP;
			}
        
            const ShaderData& GetMVPShaderData()
            {
                return mMVPShaderData;
            }

			void SetShaderData(int index, const ShaderData* data)
			{
				if (index < 0)
					return;

				if (mShaderDataVector.size() < index)
				{
					mShaderDataVector.resize(index + 50);
				}

				mShaderDataVector[index] = data;
			}

			const ShaderData* GetShaderData(int index)
			{
				if (index < 0)
					return NULL;

				if (mShaderDataVector.size() < index)
					return NULL;

				return mShaderDataVector[index];
			}

		private:
			std::stack<Matrix4> mTransformStack;
			Matrix4 mProjection;
			Matrix4 mTopTransform;
			Matrix4f mWVP;
			ShaderDataVector mShaderDataVector;
            ShaderDataMatrix4f mMVPShaderData;
	};
}
