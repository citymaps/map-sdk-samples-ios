//
//  InputLayout.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/5/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
	struct InputElement
	{
		std::string name;
		int numComponents;
		int offset;
	};
    
	typedef std::vector<InputElement> TInputElementList;
    
	class InputLayout
	{
	public:
		InputLayout() :
            mStride(0)
        {
        }
        
		~InputLayout()
        {
        }
        
		void AddElement(std::string name, int numComponents, int offset)
        {
            InputElement element = { name, numComponents, offset };
            mElements.push_back(element);
            
            mStride += numComponents * sizeof(float);
        }
        
		const TInputElementList& GetElements() { return mElements; }
        const int GetStride() { return mStride; }
        
	private:
		TInputElementList mElements;
        int mStride;
	};
};
