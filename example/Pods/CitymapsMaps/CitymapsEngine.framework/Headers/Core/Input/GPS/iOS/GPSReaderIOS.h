//
//  GPSReaderIOS.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/GPS/GPSReader.h>
#import <CitymapsEngine/Core/Input/GPS/iOS/CELocationManagerDelegate.h>

namespace citymaps
{
    class GPSReaderIOS : public GPSReader
    {
    public:
        GPSReaderIOS();
        ~GPSReaderIOS();
        
        void StartGPSUpdates();
        void StopGPSUpdates();
        void StartDirectionUpdates();
        void StopDirectionUpdates();
        
        bool CanReceiveGPSUpdates();
        
    private:
        CELocationManagerDelegate *mLocationDelegate;
    };
};
