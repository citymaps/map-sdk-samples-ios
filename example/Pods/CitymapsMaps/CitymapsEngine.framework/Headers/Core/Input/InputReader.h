//
//  InputReader.h
//  ;
//
//  Created by Adam Eskreis on 6/11/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/TouchListener.h>
#include <CitymapsEngine/Core/Util/SafeObject.h>

namespace citymaps
{
    class InputReader
    {
    public:
        InputReader();
        ~InputReader();
        
        void AddTouchListener(std::shared_ptr<ITouchListener> listener);
        void RemoveTouchListener(std::shared_ptr<ITouchListener> listener);
        
        void ProcessTouchEvent(TouchEventType type, std::vector<Point> points);
        
        void Update();
        
    private:

        void BuildSafeListenerCopy();

    private:
        SafeObject<std::vector<TouchEvent> > mTouchEventQueue;
        
        SafeObject< std::vector<std::weak_ptr<ITouchListener> > > mTouchListeners;
        std::vector<std::weak_ptr<ITouchListener> > mSafeTouchListeners;
    };
};
