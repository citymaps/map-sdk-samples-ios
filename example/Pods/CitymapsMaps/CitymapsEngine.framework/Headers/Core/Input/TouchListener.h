//
//  GestureListener.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/11/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/Util.h>

namespace citymaps
{
    enum TouchEventType
    {
        TouchEventStart,
        TouchEventMove,
        TouchEventEnd,
        TouchEventCancel
    };
    
    struct TouchEvent
    {
        TouchEventType type;
        std::vector<Point> touchPoints;
        double time;
        
        TouchEvent() :
        type(TouchEventStart), touchPoints(), time(Util::GetSystemTime())
        {}
        
        TouchEvent(TouchEventType _type, std::vector<Point> _points) :
            type(_type),
            touchPoints(_points),
            time(Util::GetSystemTime())
        {}
    };
    
    class ITouchListener
    {
    public:
        virtual void OnTouchEvent(const TouchEvent &e) = 0;
    };
};
