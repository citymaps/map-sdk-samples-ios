//
//  ZoomRotateGesture.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/11/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/Gesture/Gesture.h>

namespace citymaps
{
    class ZoomRotateGesture : public Gesture
    {
    public:
        ZoomRotateGesture(const std::vector<Point> &points);
        ~ZoomRotateGesture();
        
        void Update(const std::vector<Point> &points);
        
        bool ShouldZoom();
        bool ShouldRotate();
        
        float GetScaleFactor() const { return mScaleFactor; }
        float GetAngularDelta() const { return mAngularDelta; }
        const Point& GetMidPoint() const { return mMidPoint; }
        
    private:
        
        /* Zoom Properties */
        Point mMidPoint;
        float mCurrentDist;
        float mScaleFactor;
        float mTotalScaleFactor;
        bool mZoomStarted;
        bool mRotateStarted;
        
        /* Rotate Properties */
        float mAngle;
        float mAngularDelta;
        float mTotalAngularDelta;
        float mUnusedAngularDelta;
    };
}
