//
//  Gesture.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/11/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/TouchListener.h>

namespace citymaps
{
    enum GestureType
    {
        GestureTypePan,
        GestureTypeZoomRotate,
        GestureTypeTilt,
        GestureTypeSingleTouchZoom
    };
    
    class Gesture
    {
    public:
        Gesture(GestureType type, const std::vector<Point> &points);
        virtual ~Gesture();
        
        virtual void Update(const std::vector<Point> &points);
        
        const std::vector<Point>& GetTouchPoints() { return mPoints; }
        const std::vector<Point>& GetInitialTouchPoints() { return mInitialPoints; }
        const size_t GetNumTouches() { return mPoints.size(); }
        const GestureType GetType() { return mType; }
        bool IsType(GestureType type) { return mType == type; }
        
    private:
        GestureType mType;
        std::vector<Point> mPoints;
        std::vector<Point> mInitialPoints;
    };
};
