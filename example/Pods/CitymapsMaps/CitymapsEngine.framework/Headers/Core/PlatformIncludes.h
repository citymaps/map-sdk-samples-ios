//
//  PlatformIncludes.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/29/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#ifdef __IOS__
    // OpenGL
    #include <OpenGLES/ES2/gl.h>
    #include <OpenGLES/ES2/glext.h>

    // Engine includes
    #include <CitymapsEngine/Core/Platform/iOS/ExceptionIOS.h>
    #include <CitymapsEngine/Core/Disk/Platform/iOS/ResourceOSReaderIOS.h>
    #include <CitymapsEngine/Core/Util/Platform/iOS/TimerIOS.h>
    #include <CitymapsEngine/Core/Font/Platform/iOS/SystemFontReaderIOS.h>
    #include <CitymapsEngine/Core/Util/Platform/iOS/AppDataIOS.h>
#endif

#ifdef __ANDROID__
    // OpenGL
    #include <GLES2/gl2.h>
    #include <GLES2/gl2ext.h>

    // Engine Includes
    #include <CitymapsEngine/Core/Platform/Android/ExceptionAndroid.h>
    #include <CitymapsEngine/Core/Disk/Platform/Android/ResourceOSReaderAndroid.h>
    #include <CitymapsEngine/Core/Util/Platform/Android/TimerAndroid.h>
    #include <CitymapsEngine/Core/Font/Platform/Android/SystemFontReaderAndroid.h>
    #include <CitymapsEngine/Core/Util/Platform/Android/AppDataAndroid.h>
#endif