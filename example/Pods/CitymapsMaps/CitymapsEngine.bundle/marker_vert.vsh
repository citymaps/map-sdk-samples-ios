attribute vec2 a_position;
attribute vec2 a_texCoord;

varying vec2 v_texCoord;

uniform highp mat4 u_mvp;
uniform highp vec2 u_markerSize;

void main()
{
    v_texCoord = a_texCoord;
    gl_Position = u_mvp * vec4(a_position.x * u_markerSize.x, a_position.y * u_markerSize.y, 0.0, 1.0);
}
