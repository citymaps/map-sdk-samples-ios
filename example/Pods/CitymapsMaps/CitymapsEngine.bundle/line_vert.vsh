attribute vec2 VertexPosition;
attribute vec2 VertexTexCoord;
attribute vec2 VertexPerp;
attribute float VertexType;
attribute float LayerId;

varying highp vec4 Color;
varying highp vec2 TexCoord;

uniform highp mat4 modelViewProjectionMatrix;
uniform lowp int drawMode;
uniform vec4 colors[100];
uniform float widths[100];
uniform float deltas[100];

void main()
{
    int LayerIdInt = int(LayerId);
    Color = colors[LayerIdInt];
    TexCoord = VertexTexCoord;
    float LineWidth = widths[LayerIdInt];
    float delta = deltas[LayerIdInt];

    vec2 v2Pos = vec2(VertexPosition.x, VertexPosition.y);
    if(VertexType == 0.0) {
        v2Pos = v2Pos + (VertexPerp * LineWidth);
    } else if(VertexType == 1.0) {
        v2Pos = v2Pos - (VertexPerp * LineWidth);
    }
    gl_Position = modelViewProjectionMatrix * vec4(v2Pos.x, v2Pos.y, delta, 1.0); 
}