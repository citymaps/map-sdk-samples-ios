precision highp float;

varying vec2 v_texCoord0;
varying vec2 v_lineSize;
varying vec4 v_color;

uniform vec2 u_segDim;
uniform float u_passCount;

float calcDistanceSqr(vec2 v, vec2 w, vec2 p)
{
    float dist = 0.0;
    
    vec2 seg = w - v;
    float lengthSquared = dot(seg,seg);
    
    float t = dot(p-v,seg)/lengthSquared;
    vec2 pv = p-v;
    vec2 pw = p-w;
    float pvDist = dot(pv,pv);
    float pwDist = dot(pw,pw);
    vec2 projection = v + t * (seg);
    vec2 pp = p - projection;
    float pvwDist = dot(pp,pp);
    
    float pvMask = step(t,0.0); //  if(t < 0.0) return pvDist;
    float pwMask = step(-t,-1.0); //  else if(t > 1.0) return pwDist
    float pvwMask = step(-t,0.0) * step(t,1.0); //  if t > 0 and t <= 1.0 else return pvwDist
    
    dist += mix(0.0,pvDist,pvMask);
    dist += mix(0.0,pwDist,pwMask);
    dist += mix(0.0,pvwDist,pvwMask);
    
    return dist;
}




void calcColor(inout vec4 currColor, vec2 testCoord, vec4 lineColor) {
    vec4 white = vec4(1.0, 1.0, 1.0, 1.0);
    vec4 red = vec4(1.0, 0.0, 0.0, 1.0);
    float distSqr = 0.0;
    
    //////////////////////////////////////////////////////
    //THESE TWO CONSTS SHOULD BE MOVED TO UNIFORMS
    float borderWidth = 0.5;
    //float borderWidth = 3.0;
    float transitionWidth = 0.66;
    //float transitionWidth = 1.6;
    //////////////////////////////////////////////////////
    
    vec2 halfDim = vec2(v_lineSize.x*0.5, v_lineSize.y*0.5);
    float radius = halfDim.x - (borderWidth*u_passCount);
    
    float borderStart = radius - borderWidth;
   
    //Setup line segment endpoints
     vec2 v = vec2(halfDim.x, halfDim.x);
     //vec2 w = vec2(halfDim.x, u_segDim.y - radius);
     vec2 w = vec2(halfDim.x, v_lineSize.y - halfDim.x);
     //Test point
     vec2 p = vec2(testCoord.x, testCoord.y);

     distSqr = calcDistanceSqr(v,w,p); 
    
    //smoothly switch color based upon distance
    //float colorInterpT = smoothstep(borderStart - transitionWidth,borderStart,dist);
    //vec4 color = mix(white, red,colorInterpT);
    //vec4 color = mix(v_color, white, u_passCount);
    vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
    
    //modulate alpha based upondistance
    float diffTransition = radius - transitionWidth;
    float alpha = 1.0 - smoothstep(diffTransition*diffTransition, radius*radius, distSqr);

    color = vec4(v_color.x,v_color.y,v_color.z,alpha);
    
    currColor = color;
}

void main()
{
    vec4 origColor = vec4(0.0, 0.0, 0.0, 0.0);//gl_Color;
    vec4 currColor = origColor;

	//The local coords are the uv's, use u_segDim to translate uv's to worldUnits
	vec2 barycentricCoord = vec2(v_texCoord0.x,v_texCoord0.y);
	vec2 testCoord = vec2(barycentricCoord.x*v_lineSize.x,barycentricCoord.y*v_lineSize.y);

	calcColor(currColor, testCoord, v_color);

	//gl_FragColor = currColor;
    gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
}
