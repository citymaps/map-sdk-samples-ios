precision mediump float;

varying vec2 v_texCoord;

uniform sampler2D u_texture;

void main()
{
    vec2 uvTexCoord = (vec2(1.0, 1.0) + v_texCoord) / 2.0;
    vec4 color = texture2D(u_texture, uvTexCoord);
    gl_FragColor = color;
}
