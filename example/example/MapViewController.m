//
//  MapViewController.m
//  example
//
//  Created by Eddie Kimmel on 4/7/15.
//  Copyright (c) 2015 Citymaps. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (instancetype)initWithMapView:(CECitymapsMapView *)mapView {
    self = [super init];
    if (self) {
        _mapView = mapView;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _mapView.frame = self.view.bounds;
    [self.view addSubview:_mapView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [_mapView removeFromSuperview];
    _mapView.delegate = nil;
    _mapView.businessDelegate = nil;
}

@end
