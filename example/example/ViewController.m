//
//  ViewController.m
//  Samples
//
//  Created by Adam Eskreis on 12/13/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import "ViewController.h"
#import "BasicMapViewController.h"
#import "BusinessMapViewController.h"
#import "BusinessFilterViewController.h"
#import "AnimScriptViewController.h"
#import "CanvasViewController.h"
#import "MarkerSampleViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *demos;
@property (strong, nonatomic) UITableView *demoList;
@property (strong, nonatomic) CECitymapsMapView *mapView;
@end

@implementation ViewController

- (id)init
{
    self = [super init];
    if (self) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    _demos = @[@"Basic Map",
               @"Business Map Demo",
               @"Business Filter Demo",
               @"Animation Script Demo",
               @"Canvas Demo",
               @"Marker Demo"];
    
    _demoList = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    _demoList.delegate = self;
    _demoList.dataSource = self;
    
    /* A basic Citymaps map. */
    CECitymapsMapViewOptions *options = [[CECitymapsMapViewOptions alloc] init];
    [options setPosition:CEMapPositionMake(CELonLatMake(-73.9857, 40.7577), 15, 0, 45)];
    _mapView = [[CECitymapsMapView alloc] initWithFrame:self.view.bounds options:options];
    
    [self.view addSubview:_demoList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_demoList cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    NSString *demo = _demos[indexPath.row];
    
    UIViewController *demoController = nil;
    
    if ([demo isEqualToString:@"Basic Map"]) {
        demoController = [[BasicMapViewController alloc] initWithMapView:_mapView];
    } else if ([demo isEqualToString:@"Business Map Demo"]) {
        demoController = [[BusinessMapViewController alloc] initWithMapView:_mapView];
    } else if ([demo isEqualToString:@"Business Filter Demo"]) {
        demoController = [[BusinessFilterViewController alloc] initWithMapView:_mapView];
    } else if ([demo isEqualToString:@"Animation Script Demo"]) {
        demoController = [[AnimScriptViewController alloc] initWithMapView:_mapView];
    } else if ([demo isEqualToString:@"Canvas Demo"]) {
        demoController = [[CanvasViewController alloc] initWithMapView:_mapView];
    } else if ([demo isEqualToString:@"Marker Demo"]) {
        demoController = [[MarkerSampleViewController alloc] initWithMapView:_mapView];
    }
    
    if (demoController != nil)
    {
        [self.navigationController pushViewController:demoController animated:YES];
    }
        
}
- (IBAction)action2:(id)sender {
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"demo_cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"demo_cell"];
    }
    
    UILabel *label = (UILabel *)[cell viewWithTag:0];
    if (!label) {
        label = [[UILabel alloc] initWithFrame:cell.frame];
    }
    
    label.text = _demos[indexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _demos.count;
}

@end
