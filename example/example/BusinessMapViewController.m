//
//  BusinessMapViewController.m
//  Samples
//
//  Created by Adam Eskreis on 12/17/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import "BusinessMapViewController.h"
#import <CitymapsEngine/Citymaps.h>

@interface BusinessMapViewController () <CEMapViewDelegate, CEBusinessDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) CEBusinessData *selectedBusiness;

@end

@implementation BusinessMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.mapView.delegate = self;
    self.mapView.businessDelegate = self;
    self.mapView.businessLayer.visible = YES;
    [self.mapView setMapCenter:CELonLatMake(-73.9857, 40.7577) andZoom:13];
}

- (void)dealloc
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CEBusinessDelegate

- (void)businessTapped:(CEBusinessData *)data
{
    _selectedBusiness = data;
    
    NSString *address = [NSString stringWithFormat:@"%@\n%@, %@", data.address, data.city, data.state];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:data.name message:address delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [self.view addSubview:alertView];
    [alertView show];
}

- (void)businessDoubleTapped:(CEBusinessData *)data
{
    _selectedBusiness = data;
    
    [self.mapView setMapCenter:data.location andZoom:17 withDuration:0.3];
}

- (void)businessLongPressed:(CEBusinessData *)data
{
    _selectedBusiness = data;
    
    NSString *title = [NSString stringWithFormat:@"Call %@?", data.phone];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    [alertView show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        NSString *phoneURL = [NSString stringWithFormat:@"tel:%@", _selectedBusiness.phone];
        NSURL *url = [NSURL URLWithString:phoneURL];
        
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
