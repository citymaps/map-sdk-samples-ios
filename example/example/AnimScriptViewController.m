//
//  AnimScriptViewController.m
//  Samples
//
//  Created by Adam Eskreis on 12/18/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import "AnimScriptViewController.h"

@interface AnimScriptViewController () <CEMapViewDelegate>

@property (strong, nonatomic) CECitymapsMapView *mapView;

@end

@implementation AnimScriptViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.mapView setMapCenter:CELonLatMake(-73.9857, 40.7577) andZoom:13];
    
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame = CGRectMake(0, 400, 160, 50);
    [playButton setTitle:@"Play Animations" forState:UIControlStateNormal];
    playButton.backgroundColor = [UIColor grayColor];
    [playButton addTarget:self action:@selector(playScript:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)playScript:(id)sender
{
    CEAnimationScript *script = [[CEAnimationScript alloc] init];
    [script moveTo:CELonLatMake(-75, 41) andZoom:12];
    [script rotateTo:45];
    [script moveTo:CELonLatMake(-76, 45) andZoom:7];
    [script rotateTo:75];
    [script moveTo:CELonLatMake(-73.9857, 40.7577)];
    [script zoomTo:13 delay:0.5];
    [script rotateTo:0];
    
    [script play:self.mapView];
}

@end
