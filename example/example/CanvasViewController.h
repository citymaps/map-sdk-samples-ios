//
//  CanvasViewController.h
//  Samples
//
//  Created by Eddie Kimmel on 12/30/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"

@interface CanvasViewController : MapViewController

@end
