//
//  MarkerSampleViewController.m
//  Samples
//
//  Created by Adam Eskreis on 12/18/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import "MarkerSampleViewController.h"
#import <CitymapsEngine/Citymaps.h>

@interface MarkerSampleViewController () <CEMapViewDelegate>

@property (nonatomic, strong) CEMarker *touchMarker;
@property (nonatomic, strong) CEMarker *draggableMarker;
@property (nonatomic, strong) UILabel *tappedLabel;
@property (nonatomic, strong) UILabel *draggedLabel;

@end

@implementation MarkerSampleViewController

#define kMarkerVCMarkerGroup @"MarkerVCMarkerGroup"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mapView.delegate = self;
    self.mapView.businessLayer.visible = NO;
    
    [self.mapView setMapCenter:CELonLatMake(-73.988, 40.75) andZoom:13];
	// Do any additional setup after loading the view.
    
    _tappedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 30)];
    _tappedLabel.backgroundColor = [UIColor whiteColor];
    _tappedLabel.textColor = [UIColor blackColor];
    _tappedLabel.font = [UIFont systemFontOfSize:12];
    _tappedLabel.text = @"Tap, double tap, or long press the left marker.";
    [self.view addSubview:_tappedLabel];
    
    _draggedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, self.view.frame.size.width, 30)];
    _draggedLabel.backgroundColor = [UIColor whiteColor];
    _draggedLabel.textColor = [UIColor blackColor];
    _draggedLabel.font = [UIFont systemFontOfSize:12];
    _draggedLabel.text = @"Long press the right marker to being dragging.";
    [self.view addSubview:_draggedLabel];
    
    [self mapDidLoad:self.mapView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CEMapViewDelegate

- (void)mapDidLoad:(CEMapView *)map
{
    CEMarkerGroup *markerGroup = [map markerGroupWithName:kMarkerVCMarkerGroup];
    
    _touchMarker = [[CEMarker alloc] initWithImageNamed:@"sample_marker.png"];
    _touchMarker.position = CELonLatMake(-74, 40.75);
    _touchMarker.anchorPoint = CGPointMake(0.5, 0.5);
    _touchMarker.highlightColor = [UIColor grayColor];
    _touchMarker.delegate = self;
    
    [markerGroup addMarker:_touchMarker];
    
    _draggableMarker = [[CEMarker alloc] initWithImageNamed:@"sample_marker.png"];
    _draggableMarker.position = CELonLatMake(-73.975, 40.75);
    _draggableMarker.draggable = YES;
    _draggableMarker.anchorPoint = CGPointMake(0.5, 0.5);
    _draggableMarker.highlightColor = [UIColor grayColor];
    _draggableMarker.delegate = self;
    
    [markerGroup addMarker:_draggableMarker];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.mapView.businessLayer.visible = YES;
    CEMarkerGroup *markerGroup = [self.mapView markerGroupWithName:kMarkerVCMarkerGroup];
    [markerGroup removeAllMarkers];
}

- (BOOL)markerTouchUp:(CEMarker *)marker
{
    if (marker == _draggableMarker)
        _draggableMarker.dragging = NO;
    
    return YES;
}

- (BOOL)markerTouchDown:(CEMarker *)marker
{
    return YES;
}

- (BOOL)markerTapped:(CEMarker *)marker
{
    if (marker == _touchMarker)
        _tappedLabel.text = @"Marker tapped";
    
    return YES;
}

- (BOOL)markerDoubleTapped:(CEMarker *) marker
{
    if (marker == _touchMarker)
        _tappedLabel.text = @"Marker double tapped";
    
    return YES;
}

- (BOOL)markerLongPressed:(CEMarker *) marker
{
    if (marker == _draggableMarker)
    {
        _draggableMarker.dragging = YES;
    }
    
    if (marker == _touchMarker)
        _tappedLabel.text = @"Marker long pressed";
    
    return YES;
}

- (void)markerDidBeginDragging:(CEMarker *)marker
{
    _draggedLabel.text = @"Drag began";
}

- (void)markerDragged:(CEMarker *)marker
{
    CELonLat position = marker.position;
    _draggedLabel.text = [NSString stringWithFormat:@"Dragged: %f %f", position.lon, position.lat];
}

- (void)markerWillEndDragging:(CEMarker *)marker
{
    _draggedLabel.text = @"Drag end";
}


@end
