//
//  CanvasViewController.m
//  Samples
//
//  Created by Eddie Kimmel on 12/30/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import "CanvasViewController.h"


#import <CitymapsEngine/Citymaps.h>

@interface CanvasViewController () <CEMapViewDelegate>

@property (nonatomic, strong) CEMapView *mapView;
@property (nonatomic, strong) CECircle *circle;
@property (nonatomic, strong) CERectangle *rectangle;
@property (nonatomic, strong) CELine *line;
@property (nonatomic, strong) CEPolygon *polygon;
@property (nonatomic, strong) CEOverlay *overlay;

@end

@implementation CanvasViewController

#define kCanvasVCFeatureGroup @"CanvasVCFeatureGroup"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mapView.delegate = self;
    [self.mapView setMapCenter:CELonLatMake(-73.988, 41.00) andZoom:10];
    
    // Additional setup specific for this sample.
    UILabel *alphaLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 30)];
    alphaLabel.textAlignment = NSTextAlignmentLeft;
    alphaLabel.text = @"Move the slider to adjust alpha";
    alphaLabel.textColor = [UIColor blackColor];
    alphaLabel.backgroundColor = [UIColor whiteColor];
    alphaLabel.enabled = YES;
    [self.view addSubview:alphaLabel];
    
    UISlider *alphaSlider = [[UISlider alloc] initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 100)];
    alphaSlider.backgroundColor = [UIColor clearColor];
    alphaSlider.continuous = YES;
    alphaSlider.minimumValue = 0;
    alphaSlider.maximumValue = 100;
    alphaSlider.value = 100.f;
    alphaSlider.enabled = YES;
    
    [alphaSlider addTarget:self action:@selector(alphaValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:alphaSlider];
    
    [self mapDidLoad:self.mapView];
}

- (IBAction)alphaValueChanged:(UISlider *)sender
{
    float alpha = sender.value / 100.f;
    
    const CGFloat *circleColor = CGColorGetComponents(_circle.fillColor.CGColor);
    _circle.fillColor = [UIColor colorWithRed:circleColor[0] green:circleColor[1] blue:circleColor[2] alpha:alpha];
    
    const CGFloat *rectColor = CGColorGetComponents(_rectangle.fillColor.CGColor);
    _rectangle.fillColor = [UIColor colorWithRed:rectColor[0] green:rectColor[1] blue:rectColor[2] alpha:alpha];
    
    const CGFloat *lineColor = CGColorGetComponents(_line.fillColor.CGColor);
    _line.fillColor = [UIColor colorWithRed:lineColor[0] green:lineColor[1] blue:lineColor[2] alpha:alpha];
    
    const CGFloat *polyColor = CGColorGetComponents(_polygon.fillColor.CGColor);
    _polygon.fillColor = [UIColor colorWithRed:polyColor[0] green:polyColor[1] blue:polyColor[2] alpha:alpha];
    
    _overlay.alpha = alpha;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CEMapViewDelegate

// Called when the map has loaded and is ready for use.
- (void)mapDidLoad:(CEMapView *)map
{
    CEFeatureGroup *group = [map featureGroupWithName:kCanvasVCFeatureGroup];
    
    // Creating a circle and settings its properties.
    _circle = [[CECircle alloc] initWithPosition:CELonLatMake(-74.1, 41.05) andRadius:3000];
    _circle.fillColor = [UIColor redColor];
    _circle.strokeColor = [UIColor blueColor];
    _circle.strokeWidth = 5;
    [group addFeature:_circle];
    
    // Creating a rectangle and settings its properties.
    _rectangle = [[CERectangle alloc] initWithPosition:CELonLatMake(-73.9, 41.05) andSize:CGSizeMake(500, 2500)];
    _rectangle.fillColor = [UIColor yellowColor];
    _rectangle.strokeColor = [UIColor blackColor];
    _rectangle.strokeWidth = 6;
    [group addFeature:_rectangle];

    // Creating a line and settings its properties.
    CELonLat* linePoints = (CELonLat[]){CELonLatMake(-74, 41), CELonLatMake(-74, 40), CELonLatMake(74, 40), CELonLatMake(74, 41), CELonLatMake(175, 41), CELonLatMake(175, 42), CELonLatMake(-175, 42), CELonLatMake(-74, 41)};
    _line = [[CELine alloc] initWithPoints:linePoints count:8];
    _line.fillColor = [UIColor whiteColor];
    _line.strokeColor = [UIColor blackColor];
    _line.width = 11;
    _line.strokeWidth = 4;
    [group addFeature:_line];

    // Creating a polygon and settings its properties.
    _polygon = [[CEPolygon alloc] init];
    _polygon.fillColor = [UIColor grayColor];
    _polygon.strokeColor = [UIColor blackColor];
    _polygon.strokeWidth = 5;
    [_polygon addPoint:CELonLatMake(-74.2, 40.95)];
    [_polygon addPoint:CELonLatMake(-74.2, 40.75)];
    [_polygon addPoint:CELonLatMake(-74.4, 40.75)];
    [_polygon addPoint:CELonLatMake(-74.2, 40.55)];
    [_polygon addPoint:CELonLatMake(-74.0, 40.95)];
    [group addFeature:_polygon];
    
    // Adding a hole to the polygon.
    CEPolygon *hole = [[CEPolygon alloc] init];
    [hole addPoint:CELonLatMake(-74.03, 40.9)];
    [hole addPoint:CELonLatMake(-74.13, 40.8)];
    [hole addPoint:CELonLatMake(-74.18, 40.85)];
    [_polygon addHole:hole];
    
    // Adding an image overlay on top of the map.
    _overlay = [[CEOverlay alloc] initWithImage:[UIImage imageNamed:@"overlay_image.png"]];
    _overlay.rotation = 45;
    _overlay.position = CELonLatMake(-74, 41.05);
    _overlay.size = CGSizeMake(8000, kCEUnusedDimension);
    [group addFeature:_overlay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    CEFeatureGroup *group = [self.mapView featureGroupWithName:kCanvasVCFeatureGroup];
    [group removeAllFeatures];
}

@end
