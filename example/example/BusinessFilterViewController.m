//
//  BusinessFilterViewController.m
//  Samples
//
//  Created by Adam Eskreis on 12/18/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import "BusinessFilterViewController.h"
#import <CitymapsEngine/Citymaps.h>

@interface BusinessFilterViewController () <CEMapViewDelegate, CEBusinessDelegate>

@end

@implementation BusinessFilterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.mapView.delegate = self;
    self.mapView.businessDelegate = self;
    [self.mapView setMapCenter:CELonLatMake(-74.000804, 40.742431) andZoom:14];
    
    UIButton *applyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    applyButton.frame = CGRectMake(0, 400, 120, 50);
    [applyButton setTitle:@"Apply Filter" forState:UIControlStateNormal];
    applyButton.backgroundColor = [UIColor grayColor];
    [applyButton addTarget:self action:@selector(applyFilter:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:applyButton];
    
    UIButton *removeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    removeButton.frame = CGRectMake(130, 400, 120, 50);
    [removeButton setTitle:@"Remove Filter" forState:UIControlStateNormal];
    removeButton.backgroundColor = [UIColor grayColor];
    [removeButton addTarget:self action:@selector(removeFilter:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:removeButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)removeFilter:(id)sender
{
    [self.mapView removeBusinessFilter];
}

- (void)applyFilter:(id)sender
{
    CELonLat center = self.mapView.mapCenter;
    CELonLatBounds extents = self.mapView.extents;
    
    NSString *searchURL = [NSString stringWithFormat:@"http://coresearch.citymaps.com/search/filter/bar,club?lat=%f&lon=%f&zoom=%d&north_east=%f,%f&south_west=%f,%f&radius=20&client=test&max_businesses=50", center.lat, center.lon, (int)self.mapView.zoom, extents.max.lon, extents.max.lat, extents.min.lon, extents.min.lat];
    searchURL = [searchURL stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:searchURL]] returningResponse:nil error:nil];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    CEBusinessFilter *filter = [[CEBusinessFilter alloc] init];
    
    NSArray *items = [result objectForKey:@"items"];
    for (NSDictionary *item in items) {
        NSString *bid = [item objectForKey:@"object_id"];
        NSNumber *lon = [item objectForKey:@"lon"];
        NSNumber *lat = [item objectForKey:@"lat"];
        NSNumber *logoImageId = [item objectForKey:@"logo_image"];
        NSNumber *categoryIconId = [item objectForKey:@"category_icon"];
        NSString *name = [item objectForKey:@"name"];
        NSString *address = [item objectForKey:@"address"];
        
        CEBusinessData *business = [[CEBusinessData alloc] init];
        business.businessID = bid;
        business.name = name;
        business.address = address;
        business.location = CELonLatMake([lon doubleValue], [lat doubleValue]);
        business.logoImageID = [logoImageId intValue];
        business.categoryIconID = [categoryIconId intValue];
        
        [filter addBusiness:business state:kCEBusinessStateNormal];
    }
    
    [self.mapView applyBusinessFilter:filter];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.mapView removeBusinessFilter];
}

#pragma mark - CEBusinessDelegate

- (void)businessTapped:(CEBusinessData *)data
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:data.name message:data.address delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [self.view addSubview:alertView];
    [alertView show];
}

@end
