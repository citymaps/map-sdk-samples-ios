//
//  MapViewController.h
//  example
//
//  Created by Eddie Kimmel on 4/7/15.
//  Copyright (c) 2015 Citymaps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CitymapsEngine/Citymaps.h>

@interface MapViewController : UIViewController

- (instancetype)initWithMapView:(CECitymapsMapView *)mapView;

@property (nonatomic, strong) CECitymapsMapView *mapView;

@end
