//
//  BasicMapViewController.m
//  Samples
//
//  Created by Adam Eskreis on 12/13/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import "BasicMapViewController.h"
#import <CitymapsEngine/Citymaps.h>

@interface BasicMapViewController () <CEMapViewDelegate>

@end

@implementation BasicMapViewController

@end
