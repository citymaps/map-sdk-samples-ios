//
//  BusinessMapViewController.h
//  Samples
//
//  Created by Adam Eskreis on 12/17/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"

@interface BusinessMapViewController : MapViewController

@end
