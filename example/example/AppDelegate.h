//
//  AppDelegate.h
//  example
//
//  Created by Eddie Kimmel on 4/7/15.
//  Copyright (c) 2015 Citymaps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

